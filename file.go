package cephlapod

/*
FileWatchFunc is a callback type for FileWatch(). It is invoked with a Logger,
a list of all the files current in the glob, the FileChange of the file that caused
the callback, and the path of the file that caused the callback.
*/
type FileWatchFunc func(logger Logger, files []string, change FileChange, path string)

//go:generate stringer -type=FileChange

/*
FileChange represents the mode of change for a given file in the file watcher.
*/
type FileChange int

/*
There are three possible change conditions for a file: created, modified and deleted.
*/
const (
	FileCreated FileChange = iota
	FileModified
	FileDeleted
)
