package cephlapod

import (
	"fmt"
	"net"
	"os"

	"bitbucket.org/homemade/cephlapod/pb"

	"google.golang.org/grpc"
)

type grpcServer struct {
	server              *grpc.Server
	opts                []grpc.ServerOption
	context             Context
	logger              Logger
	configFn            ConfigFunc
	queryEngineReadyFn  QueryEngineReadyFunc
	iqsConn             pb.ApplicationClient
	fileWatchActuators  fileWatchActuatorList
	httpAccessControlFn HTTPAccessControlFunc
	hooks               map[string]hook
	deployers           map[string]DeploymentFunc
	controllers         map[string]ControllerFunc
	modules             map[string]*Module
	models              map[string]*ModelDefinition
	modelFieldTypes     map[string]*ModelFieldType
	apis                map[string]*API
	themes              map[string]*Theme
	urls                map[string]HTTPHandlerFunc
	urlPermissions      map[string]Permission
}

/*
NewGrpcServer creates a new plugin server that uses gRPC. This is currently the only supported
kind of server in Sepia. The Stop() method of the gRPC Server responds to interrupt
signals, so there's no need to write code to terminate the server when the process
stops.
*/
func NewGrpcServer(opts ...grpc.ServerOption) (Server, error) {

	port := os.Getenv(EnvPluginPort)

	if port == "" {
		return nil, fmt.Errorf("%s environment variable required", EnvPluginPort)
	}

	handle := os.Getenv(EnvPluginHandle)

	if handle == "" {
		return nil, fmt.Errorf("%s environment variable required", EnvPluginHandle)
	}

	p := &grpcServer{
		server: grpc.NewServer(opts...),
		opts:   opts,
		context: Context{
			Port:   port,
			Handle: handle,
		},
		logger:          NewStdoutLogger(),
		hooks:           make(map[string]hook),
		deployers:       make(map[string]DeploymentFunc),
		controllers:     make(map[string]ControllerFunc),
		modules:         make(map[string]*Module),
		models:          make(map[string]*ModelDefinition),
		modelFieldTypes: make(map[string]*ModelFieldType),
		apis:            make(map[string]*API),
		themes:          make(map[string]*Theme),
		urls:            make(map[string]HTTPHandlerFunc),
		urlPermissions:  make(map[string]Permission),
	}

	return p, nil
}

func (g *grpcServer) Serve() error {

	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", g.context.Port))

	if err != nil {
		return fmt.Errorf("Failed to listen on port %s: %v", g.context.Port, err)
	}

	pb.RegisterSetupServer(g.server, g)
	pb.RegisterExternalServiceServer(g.server, g)
	pb.RegisterDeploymentServer(g.server, g)
	pb.RegisterControllerServiceServer(g.server, g)
	pb.RegisterHttpServiceServer(g.server, g)
	pb.RegisterAccessControlServer(g.server, g)

	return g.server.Serve(lis)
}

func (g *grpcServer) Stop() error {

	g.server.Stop()

	return nil
}
