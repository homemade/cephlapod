package cephlapod

import "bitbucket.org/homemade/cephlapod/pb"

type grpcDeleteQuery struct {
	server    *grpcServer
	modelName string
	uid       string
}

func newGrpcDeleteQuery(server *grpcServer, modelName, uid string) WriteQuery {

	return &grpcDeleteQuery{
		server:    server,
		uid:       uid,
		modelName: modelName,
	}
}

func (q *grpcDeleteQuery) Execute() error {

	_, err := q.server.executeIQSQuery(&pb.Query_Input{
		Type: pb.Query_Input_DELETE,
		Payload: &pb.Query_Input_Delete_{
			Delete: &pb.Query_Input_Delete{
				Model: q.modelName,
				Uid:   q.uid,
			},
		},
	}, 0)

	return err
}
