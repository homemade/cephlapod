package cephlapod

import (
	"context"
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AnIQSReadyFuncCanBeRegisteredAgainstGrpcServer(t *testing.T) {

	p, _ := newMockGrpcServer(t)

	p.QueryEngineReady(func(Context, Logger) {})

	require.NotNil(t, p.queryEngineReadyFn)
}

func Test_AGrpcServerReturnsAnErrorWhenADialFails(t *testing.T) {

	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{"nothing:0"})
	require.Nil(t, resp)
	require.NotNil(t, err)
	require.Equal(t, "Can't connect to internal query server: grpc: timed out when dialing", err.Error())
}

func Test_AGrpcServerCanProduceQueries(t *testing.T) {
	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	require.NotNil(t, s.NewCreateQuery(NewModel("t0")))
	require.NotNil(t, s.NewReadQuery("t0"))
	require.NotNil(t, s.NewUpdateQuery("123", NewModel("t0")))
	require.NotNil(t, s.NewDeleteQuery("t0", "123"))
	require.NotNil(t, s.NewDataModelQuery())
}
