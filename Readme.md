## Cephlapod is a Sepia plugin framework for Go.

All Sepia plugins are gRPC servers which respond to requests from Sepia itself,
which acts as a client.  This package wraps the complex and sometimes messy
gRPC functionality, and allows plugins to be developed and tested in an
abstract, test-driven way.

All Sepia plugins are independent, stand-alone applications that run as a
discreet process at the operating system level. They must be independently
executable (as in, their package must be named 'main' and contain a main()
function) and are required to clear up after themselved when their process is
killed by an interrupt signal. Aside from using some standard code to interact
with Sepia, they are free to operate in any way they would like.

Full documentation is available via godoc, and starter plugin project templates
can be generated via the sep binary (see below).

### Basic framework

A cephlapod-powered plugin could be as simple as:

```
package main

import "bitbucket.org/homemade/cephlapod"

func main() {

    server, err := cephlapod.NewGrpcServer()

    if err != nil {
        // handle fatal error
    }

    server.Serve()
}
```

Note that to actually be used in Sepia is will require a Pluginfile (see the 'Plugins'
section in sep doc). Also that plugin won't actually do anything.

A more realistic example of a very simple plugin is:

```
package main

import "bitbucket.org/homemade/cephlapod"

func main() {

    server, err := cephlapod.NewGrpcServer()

    if err != nil {
        // handle fatal error
    }

    server.HandleHook("hookname", myHookHandler, cephlapod.ScopePublic)

    server.Serve()
}

func myHookHandler(ctx cephlapod.Context, logger cephlapod.Logger, req cephlapod.HookRequest, resp *cephlapod.HookResponse) {

    logger.Log("Received a hook request for '%s'", req.Function)

    resp.Model.Set("someField", "This was set by my hook!")
}
```

That plugin (again, it requires a Pluginfile) could be attacked to a model hook
field, and would set the value of `someField`, if it could. It would also log
to Sepia's main log output.

### The sep binary has plugin templates!

The best way to get started with a plugin is to use one of the templates from
the sep binary itself. For a hook template, run

sep new hook-plugin

in a directory under `plugins` in your sepia project. A fully-formed,
documented and unit tested template will be generated with multuple hooks and
complex configuration.