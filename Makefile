test: gotest gometalint

gotest:
	@go test

gometalint:
	@gometalinter -D deadcode -D dupl -D gotype -D gocyclo --deadline=10m --exclude '_test.go' --skip pb --exclude '_string.go'
