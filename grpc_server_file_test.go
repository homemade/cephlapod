package cephlapod

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/stretchr/testify/require"
)

func Test_AGrpcServerCanReadFiles(t *testing.T) {

	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	qs, addr := iqs.NewGrpcServer(t)
	defer qs.Stop()

	qs.FileSystem(map[string][]byte{
		"B": []byte("content B"),
		"A": []byte("content A"),
	})

	resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
	require.Nil(t, err)
	require.NotNil(t, resp)

	content, err := s.FileRead("A")
	require.Nil(t, err)
	require.Equal(t, []byte("content A"), content)

	content, err = s.FileRead("B")
	require.Nil(t, err)
	require.Equal(t, []byte("content B"), content)

	_, err = s.FileRead("C")
	require.NotNil(t, err)
	require.Equal(t, "rpc error: code = 2 desc = File not found", err.Error())
}

func Test_AGrpcServerCanGetAGlobListOfFiles(t *testing.T) {

	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	qs, addr := iqs.NewGrpcServer(t)
	defer qs.Stop()

	qs.FileSystem(map[string][]byte{
		"B": []byte("content B"),
		"A": []byte("content A"),
		"C": []byte("content C"),
	})

	resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
	require.Nil(t, err)
	require.NotNil(t, resp)

	files, err := s.FileGlob("*")
	require.Nil(t, err)
	require.Equal(t, []string{"A", "B", "C"}, files)
}

func mockIQSWithFileSystem(t *testing.T) (iqs.GrpcServer, string, func() error) {

	qs, addr := iqs.NewGrpcServer(t)

	qs.FileSystem(map[string][]byte{
		"B": []byte("content B"),
		"A": []byte("content A"),
		"C": []byte("content C"),
	})

	return qs, addr, qs.Stop
}

func assertWatchCallback(t *testing.T, changes chan []string, expected []string) {

	ticker := time.NewTicker(500 * time.Millisecond)
	defer ticker.Stop()

	select {
	case <-ticker.C:
		t.Fatalf("Timed out waiting for function callback")

	case result, ok := <-changes:
		require.True(t, ok)
		require.Equal(t, expected, result)
	}
}

func assertNoWatchCallback(t *testing.T, changes chan []string) {

	ticker := time.NewTicker(20 * time.Millisecond)
	defer ticker.Stop()

	select {
	case <-ticker.C:
		return

	case <-changes:
		t.Fatalf("Received unexpected input")
	}
}

func Test_AGrpcServerCanWatchSomeGlobs(t *testing.T) {

	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	qs, addr, stopQs := mockIQSWithFileSystem(t)
	defer stopQs()

	resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
	require.Nil(t, err)
	require.NotNil(t, resp)

	changes := make(chan []string, 10)

	fn := func(l Logger, files []string, change FileChange, path string) {
		require.NotNil(t, l)
		changes <- files
	}

	require.Nil(t, s.FileWatch(fn, "A"))

	qs.DeleteFile("A")
	assertWatchCallback(t, changes, []string{"B", "C"})

	qs.WriteFile("A", []byte("A is back!"))
	assertWatchCallback(t, changes, []string{"B", "C", "A"})

	qs.WriteFile("A", []byte("A has changed"))
	assertWatchCallback(t, changes, []string{"B", "C", "A"})

	/////////////////////////////////////////////////////////////////////
	// Make sure that switching to another IQS doesn't break anything
	/////////////////////////////////////////////////////////////////////

	qs2, addr2, stopQs2 := mockIQSWithFileSystem(t)
	defer stopQs2()

	s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr2})
	<-time.After(1 * time.Second)

	qs2.DeleteFile("A")
	assertWatchCallback(t, changes, []string{"B", "C"})

	qs2.WriteFile("A", []byte("A is back!"))
	assertWatchCallback(t, changes, []string{"B", "C", "A"})

	qs2.WriteFile("A", []byte("A has changed"))
	assertWatchCallback(t, changes, []string{"B", "C", "A"})

	/////////////////////////////////////////////////////////////////////
	// Test unwatching
	/////////////////////////////////////////////////////////////////////

	err = s.FileUnwatch("not-watched")
	require.NotNil(t, err)
	require.Equal(t, "Path is not being watched", err.Error())

	require.Nil(t, s.FileUnwatch("A"))
	qs2.WriteFile("A", []byte("A has changed"))
	assertNoWatchCallback(t, changes)
}

func Test_AGrpcServerCanTriggerLiveAssetReloads(t *testing.T) {

	s, _ := newMockGrpcServer(t)
	defer s.Stop()

	_, addr, stopQs := mockIQSWithFileSystem(t)
	defer stopQs()

	resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
	require.Nil(t, err)
	require.NotNil(t, resp)

	require.Nil(t, s.LiveReload("A", "B"))
}
