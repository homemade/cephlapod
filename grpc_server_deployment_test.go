package cephlapod

import (
	"fmt"
	"testing"
	"time"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func mockDeploymentFn(Context, Logger, string) error {
	return nil
}

func Test_ADeploymentFuncCanBeRegisteredAgainstAServer(t *testing.T) {

	df := func(Context, Logger, string) error {
		return nil
	}

	for cycle, test := range []struct {
		description string

		// Inputs
		strategy string
		fn       DeploymentFunc

		// Outputs
		deployers map[string]DeploymentFunc
	}{
		{
			description: "Nil registration",

			strategy: "0",
			fn:       nil,

			deployers: map[string]DeploymentFunc{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			strategy: "1",
			fn:       df,

			deployers: map[string]DeploymentFunc{
				"1": df,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.deployers = make(map[string]DeploymentFunc)

		p.Deployer(test.strategy, test.fn)

		require.Equal(t, len(test.deployers), len(p.deployers))

		for k, v := range test.deployers {

			if v != nil {
				require.NotNil(t, p.deployers[k])
			}
		}
	}
}

func Test_ADeploymentFuncCanCalledOnAServer(t *testing.T) {

	c := make(chan struct{})
	defer close(c)

	failed := func(Context, Logger, string) error {

		go func() { c <- struct{}{} }()
		return fmt.Errorf("test failure")
	}

	succeeded := func(Context, Logger, string) error {

		go func() { c <- struct{}{} }()
		return nil
	}

	for cycle, test := range []struct {
		description string

		// Inputs
		deployers map[string]DeploymentFunc
		request   *pb.DeploymentRequest

		// Outputs
		response *pb.DeploymentResponse
		err      error
	}{
		{
			description: "Call on nil registration",

			deployers: map[string]DeploymentFunc{},
			request: &pb.DeploymentRequest{
				Strategy: "doesn't exist",
			},

			response: &pb.DeploymentResponse{false},
			err:      fmt.Errorf("No deployment strategy '%s' registered", "doesn't exist"),
		},

		{
			description: "Call on non-nil, failed registration",

			deployers: map[string]DeploymentFunc{
				"fail": failed,
			},
			request: &pb.DeploymentRequest{
				Strategy: "fail",
			},

			response: &pb.DeploymentResponse{false},
		},

		{
			description: "Call on non-nil, successful registration",

			deployers: map[string]DeploymentFunc{
				"succeed": succeeded,
			},
			request: &pb.DeploymentRequest{
				Strategy: "succeed",
			},

			response: &pb.DeploymentResponse{true},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)

		p.deployers = test.deployers

		response, err := p.Deploy(context.Background(), test.request)

		require.Equal(t, test.err, err)
		require.Equal(t, test.response, response)

		if test.err == nil {
			timeout := time.NewTicker(1 * time.Second).C

			select {
			case <-timeout:
				t.Fatalf("Timed out waiting for function callback")

			case <-c:
				break
			}
		}
	}
}
