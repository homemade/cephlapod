package cephlapod

import (
	"fmt"
	"testing"

	"github.com/Pallinder/go-randomdata"
	"github.com/stretchr/testify/require"
)

func Test_AModelCanbeCreatedWithValidValues(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		name      string
		fieldSets []map[string]interface{}

		// Outputs
		model  Model
		fields map[string]interface{}
		json   []byte
	}{
		{
			description: "Empty model",

			name: randomdata.SillyName(),

			fields: map[string]interface{}{},
		},

		{
			description: "Model with non-overlapping fields",

			name: randomdata.SillyName(),
			fieldSets: []map[string]interface{}{
				map[string]interface{}{
					"field1": "A",
					"field2": "B",
				},
			},
			fields: map[string]interface{}{
				"field1": "A",
				"field2": "B",
			},

			json: []byte(`{"field1":"A","field2":"B"}`),
		},

		{
			description: "Model with overlapping fields",

			name: "test",
			fieldSets: []map[string]interface{}{
				map[string]interface{}{
					"field1": "A",
					"field2": "B",
				},
				map[string]interface{}{
					"field1": "C",
				},
			},
			fields: map[string]interface{}{
				"field1": "C",
				"field2": "B",
			},
			json: []byte(`{"field1":"C","field2":"B"}`),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		m := NewModel(test.name, test.fieldSets...)

		require.Equal(t, test.name, m.Name())

		for k, v := range test.fields {
			rv, err := m.Get(k)
			require.Nil(t, err)
			require.Equal(t, v, rv)
		}

		require.Equal(t, test.fields, m.Map())

		if len(test.json) > 0 {

			json, err := m.MarshalJSON()
			require.Nil(t, err)
			require.Equal(t, string(test.json), string(json))
		}
	}
}

func Test_AModelCanbeCreatedFromJSON(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		json []byte

		// Outputs
		fields map[string]interface{}
		err    error
	}{
		{
			description: "Empty model",

			json: []byte(`{"field1":"A","field2":"B"}`),
		},

		{
			description: "JSON contains some fields",

			json: []byte(`{"field1":"A","field2":"B"}`),

			fields: map[string]interface{}{
				"field1": "A",
				"field2": "B",
			},
		},

		{
			description: "Invalid JSON",

			json: []byte(`{"field1":"A","field2":"B"`),

			err: fmt.Errorf("unexpected end of JSON input"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		m := NewModel("test")

		err := m.UnmarshalJSON(test.json)

		if test.err == nil {

			require.Nil(t, err, "m.UnmarshalJSON() didn't return nil")

			for k, v := range test.fields {
				rv, err := m.Get(k)
				require.Nil(t, err)
				require.Equal(t, v, rv)
			}

		} else {
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}

func Test_AModelGivesAnErrorWhenANonExistentFieldIsRequested(t *testing.T) {

	m := NewModel(randomdata.SillyName())

	fieldname := randomdata.SillyName()

	f, err := m.Get(fieldname)

	require.Nil(t, f)
	require.NotNil(t, err)
	require.Equal(t, err.Error(), fmt.Sprintf("No field %s", fieldname))
}

func Test_AModelTracksDirtyFieldsOnSetAndReturnsThem(t *testing.T) {

	m := NewModel(randomdata.SillyName(), map[string]interface{}{"A": 1.0})
	require.Equal(t, map[string]interface{}{"A": 1.0}, m.dirtyFields())

	require.Nil(t, m.Set("B", 2.0))
	require.Equal(t, map[string]interface{}{"A": 1.0, "B": 2.0}, m.dirtyFields())

	m.resetDirtyFields()
	require.Equal(t, map[string]interface{}{}, m.dirtyFields())

	require.Nil(t, m.Set("id", 2.0))
	require.Nil(t, m.Set("created", 2.0))
	require.Nil(t, m.Set("last_updated", 2.0))
	require.Equal(t, map[string]interface{}{}, m.dirtyFields())
}
