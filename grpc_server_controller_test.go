package cephlapod

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/homemade/cephlapod/pb"
	"golang.org/x/net/context"

	"github.com/stretchr/testify/require"
)

func Test_AControllerFuncCanBeRegisteredAgainstAServer(t *testing.T) {

	cf := func(Context, Logger, *ControllerContext) error {
		return nil
	}

	for cycle, test := range []struct {
		description string

		// Inputs
		name string
		fn   ControllerFunc

		// Outputs
		controllers map[string]ControllerFunc
	}{
		{
			description: "Nil registration",

			name: "test",
			fn:   nil,

			controllers: map[string]ControllerFunc{
				"test": nil,
			},
		},

		{
			description: "Non-nil registration",

			name: "test",
			fn:   cf,

			controllers: map[string]ControllerFunc{
				"test": cf,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)

		p.Controller(test.name, test.fn)

		require.Equal(t, len(test.controllers), len(p.controllers))

		for k, v := range test.controllers {

			if v != nil {
				require.NotNil(t, p.controllers[k])
			}
		}
	}
}

func Test_AControllerFuncCanBeInvokedOnAServer(t *testing.T) {

	c := make(chan struct{})

	failed := func(Context, Logger, *ControllerContext) error {

		go func() { c <- struct{}{} }()
		return fmt.Errorf("test failure")
	}

	succeeded := func(ctx0 Context, l Logger, ctx1 *ControllerContext) error {

		ctx1.Config["updated"] = "value"

		go func() { c <- struct{}{} }()
		return nil
	}

	invalid := func(ctx0 Context, l Logger, ctx1 *ControllerContext) error {

		ctx1.Config["wrong"] = func() {}

		go func() { c <- struct{}{} }()
		return nil
	}

	for cycle, test := range []struct {
		description string

		// Inputs
		controllers map[string]ControllerFunc
		request     *pb.Controller_InvokeRequest

		// Outputs
		response *pb.Controller_InvokeResponse
		err      error
	}{
		{
			description: "Call on nil registration",

			controllers: map[string]ControllerFunc{},
			request: &pb.Controller_InvokeRequest{
				Name: "doesn't exist",
			},

			response: &pb.Controller_InvokeResponse{},
			err:      fmt.Errorf("Controller '%s' not registered", "doesn't exist"),
		},

		{
			description: "Invalid JSON in request",

			controllers: map[string]ControllerFunc{"success": succeeded},
			request: &pb.Controller_InvokeRequest{
				Name: "success",
				Context: &pb.Controller_Context{
					Config: []byte(`{"one":1.0`),
				},
			},

			response: &pb.Controller_InvokeResponse{},
			err:      fmt.Errorf("Failed to extract context from request: JSON decode failed: unexpected end of JSON input"),
		},

		{
			description: "Controller failure",

			controllers: map[string]ControllerFunc{"failed": failed},
			request: &pb.Controller_InvokeRequest{
				Name: "failed",
				Context: &pb.Controller_Context{
					Config: []byte(`{"one":"two"}`),
				},
			},

			response: &pb.Controller_InvokeResponse{
				Success:      false,
				ErrorMessage: "test failure",
			},
		},

		{
			description: "Invalid response",

			controllers: map[string]ControllerFunc{"invalid": invalid},
			request: &pb.Controller_InvokeRequest{
				Name: "invalid",
				Context: &pb.Controller_Context{
					Config: []byte(`{"one":"two"}`),
				},
			},

			response: &pb.Controller_InvokeResponse{},

			err: fmt.Errorf("Faled to serialise response: JSON encode failed: json: unsupported type: func()"),
		},

		{
			description: "Successful request",

			controllers: map[string]ControllerFunc{"succeeded": succeeded},
			request: &pb.Controller_InvokeRequest{
				Name: "succeeded",
				Context: &pb.Controller_Context{
					Config: []byte(`{"one":"two"}`),
				},
			},

			response: &pb.Controller_InvokeResponse{
				Success: true,
				Context: &pb.Controller_Context{
					UrlParams: make(map[string]string),
					Flags:     make(map[string]string),
					Config:    []byte(`{"one":"two","updated":"value"}`),
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)

		p.controllers = test.controllers

		response, err := p.InvokeController(context.Background(), test.request)

		require.Equal(t, test.err, err)

		if test.err == nil {
			require.Equal(t, test.response.Success, response.Success)
			require.Equal(t, test.response.ErrorMessage, response.ErrorMessage)
			require.Equal(t, test.response.Context, response.Context)

			timeout := time.NewTicker(1 * time.Second).C

			select {
			case <-timeout:
				t.Fatalf("Timed out waiting for function callback")

			case <-c:
				break
			}
		}
	}
}
