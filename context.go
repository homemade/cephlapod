package cephlapod

import "strings"

/*
A Context is passed to all cephlapod handlers and callback functions. It contains everything
that cephlapod knows about the plugin process. The port and handle are assigned at runtime
(when started by Sepia, they're environment variables; in cephtest they're randomised). The
ConfigMap contains everything sent by Sepia (as declared by in the plugins section of sepia.scl)
and any config modified by Server.ValidateConfig().

Note that though a ConfigMap is passed to every callback in a Context, it
shouldn't be modified outside of the a Server.ValidateConfig callback! Doing
that at high load will cause concurrent read/write errors.
*/
type Context struct {
	Port               string
	Handle             string
	ApplicationRootURL string
	Config             ConfigMap
}

/*
PublicURL applies the Sepia application's root URL to a given input URL. For example, if the
Sepia application has a root URL of "/my-app", then PublicURL("/path/to/my/resource") would
return "/my-app/path/to/my/resource".

Note that in --watch mode, the result of this function will change when the
root path in the sepia.scl is changed.
*/
func (c Context) PublicURL(url string) string {
	return "/" + strings.TrimLeft(c.ApplicationRootURL+"/"+strings.TrimLeft(url, "/"), "/")
}
