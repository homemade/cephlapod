package cephlapod

import "net/http"

/*
A HTTPHandlerFunc is the callback type for Server.URL. Aside from the additions
of a Context param and an error return value, it functions like the standard,
idiomatic HTTP handler functions used by the Go standard library.

The return value is used a shorthand way of serving HTTP 500 status codes. If
a non-nil value is returned from the function, the Sepia application send the
requested a 500 Internal Service Error response code and a header containing
the string value of the error message, and will also log the error into the
standard log stream.

As with most HTTP handler functions, the default status code that will be
returned is 200.
*/
type HTTPHandlerFunc func(Context, *http.Request, http.ResponseWriter) error
