package cephlapod

import (
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_ANewControllerCanBeCreated(t *testing.T) {
	require.NotNil(t, NewControllerContext())
}

func Test_AControllerCanBeExtractedFromAGrpcContext(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input pb.Controller_Context

		// Outputs
		output *ControllerContext
		err    error
	}{
		{
			description: "Valid context",

			input: pb.Controller_Context{
				UrlParams: map[string]string{
					"one": "two",
				},
				Css:    []string{"one.css"},
				Js:     []string{"one.js"},
				Config: []byte(`{"one":1.0}`),
				Flags: map[string]string{
					"one": "two",
				},
			},

			output: &ControllerContext{
				URLParams: map[string]string{
					"one": "two",
				},
				CSS: []string{"one.css"},
				JS:  []string{"one.js"},
				Config: map[string]interface{}{
					"one": 1.0,
				},
				Flags: map[string]string{
					"one": "two",
				},
			},
		},

		{
			description: "Invalid context",

			input: pb.Controller_Context{
				UrlParams: map[string]string{
					"one": "two",
				},
				Css:    []string{"one.css"},
				Js:     []string{"one.js"},
				Config: []byte(`{"one":1.0`),
				Flags: map[string]string{
					"one": "two",
				},
			},

			err: fmt.Errorf("JSON decode failed: unexpected end of JSON input"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := controllerContextFromGrpcControllerContext(&test.input)

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, *test.output, *output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}

func Test_AGrpcControllerCanBeExtractedFromAControllerContext(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input *ControllerContext

		// Outputs
		output *pb.Controller_Context
		err    error
	}{
		{
			description: "Valid context",

			input: &ControllerContext{
				URLParams: map[string]string{
					"one": "two",
				},
				CSS: []string{"one.css"},
				JS:  []string{"one.js"},
				Config: map[string]interface{}{
					"one": 1,
				},
				Flags: map[string]string{
					"one": "two",
				},
			},

			output: &pb.Controller_Context{
				UrlParams: map[string]string{
					"one": "two",
				},
				Css:    []string{"one.css"},
				Js:     []string{"one.js"},
				Config: []byte(`{"one":1}`),
				Flags: map[string]string{
					"one": "two",
				},
			},
		},

		{
			description: "Invalid context",

			input: &ControllerContext{
				URLParams: map[string]string{
					"one": "two",
				},
				CSS: []string{"one.css"},
				JS:  []string{"one.js"},
				Config: map[string]interface{}{
					"one": func() {},
				},
				Flags: map[string]string{
					"one": "two",
				},
			},

			output: &pb.Controller_Context{
				UrlParams: map[string]string{
					"one": "two",
				},
				Css: []string{"one.css"},
				Js:  []string{"one.js"},
				Flags: map[string]string{
					"one": "two",
				},
			},

			err: fmt.Errorf("JSON encode failed: json: unsupported type: func()"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := test.input.grpcControllerContext()

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}
