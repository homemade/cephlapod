package cephlapod

import (
	"fmt"
	"net/http"
	"testing"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AnHttpAccessControlFunctionCanBeRegisteredAgainstAGrpcServer(t *testing.T) {

	g, _ := newMockGrpcServer(t)
	require.Nil(t, g.httpAccessControlFn)

	g.HTTPAccessControl(func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error { return nil })
	require.NotNil(t, g.httpAccessControlFn)
}

func Test_AGrpcServerEnactsAccessControlOnRequest(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		fn      HTTPAccessControlFunc
		request *pb.Access_Request

		// Outputs
		response *pb.Access_Response
		err      error
	}{
		{
			description: "No function registered",
			err:         fmt.Errorf("This plugin is not an access control provider"),
		},
		{
			description: "Request error",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{
					Uri: `https://a/b/c/d%X;p?q`,
				},
			},
			fn:  func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error { return nil },
			err: fmt.Errorf("Can't parse HTTP request: parse https://a/b/c/d%%X;p?q: invalid URL escape \"%%X;\""),
		},
		{
			description: "Authorised",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error { return nil },
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_AUTHORISED,
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Authorised with headers",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.Header().Add("h0", "v0")
				return nil
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_AUTHORISED,
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers: map[string]*pb.Http_Header{
						"H0": &pb.Http_Header{
							Value: []string{"v0"},
						},
					},
				},
			},
		},
		{
			description: "Authorised with status code",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.WriteHeader(404)
				return nil
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_AUTHORISED,
				HttpResponse: &pb.Http_Response{
					StatusCode: 404,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "No credentials",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error {
				return NewAccessControlErrNoCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "No credentials with header",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.Header().Add("h0", "v0")
				return NewAccessControlErrNoCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers: map[string]*pb.Http_Header{
						"H0": &pb.Http_Header{
							Value: []string{"v0"},
						},
					},
				},
			},
		},
		{
			description: "No credentials with status code",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.WriteHeader(404)
				return NewAccessControlErrNoCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 404,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Invalid credentials",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error {
				return NewAccessControlErrInvalidCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_INVALIDCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Invalid credentials with header",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.Header().Add("h0", "v0")
				return NewAccessControlErrInvalidCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_INVALIDCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers: map[string]*pb.Http_Header{
						"H0": &pb.Http_Header{
							Value: []string{"v0"},
						},
					},
				},
			},
		},
		{
			description: "Invalid credentials with status code",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.WriteHeader(404)
				return NewAccessControlErrInvalidCredentials("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_INVALIDCREDENTIALS,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 404,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Unauthorisable credentials",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error {
				return NewAccessControlErrNotAuthorised("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOTAUTHORISED,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Unauthorisable credentials with header",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.Header().Add("h0", "v0")
				return NewAccessControlErrNotAuthorised("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOTAUTHORISED,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 200,
					Headers: map[string]*pb.Http_Header{
						"H0": &pb.Http_Header{
							Value: []string{"v0"},
						},
					},
				},
			},
		},
		{
			description: "Unauthorisable credentials with status code",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(c Context, l Logger, p []AccessRequest, r *http.Request, w http.ResponseWriter) error {
				w.WriteHeader(404)
				return NewAccessControlErrNotAuthorised("e0")
			},
			response: &pb.Access_Response{
				Outcome: pb.Access_Response_NOTAUTHORISED,
				Message: "e0",
				HttpResponse: &pb.Http_Response{
					StatusCode: 404,
					Headers:    make(map[string]*pb.Http_Header),
				},
			},
		},
		{
			description: "Untyped error",
			request: &pb.Access_Request{
				HttpRequest: &pb.Http_Request{},
			},
			fn: func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error {
				return fmt.Errorf("e1")
			},
			err: fmt.Errorf("e1"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		g, _ := newMockGrpcServer(t)
		g.HTTPAccessControl(test.fn)

		response, err := g.Auth(context.Background(), test.request)

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.response, response)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, response)
		}
	}
}
