package cephlapod

func (g *grpcServer) URL(path string, fn HTTPHandlerFunc, perm ...Permission) {
	g.urls[path] = fn

	if len(perm) > 0 {
		g.urlPermissions[path] = perm[0]
	}
}
