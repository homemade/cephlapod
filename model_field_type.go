package cephlapod

import "bitbucket.org/homemade/cephlapod/pb"

type ModelFieldType struct {
	Validation map[string]ModelFieldValidationDefinition `json:"validation_rules"`
}

func NewModelFieldType() *ModelFieldType {
	return &ModelFieldType{
		Validation: make(map[string]ModelFieldValidationDefinition),
	}
}

func (m *ModelFieldType) grpc() (*pb.SetupConfigResponse_ModelFieldType, error) {

	output := &pb.SetupConfigResponse_ModelFieldType{}

	var err error
	output.Validation, err = grpcModelFieldValidationDefinition(m.Validation)

	if err != nil {
		return nil, err
	}

	return output, nil
}
