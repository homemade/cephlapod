package cephlapod

//go:generate stringer -type=Operator
//go:generate stringer -type=Order

// Operator represents a comparison method for Internal Service Queries.
type Operator int

/*
Sepia supports 8 comparison operators for the Internal Query System: EQual,
GreaterThan, GreaterThan/Equal, LessThan, LessThan/Equal, NotEqual, IN (a set)
NotIN (a set).  For convenience, they are represented as a series of constants.
*/
const (
	OperatorEQ Operator = iota
	OperatorGT
	OperatorGTE
	OperatorLT
	OperatorLTE
	OperatorNE
	OperatorIN
	OperatorNIN
)

// Order represents a control for the sort sequence of results in Internal Service Queries.
type Order int

//go:generate stringer -type=Order

// Results can be sorted on a given field in asending or descending order.
const (
	SortAscending Order = iota
	SortDescending
)

/*
QueryEngineReadyFunc is a callback type that can be registered against a Server
to be called when the Internal Query Service (provided by the Sepia core) is
ready to accept requests. Typically this will happen within in milliseconds of
the application starting. Before the IQS is ready, executing queries will
result in errors at the Go level.
*/
type QueryEngineReadyFunc func(Context, Logger)

/*
ReadQuery represents a request for record information from Sepia's Internal
Query Service. It's the eqivalent of a SELECT statement in SQL or a find()
operation in MongoDB. For convenience, all the query methods return a
ReadyQuery, which is the query itself, allowing statments to be chained (see
example).
*/
type ReadQuery interface {
	Select(...string) ReadQuery
	Where(string, Operator, interface{}) ReadQuery
	Sort(string, Order) ReadQuery
	Limit(page, pageSize int) ReadQuery
	Execute() ([]Model, error)
}

/*
WriteQuery represents a create, update or delete request to Sepia's Internal
Query Service.
*/
type WriteQuery interface {
	Execute() error
}

/*
DataModelQuery represts a request to get the entire Sepia data model – that is,
a list of all the models, including their fields, validation, hooks and HTTP APIs.
It's a bit like SHOW TABLES in SQL or show collections in MongoDB.
*/
type DataModelQuery interface {
	Execute() (map[string]ModelDefinition, error)
}
