package cephlapod

/*
ControllerFunc is the callback type for Module controllers declared by plugins. A controller's
job is typically to modify the ControllerContext passed in the function, which will then propagate
changes or features in the Modules behaviour or output.

When invoked, a ControllerFunc is passed the current Server's context and logger, in addition to the
ControllerContext.
*/
type ControllerFunc func(Context, Logger, *ControllerContext) error
