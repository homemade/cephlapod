package cephlapod

import (
	"context"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcDatamodelQueryCanBeCreatedFromAValidServer(t *testing.T) {

	for cycle, test := range []struct {
		description string
		results     []*pb.Query_Result
		output      map[string]ModelDefinition
		err         error
	}{
		{
			description: "Success",
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_Type(0),
					Payload: &pb.Query_Result_DataModel_{
						DataModel: &pb.Query_Result_DataModel{
							Models: map[string]*pb.SetupConfigResponse_Model{
								"test1": &pb.SetupConfigResponse_Model{
									Fields: map[string]*pb.SetupConfigResponse_Model_Field{
										"first_name": &pb.SetupConfigResponse_Model_Field{
											Label: "First name",
											Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
												"exampleRule": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
													Config: []uint8{123, 125},
												},
											},
											Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
												"model:before-create": &pb.SetupConfigResponse_Model_Hook{
													Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
														"async": &pb.SetupConfigResponse_Model_Hook_HookAction{
															Functions: []string{"fn0"},
															Async:     true,
															Priority:  0,
														},
														"service": &pb.SetupConfigResponse_Model_Hook_HookAction{
															Functions: []string{"fn"},
															Async:     false,
															Priority:  0,
														},
													},
												},
											},
										},
									},
									Hooks: map[string]*pb.SetupConfigResponse_Model_Hook(nil),
									Apis:  map[string]*pb.SetupConfigResponse_Api(nil),
								},
								"test2": &pb.SetupConfigResponse_Model{
									Fields: map[string]*pb.SetupConfigResponse_Model_Field{
										"first_name": &pb.SetupConfigResponse_Model_Field{
											Label: "First name",
											Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
												"exampleRule": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
													Config: []uint8{123, 125},
												},
											},
											Hooks: map[string]*pb.SetupConfigResponse_Model_Hook(nil),
										},
									},
									Hooks: map[string]*pb.SetupConfigResponse_Model_Hook(nil),
									Apis:  map[string]*pb.SetupConfigResponse_Api(nil),
								},
							},
							Apis: []string{"v1"},
						},
					},
				},
			},
			output: map[string]ModelDefinition{
				"test1": ModelDefinition{
					Fields: map[string]*ModelFieldDefinition{
						"first_name": &ModelFieldDefinition{
							Label: "First name",
							Validation: map[string]ModelFieldValidationDefinition{
								"exampleRule": ModelFieldValidationDefinition{},
							},
							Hooks: map[string]ModelHookDefinition{
								"model:before-create": ModelHookDefinition{
									"async": &ModelHookActionDefinition{
										Functions: []string{"fn0"},
										Async:     true,
										Priority:  0,
									},
									"service": &ModelHookActionDefinition{
										Functions: []string{"fn"},
										Async:     false,
										Priority:  0,
									},
								},
							},
						},
					},
					Hooks: map[string]ModelHookDefinition{},
					Apis:  map[string]*API{},
				},
				"test2": ModelDefinition{
					Fields: map[string]*ModelFieldDefinition{
						"first_name": &ModelFieldDefinition{
							Label: "First name",
							Validation: map[string]ModelFieldValidationDefinition{
								"exampleRule": ModelFieldValidationDefinition{},
							},
							Hooks: map[string]ModelHookDefinition{},
						},
					},
					Hooks: map[string]ModelHookDefinition{},
					Apis:  map[string]*API{},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		qs, addr := iqs.NewGrpcServer(t, test.results...)
		defer qs.Stop()

		resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
		require.Nil(t, err)
		require.NotNil(t, resp)

		q := newGrpcDatamodelQuery(s)
		require.NotNil(t, q)

		output, err := q.Execute()

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}
