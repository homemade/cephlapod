package cephlapod

/*
A HookRequest object is passed to hook callback functions. It contains the name
of the invoked hook and the model data passed from Sepia. Although the model can
be modified, it won't be sent back to Sepia.
*/
type HookRequest struct {
	Function string
	Model    Model
}

/*
NewHookRequest returns a HookRequest object with allocated memory.
*/
func NewHookRequest(function string, model Model) HookRequest {
	return HookRequest{
		Function: function,
		Model:    model,
	}
}
