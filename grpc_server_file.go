package cephlapod

import (
	"context"
	"fmt"
	"io"
	"reflect"
	"time"

	"bitbucket.org/homemade/cephlapod/pb"
)

func (g *grpcServer) FileRead(path string) ([]byte, error) {

	var content []byte

	if g.iqsConn == nil {
		return content, fmt.Errorf("No internal query service configured")
	}

	resp, err := g.iqsConn.FileRead(context.Background(), &pb.FileRead_Input{Path: path})

	if err != nil {
		return content, err
	}

	return resp.Content, nil
}

func (g *grpcServer) FileGlob(globs ...string) ([]string, error) {

	var files []string

	if g.iqsConn == nil {
		return files, fmt.Errorf("No internal query service configured")
	}

	stream, err := g.iqsConn.FileGlob(
		context.Background(),
		&pb.FileGlob_Input{Globs: globs},
	)

	if err != nil {
		return files, fmt.Errorf("Couldn't create query: %s", err)
	}

	resc, errc := make(chan *pb.FileGlob_Output), make(chan error)

	go grpcFileChangeStreamReceiver(stream, resc, errc)

	ticker := time.NewTicker(500 * time.Millisecond)
	defer ticker.Stop()

	select {
	case <-ticker.C:
		return files, fmt.Errorf("Timed out waiting for response")

	case result, ok := <-resc:

		if !ok {
			return files, fmt.Errorf("Channel closed when expecting glob results")
		}

		list := result.GetList()

		if list == nil {
			return files, fmt.Errorf("Expected a list, got a %d", result.Type)
		}

		files = list.Paths

	case err := <-errc:
		return files, err
	}

	return files, stream.CloseSend()
}

func (g *grpcServer) FileWatch(fn FileWatchFunc, globs ...string) error {

	// create actuator
	actuator := g.newFileWatchActuator(fn, globs)
	g.fileWatchActuators = append(g.fileWatchActuators, actuator)

	// start if there's an iqs connection
	if g.iqsConn != nil {
		return actuator.start(g.iqsConn)
	}

	return nil
}

func (g *grpcServer) FileUnwatch(globs ...string) error {

	key := -1

	for k, v := range g.fileWatchActuators {
		if reflect.DeepEqual(v.globs, globs) {
			key = k
		}
	}

	if key == -1 {
		return fmt.Errorf("Path is not being watched")
	}

	g.fileWatchActuators[key].stop()
	g.fileWatchActuators = append(g.fileWatchActuators[:key], g.fileWatchActuators[key+1:]...)

	return nil
}

func (g *grpcServer) LiveReload(paths ...string) error {

	if g.iqsConn == nil {
		return fmt.Errorf("No internal query service configured")
	}

	_, err := g.iqsConn.LiveReload(context.Background(), &pb.LiveReload_Input{Paths: paths})

	return err
}

///////////////////////////////////////////////////////////////////////////////
// Utility functions
///////////////////////////////////////////////////////////////////////////////

func grpcFileChangeStreamReceiver(stream pb.Application_FileGlobClient, resc chan *pb.FileGlob_Output, errc chan error) {

	for {
		result, err := stream.Recv()

		if err == io.EOF {
			close(resc)
			return
		}

		if err != nil {
			errc <- err
		}

		resc <- result
	}
}

///////////////////////////////////////////////////////////////////////////////
// File watch actuator helper
///////////////////////////////////////////////////////////////////////////////

type grpcFileWatchActuator struct {
	cancel  chan struct{}
	fn      FileWatchFunc
	globs   []string
	logger  Logger
	files   []string
	running bool
}

type fileWatchActuatorList []*grpcFileWatchActuator

func (g *grpcServer) newFileWatchActuator(fn FileWatchFunc, globs []string) *grpcFileWatchActuator {

	return &grpcFileWatchActuator{
		cancel: make(chan struct{}, 1),
		fn:     fn,
		globs:  globs,
		logger: g.logger,
	}
}

func (f *grpcFileWatchActuator) start(iqsConn pb.ApplicationClient) error {

	stream, err := iqsConn.FileGlob(
		context.Background(),
		&pb.FileGlob_Input{Globs: f.globs},
	)

	if err != nil {
		return fmt.Errorf("Couldn't create query: %s", err)
	}

	resc, errc := make(chan *pb.FileGlob_Output), make(chan error)

	////////////////////////////////////////////////////////////
	// Set up a persistent reader
	////////////////////////////////////////////////////////////

	go grpcFileChangeStreamReceiver(stream, resc, errc)

	////////////////////////////////////////////////////////////
	// Get initial file list
	////////////////////////////////////////////////////////////

	ticker := time.NewTicker(500 * time.Millisecond)
	defer ticker.Stop()

	select {
	case <-ticker.C:
		return fmt.Errorf("Timed out waiting for initial glob response")

	case result, ok := <-resc:

		if !ok {
			return fmt.Errorf("Channel closed when expecting glob results")
		}

		list := result.GetList()

		if list == nil {
			return fmt.Errorf("Expected a list, got a %s", result.Type)
		}

		f.files = list.Paths

	case err := <-errc:
		return err
	}

	////////////////////////////////////////////////////////////
	// Listen for any changes
	////////////////////////////////////////////////////////////

	f.running = true

	go func() {
		for {
			select {

			case <-f.cancel:
				f.running = false
				return

			case result, ok := <-resc:

				if !ok {
					return
				}

				change := result.GetChange()

				if change == nil {
					f.logger.Log("File watcher: expected a change response, got a %s", result.Type)
					return
				}

				f.actuate(change)

			case err := <-errc:
				f.logger.Log("Error reading wwatch list: %s", err)
				return
			}
		}
	}()

	return nil
}

func (f *grpcFileWatchActuator) actuate(change *pb.FileGlob_Output_Change) {

	op := FileModified

	switch change.Type {

	case pb.FileGlob_Output_Change_ADDITION:

		// Add the element
		f.files = append(f.files, change.Path)

		op = FileCreated

	case pb.FileGlob_Output_Change_DELETION:

		// Delete the element
		for k, v := range f.files {
			if v == change.Path {
				f.files = append(f.files[:k], f.files[k+1:]...)
				break
			}
		}

		op = FileDeleted
	}

	// Call the function
	f.fn(f.logger, f.files, op, change.Path)
}

func (f *grpcFileWatchActuator) stop() {

	if f.running {
		f.cancel <- struct{}{}
	}
}
