package cephlapod

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

/*
A Module reflects the Sepia concepts of a module. Modules in Sepia are the
endpoints of a Sepia web app; they’re the pages an end user will visit in their
browser.
*/
type Module struct {
	Model      map[string]ModuleModelQuery
	View       ModuleView
	Controller string
	Config     map[string]interface{}
}

/*
A ModuleView describes the theme, template and asset information for a Module.
It essentially contains all the information about how the final markup will be
created for the end user.
*/
type ModuleView struct {
	Template string
	Theme    string
	CSS      []string
	JS       []string
	Config   map[string]interface{}
}

/*
ModuleModelQuery describes a model query to be executed by the module at render
time, as described in the Sepia documentation.
*/
type ModuleModelQuery struct {
	Model      string
	Filters    []ModuleModelQueryFilters
	Sortations []ModuleModelQuerySortation
	Limit      ModuleModelQueryLimit
	Fields     []string
	Required   bool
}

/*
NewModuleModelQuery creates a pointer to a model query with allocated memory.
*/
func NewModuleModelQuery() *ModuleModelQuery {
	return &ModuleModelQuery{}
}

/*
ModuleModelQueryFilters describes filters for a query.
*/
type ModuleModelQueryFilters struct {
	Field    string
	Operator Operator
	Value    interface{}
}

/*
ModuleModelQuerySortation describes a sort condition for a query.
*/
type ModuleModelQuerySortation struct {
	Field string
	Order Order
}

/*
ModuleModelQueryLimit the paginated limit for a query.
*/
type ModuleModelQueryLimit struct {
	Page     int
	PageSize int
}

/*
NewModule creates a pointer to a Module with allocated memory.
*/
func NewModule() *Module {
	return &Module{
		Config: make(map[string]interface{}),
		Model:  make(map[string]ModuleModelQuery),
		View: ModuleView{
			Config: make(map[string]interface{}),
		},
	}
}

func controllerFromGrpcModule(input *pb.SetupConfigResponse_Module) (*Module, error) {

	output := NewModule()

	output.Controller = input.Controller

	if input.View != nil {

		output.View.Template = input.View.Template
		output.View.Theme = input.View.Template

		output.View.CSS = append(output.View.CSS, input.View.Css...)
		output.View.JS = append(output.View.JS, input.View.Js...)

		if err := json.Unmarshal(input.View.Config, &output.View.Config); err != nil {
			return nil, fmt.Errorf("View JSON decode failed: %s", err)
		}
	}

	if err := json.Unmarshal(input.Config, &output.Config); err != nil {
		return nil, fmt.Errorf("JSON decode failed: %s", err)
	}

	return output, nil
}

func (c *Module) grpcModule() (*pb.SetupConfigResponse_Module, error) {

	config, err := json.Marshal(c.Config)

	if err != nil {
		return nil, fmt.Errorf("JSON encode failed: %s", err)
	}

	viewConfig, err := json.Marshal(c.View.Config)

	if err != nil {
		return nil, fmt.Errorf("View JSON encode failed: %s", err)
	}

	output := &pb.SetupConfigResponse_Module{
		Controller: c.Controller,
		Config:     config,
		View: &pb.SetupConfigResponse_Module_View{
			Config:   viewConfig,
			Template: c.View.Template,
			Theme:    c.View.Theme,
			Css:      c.View.CSS,
			Js:       c.View.JS,
		},
	}

	for k, q := range c.Model {

		gq, err := q.grpcModelQuery()

		if err != nil {
			return nil, err
		}

		gq.Name = k

		output.Model = append(output.Model, gq)
	}

	return output, nil
}

func (c *ModuleModelQuery) grpcModelQuery() (*pb.SetupConfigResponse_Module_ModelQuery, error) {

	output := &pb.SetupConfigResponse_Module_ModelQuery{
		Model:    c.Model,
		Required: c.Required,
		Filters:  make(map[string]*pb.Query_Input_Read_Filter),
		Limit: &pb.Query_Input_Read_Limit{
			Page:     int32(c.Limit.Page),
			PageSize: int32(c.Limit.PageSize),
		},
	}

	for _, f := range c.Filters {
		v, err := f.grpcFilters()

		if err != nil {
			return nil, err
		}

		output.Filters[f.Field] = v
	}

	for _, s := range c.Sortations {
		output.Sortations = append(output.Sortations, s.grpcSortation())
	}

	return output, nil
}

func (c *ModuleModelQuerySortation) grpcSortation() *pb.Query_Input_Read_Sortation {

	var pbOrder pb.Query_Input_Read_Sortation_Order

	switch c.Order {
	case SortAscending:
		pbOrder = pb.Query_Input_Read_Sortation_ASCENDING
	default:
		pbOrder = pb.Query_Input_Read_Sortation_DESCENDING
	}

	return &pb.Query_Input_Read_Sortation{
		Field: c.Field,
		Order: pbOrder,
	}
}

func (c *ModuleModelQueryFilters) grpcFilters() (*pb.Query_Input_Read_Filter, error) {
	jsn, err := json.Marshal(c.Value)

	if err != nil {
		return nil, err
	}

	var operator pb.Query_Input_Read_Filter_Operator

	switch c.Operator {
	case OperatorEQ:
		operator = pb.Query_Input_Read_Filter_EQ

	case OperatorGT:
		operator = pb.Query_Input_Read_Filter_GT

	case OperatorGTE:
		operator = pb.Query_Input_Read_Filter_GTE

	case OperatorLT:
		operator = pb.Query_Input_Read_Filter_LT

	case OperatorLTE:
		operator = pb.Query_Input_Read_Filter_LTE

	case OperatorNE:
		operator = pb.Query_Input_Read_Filter_NE

	case OperatorIN:
		operator = pb.Query_Input_Read_Filter_IN

	case OperatorNIN:
		operator = pb.Query_Input_Read_Filter_NIN

	default:
		operator = pb.Query_Input_Read_Filter_EQ
	}

	return &pb.Query_Input_Read_Filter{
		Operator: operator,
		Value:    jsn,
	}, nil
}
