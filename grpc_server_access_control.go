package cephlapod

import (
	"fmt"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"
)

func (g *grpcServer) HTTPAccessControl(fn HTTPAccessControlFunc) {
	g.httpAccessControlFn = fn
}

func (g *grpcServer) Auth(ctx context.Context, in *pb.Access_Request) (*pb.Access_Response, error) {

	if g.httpAccessControlFn == nil {
		return nil, fmt.Errorf("This plugin is not an access control provider")
	}

	r, err := httpRequest(in.HttpRequest)

	if err != nil {
		return nil, fmt.Errorf("Can't parse HTTP request: %s", err)
	}

	w := newHTTPResponseWriter()

	var perms []AccessRequest

	for _, p := range in.Permissions {
		perms = append(perms, accessRequestFromGrpc(p))
	}

	err = g.httpAccessControlFn(g.context, g.logger, perms, r, w)

	response := &pb.Access_Response{
		HttpResponse: w.Response(),
	}

	if err == nil {
		response.Outcome = pb.Access_Response_AUTHORISED
		return response, nil
	}

	response.Message = err.Error()

	switch err.(type) {

	case AccessControlErrNoCredentials:
		response.Outcome = pb.Access_Response_NOCREDENTIALS
		return response, nil

	case AccessControlErrInvalidCredentials:
		response.Outcome = pb.Access_Response_INVALIDCREDENTIALS
		return response, nil

	case AccessControlErrNotAuthorised:
		response.Outcome = pb.Access_Response_NOTAUTHORISED
		return response, nil
	}

	return nil, err
}
