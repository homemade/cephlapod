package cephlapod

import (
	"fmt"
	"testing"
	"time"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/Pallinder/go-randomdata"
	"github.com/stretchr/testify/require"
)

func Test_AConfigValidationCallBackCanBeAddedToAGrpcServer(t *testing.T) {

	c := make(chan struct{}, 1)

	for cycle, test := range []struct {
		description string

		// Inputs
		input ConfigMap
		fn    ConfigFunc

		// Outputs
		output      ConfigMap
		logMessages []string
	}{
		{
			description: "No operations",

			input: ConfigMap{},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {
				c <- struct{}{}
				return nil
			},
			output: ConfigMap{},
		},

		{
			description: "One field added",

			input: ConfigMap{
				"one": "A",
			},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {

				logger.Log("recieved %+v", configMap)

				configMap["two"] = "B"

				c <- struct{}{}
				return nil
			},
			output: ConfigMap{
				"one": "A",
				"two": "B",
			},
			logMessages: []string{"recieved map[one:A]"},
		},

		{
			description: "One field changed",

			input: ConfigMap{
				"one": "B",
			},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {

				logger.Log("recieved %+v", configMap)

				configMap["one"] = "C"

				c <- struct{}{}
				return nil
			},
			output: ConfigMap{
				"one": "C",
			},
			logMessages: []string{"recieved map[one:B]"},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, m := newMockGrpcServer(t)

		p.ValidateConfig(test.fn)

		require.NotNil(t, p.configFn)

		go p.configFn(p.context, p.logger, test.input)

		timeout := time.NewTicker(1 * time.Second).C

		select {
		case <-timeout:
			t.Fatalf("Timed out waiting for log response")

		case <-c:
			break
		}

		m.assert(t, test.logMessages...)

		require.Equal(t, test.input, test.output)
	}
}

func Test_ConfigCanBeSentToAPluginServerOverTheWire(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		req   *pb.SetupConfigRequest
		fn    ConfigFunc
		hooks map[string]hook

		// Outputs
		response *pb.SetupConfigResponse
		err      error
		cfg      ConfigMap
	}{
		{
			description: "No operations",

			req: &pb.SetupConfigRequest{},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {
				return nil
			},
			hooks: map[string]hook{},

			response: &pb.SetupConfigResponse{
				ControllerNames: []string{"test"},
				DeployerNames:   []string{"test"},
				Urls:            []string{"0"},
				UrlPermissions:  make(map[string]*pb.Access_Permission),
			},
			cfg: ConfigMap{},
		},

		{
			description: "Some config passed",

			req: &pb.SetupConfigRequest{
				Config: []*pb.SetupConfigRequest_Config{
					{
						Name:  "one",
						Value: "A",
					},
				},
			},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {
				return nil
			},
			hooks: map[string]hook{},

			response: &pb.SetupConfigResponse{
				ControllerNames: []string{"test"},
				DeployerNames:   []string{"test"},
				Urls:            []string{"0"},
				UrlPermissions:  make(map[string]*pb.Access_Permission),
			},
			cfg: ConfigMap{
				"one": "A",
			},
		},

		{
			description: "Some config passed and modified",

			req: &pb.SetupConfigRequest{
				Config: []*pb.SetupConfigRequest_Config{
					{
						Name:  "one",
						Value: "A",
					},
				},
			},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {

				configMap["one"] = "B"
				return nil
			},
			hooks: map[string]hook{},

			response: &pb.SetupConfigResponse{
				ControllerNames: []string{"test"},
				DeployerNames:   []string{"test"},
				Urls:            []string{"0"},
				UrlPermissions:  make(map[string]*pb.Access_Permission),
			},
			cfg: ConfigMap{
				"one": "B",
			},
		},

		{
			description: "Hook config passed",

			req: &pb.SetupConfigRequest{},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {
				return nil
			},
			hooks: map[string]hook{
				"A": hook{
					scope: ScopeProtected,
				},
			},

			response: &pb.SetupConfigResponse{
				ControllerNames: []string{"test"},
				DeployerNames:   []string{"test"},
				HookFunctions: []*pb.SetupConfigResponse_HookFunction{
					{
						Name:  "A",
						Scope: pb.SetupConfigResponse_HookFunction_Scope(ScopeProtected),
					},
				},
				Urls:           []string{"0"},
				UrlPermissions: make(map[string]*pb.Access_Permission),
			},
			cfg: ConfigMap{},
		},

		{
			description: "Function call failed",

			req: &pb.SetupConfigRequest{},
			fn: func(ctx Context, logger Logger, configMap ConfigMap) error {
				return fmt.Errorf("Error")
			},

			err: fmt.Errorf("Config failed: Error"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.ValidateConfig(test.fn)
		p.hooks = test.hooks

		response, err := p.Config(context.Background(), test.req)

		if test.err == nil {

			// Don't test modules, models or APIs
			response.Modules = nil
			response.Models = nil
			response.Apis = nil
			response.Themes = nil
			response.ModelFieldTypes = nil

			require.Equal(t, test.err, err)
			require.Equal(t, test.response, response)

			require.Equal(t, test.cfg, p.context.Config)

		} else {
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, response)
		}
	}
}

func Test_AGrpcServerStoresAGrpcConnectionAndInvokesACallbackFunctionWhenTheICQISReady(t *testing.T) {

	iqs, addr := iqs.NewGrpcServer(t)
	defer iqs.Stop()

	p, m := newMockGrpcServer(t)

	rstr := randomdata.SillyName()

	p.QueryEngineReady(func(c Context, l Logger) {
		l.Log(rstr)
	})

	resp, err := p.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
	require.Nil(t, err)
	require.NotNil(t, resp)
	require.NotNil(t, p.iqsConn)

	m.assert(t, rstr)
}
