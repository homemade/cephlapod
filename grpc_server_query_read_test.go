package cephlapod

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcCreateReadCanBeCreatedFromAValidServer(t *testing.T) {

	for cycle, test := range []struct {
		description string
		modelName   string
		models      []Model
		results     []*pb.Query_Result
		err         error
	}{
		{
			description: "Success",
			modelName:   "test0",
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model: "test0",
							Records: [][]byte{
								[]byte(`[{"id": "1","A": 1.0},{"id":"2","B":2.0},{"id":"3","C":3.0},{"id":"4","D":4.0},{"id":"5","E":5.0}]`),
							},
						},
					},
				},
			},
			models: []Model{
				NewModel("test0", map[string]interface{}{"id": "1", "A": 1.0}),
				NewModel("test0", map[string]interface{}{"id": "2", "B": 2.0}),
				NewModel("test0", map[string]interface{}{"id": "3", "C": 3.0}),
				NewModel("test0", map[string]interface{}{"id": "4", "D": 4.0}),
				NewModel("test0", map[string]interface{}{"id": "5", "E": 5.0}),
			},
		},
		{
			description: "Failure: wrong model type",
			modelName:   "test0",
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model: "test1",
							Records: [][]byte{
								[]byte(`[{"id": "1", "A": 1.0]}`),
							},
						},
					},
				},
			},
			err: fmt.Errorf("Wrong model type: got test1, expected test0"),
		},
		{
			description: "Failure: no response",
			modelName:   "test0",
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type:    pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{},
				},
			},
			err: fmt.Errorf("Query failed: rpc error: code = 13 desc = grpc: proto: oneof field has nil value"),
		},
		{
			description: "Failure: invalid response",
			modelName:   "test0",
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_DATAMODEL,
					Payload: &pb.Query_Result_DataModel_{
						DataModel: &pb.Query_Result_DataModel{},
					},
				},
			},
			err: fmt.Errorf("Can't parse record set from response"),
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		qs, addr := iqs.NewGrpcServer(t, test.results...)
		defer qs.Stop()

		resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
		require.Nil(t, err)
		require.NotNil(t, resp)

		q := newGrpcReadQuery(s, test.modelName)
		require.NotNil(t, q)

		models, err := q.Execute()

		if test.err == nil {

			require.Nil(t, err)

			for k := range models {
				models[k].resetDirtyFields()
				test.models[k].resetDirtyFields()
			}

			require.Equal(t, test.models, models)

		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}

func Test_AGrpcReadQueryCanApplyFiltersSortingAndLimiting(t *testing.T) {

	for cycle, test := range []struct {
		description string
		setup       func(ReadQuery)
		queryInput  *pb.Query_Input
	}{
		{
			description: "Empty query",
			setup: func(q ReadQuery) {
			},
			queryInput: &pb.Query_Input{
				Type: pb.Query_Input_READ,
				Payload: &pb.Query_Input_Read_{
					Read: &pb.Query_Input_Read{
						Model: "test0",
					},
				},
			},
		},
		{
			description: "Select statement",
			setup: func(q ReadQuery) {
				q.
					Select("one").
					Select("two").
					Select("one")
			},
			queryInput: &pb.Query_Input{
				Type: pb.Query_Input_READ,
				Payload: &pb.Query_Input_Read_{
					Read: &pb.Query_Input_Read{
						Model:  "test0",
						Fields: []string{"one", "two"},
					},
				},
			},
		},
		{
			description: "Sort statement",
			setup: func(q ReadQuery) {
				q.
					Sort("one", SortAscending).
					Sort("two", SortDescending)
			},
			queryInput: &pb.Query_Input{
				Type: pb.Query_Input_READ,
				Payload: &pb.Query_Input_Read_{
					Read: &pb.Query_Input_Read{
						Model: "test0",
						Sortations: []*pb.Query_Input_Read_Sortation{
							&pb.Query_Input_Read_Sortation{
								Field: "one",
								Order: pb.Query_Input_Read_Sortation_ASCENDING,
							},
							&pb.Query_Input_Read_Sortation{
								Field: "two",
								Order: pb.Query_Input_Read_Sortation_DESCENDING,
							},
						},
					},
				},
			},
		},
		{
			description: "Where statement",
			setup: func(q ReadQuery) {
				q.
					Where("a", OperatorEQ, "A").
					Where("b", OperatorGT, 1).
					Where("c", OperatorGTE, 1).
					Where("d", OperatorLT, 1).
					Where("e", OperatorLTE, 1).
					Where("f", OperatorNE, "B").
					Where("g", OperatorIN, []string{"A", "B"}).
					Where("h", OperatorNIN, []string{"A", "B"})
			},
			queryInput: &pb.Query_Input{
				Type: pb.Query_Input_READ,
				Payload: &pb.Query_Input_Read_{
					Read: &pb.Query_Input_Read{
						Model: "test0",
						Filters: map[string]*pb.Query_Input_Read_Filter{
							"a": {
								Operator: pb.Query_Input_Read_Filter_EQ,
								Value:    []byte(`"A"`),
							},
							"b": {
								Operator: pb.Query_Input_Read_Filter_GT,
								Value:    []byte(`1`),
							},
							"c": {
								Operator: pb.Query_Input_Read_Filter_GTE,
								Value:    []byte(`1`),
							},
							"d": {
								Operator: pb.Query_Input_Read_Filter_LT,
								Value:    []byte(`1`),
							},
							"e": {
								Operator: pb.Query_Input_Read_Filter_LTE,
								Value:    []byte(`1`),
							},
							"f": {
								Operator: pb.Query_Input_Read_Filter_NE,
								Value:    []byte(`"B"`),
							},
							"g": {
								Operator: pb.Query_Input_Read_Filter_IN,
								Value:    []byte(`["A","B"]`),
							},
							"h": {
								Operator: pb.Query_Input_Read_Filter_NIN,
								Value:    []byte(`["A","B"]`),
							},
						},
					},
				},
			},
		},
		{
			description: "Limit statement",
			setup: func(q ReadQuery) {
				q.Limit(1, 20)
			},
			queryInput: &pb.Query_Input{
				Type: pb.Query_Input_READ,
				Payload: &pb.Query_Input_Read_{
					Read: &pb.Query_Input_Read{
						Model: "test0",
						Limit: &pb.Query_Input_Read_Limit{
							Page:     1,
							PageSize: 20,
						},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		q := newGrpcReadQuery(s, "test0")
		test.setup(q)

		g, ok := q.(*grpcReadQuery)
		require.True(t, ok)

		require.Equal(t, test.queryInput, g.queryInput())
	}
}
