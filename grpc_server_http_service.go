package cephlapod

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"
)

func (g *grpcServer) HttpRequest(ctx context.Context, req *pb.Http_Request) (*pb.Http_Response, error) {

	handler, exists := g.urls[req.Uri]

	if !exists {
		return nil, fmt.Errorf("No URI '%s' registered", req.Uri)
	}

	r, err := httpRequest(req)

	if err != nil {
		return nil, fmt.Errorf("Can't parse HTTP request: %s", err)
	}

	w := newHTTPResponseWriter()

	if err := handler(g.context, r, w); err != nil {
		return nil, err
	}

	return w.Response(), nil
}

func httpRequest(req *pb.Http_Request) (*http.Request, error) {

	uri, err := url.Parse(req.Uri)

	if err != nil {
		return nil, err
	}

	resp := &http.Request{
		Method:        req.Method,
		URL:           uri,
		Header:        make(http.Header),
		ContentLength: req.ContentLength,
	}

	if len(req.Content) > 0 {
		resp.Body = ioutil.NopCloser(bytes.NewBuffer(req.Content))
	}

	for k, v := range req.Cookies {
		resp.AddCookie(&http.Cookie{
			Name:  k,
			Value: v.Value,
		})
	}

	for k, v := range req.Headers {
		for _, h := range v.Value {
			resp.Header.Add(k, h)
		}
	}

	return resp, nil
}

type httpResponseWriter struct {
	header   http.Header
	response *pb.Http_Response
}

func newHTTPResponseWriter() *httpResponseWriter {
	return &httpResponseWriter{
		header: make(http.Header),
		response: &pb.Http_Response{
			StatusCode: 200,
			Headers:    make(map[string]*pb.Http_Header),
		},
	}
}

func (h *httpResponseWriter) Header() http.Header {
	return h.header
}

func (h *httpResponseWriter) Write(input []byte) (int, error) {
	h.response.Content = append(h.response.Content, input...)
	return len(input), nil
}

func (h *httpResponseWriter) WriteHeader(header int) {
	h.response.StatusCode = int32(header)
}

func (h *httpResponseWriter) Response() *pb.Http_Response {

	for k, v := range h.header {
		h.response.Headers[k] = &pb.Http_Header{
			Value: v,
		}
	}

	h.response.ContentLength = int32(len(h.response.Content))

	return h.response
}
