package cephlapod

import (
	"context"
	"fmt"
	"io"
	"time"

	"bitbucket.org/homemade/cephlapod/pb"
)

func (g *grpcServer) QueryEngineReady(fn QueryEngineReadyFunc) {
	g.queryEngineReadyFn = fn
}

func (g *grpcServer) NewCreateQuery(model Model) WriteQuery {
	return newGrpcCreateQuery(g, model)
}

func (g *grpcServer) NewReadQuery(modelName string) ReadQuery {
	return newGrpcReadQuery(g, modelName)
}

func (g *grpcServer) NewUpdateQuery(uid string, model Model) WriteQuery {
	return newGrpcUpdateQuery(g, uid, model)
}

func (g *grpcServer) NewDeleteQuery(modelName, uid string) WriteQuery {
	return newGrpcDeleteQuery(g, modelName, uid)
}

func (g *grpcServer) NewDataModelQuery() DataModelQuery {
	return newGrpcDatamodelQuery(g)
}

func (g *grpcServer) executeIQSQuery(req *pb.Query_Input, count int) (res []*pb.Query_Result, err error) {

	if g.iqsConn == nil {
		return nil, fmt.Errorf("No internal query service configured")
	}

	stream, err := g.iqsConn.Query(context.Background(), req)

	if err != nil {
		return nil, fmt.Errorf("Couldn't create query: %s", err)
	}

	resc, errc := make(chan *pb.Query_Result), make(chan error)

	go func() {
		for {
			result, err := stream.Recv()

			if err == io.EOF {
				close(resc)
				return
			}

			if err != nil {
				errc <- err
			}

			resc <- result
		}
	}()

	ticker := time.NewTicker(2000 * time.Millisecond)
	defer ticker.Stop()

	for i := 0; i < count; i++ {

		select {

		case <-ticker.C:
			return nil, fmt.Errorf("Timed out waiting for query response")

		case result, ok := <-resc:

			if !ok {
				return nil, fmt.Errorf("Channel closed when expecting query results")
			}

			res = append(res, result)

		case err = <-errc:
			return
		}
	}

	return
}
