package cephlapod

import (
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/stretchr/testify/require"
)

func Test_AGrpcThemeCanBeExtractedFromATheme(t *testing.T) {
	for cycle, test := range []struct {
		description string

		// Inputs
		input *Theme

		// Outputs
		output *pb.SetupConfigResponse_Theme
		err    error
	}{
		{
			description: "Valid; empty",
			input:       NewTheme(),
			output: &pb.SetupConfigResponse_Theme{
				Css: map[string]*pb.SetupConfigResponse_Theme_Path{},
				Js:  map[string]*pb.SetupConfigResponse_Theme_Path{},
				Dir: map[string]*pb.SetupConfigResponse_Theme_Path{},
			},
		},
		{
			description: "Valid",
			input: &Theme{
				BackendTemplatePaths:  []string{"1", "2"},
				BackendMasterTemplate: "master",
				FrontendTemplatePaths: []string{"3", "4"},
				CSS: ThemePathMap{
					"one.css": ThemePath{
						Paths:      []string{"1", "2"},
						TrimPrefix: "1",
					},
				},
				JS: ThemePathMap{
					"one.js": ThemePath{
						Paths:      []string{"1", "2"},
						TrimPrefix: "1",
					},
				},
				Dir: ThemePathMap{
					"one.dir": ThemePath{
						Paths: []string{"1", "2"},
					},
				},
			},
			output: &pb.SetupConfigResponse_Theme{
				BackendTemplatePaths:  []string{"1", "2"},
				BackendMasterTemplate: "master",
				FrontendTemplatePaths: []string{"3", "4"},
				Css: map[string]*pb.SetupConfigResponse_Theme_Path{
					"one.css": &pb.SetupConfigResponse_Theme_Path{
						Paths:      []string{"1", "2"},
						TrimPrefix: "1",
					},
				},
				Js: map[string]*pb.SetupConfigResponse_Theme_Path{
					"one.js": &pb.SetupConfigResponse_Theme_Path{
						Paths:      []string{"1", "2"},
						TrimPrefix: "1",
					},
				},
				Dir: map[string]*pb.SetupConfigResponse_Theme_Path{
					"one.dir": &pb.SetupConfigResponse_Theme_Path{
						Paths: []string{"1", "2"},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output := test.input.grpcTheme()

		require.NotNil(t, output)
		require.Equal(t, test.output, output)
	}
}
