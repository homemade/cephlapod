package cephlapod

import "bitbucket.org/homemade/cephlapod/pb"

/*
A ThemePath represents the file system location of an asset in a theme.
*/
type ThemePath struct {
	Paths      []string
	TrimPrefix string
}

/*
A ThemePathMap is a shorthand for a map of strings to ThemePaths.
*/
type ThemePathMap map[string]ThemePath

/*
NewThemePathMap created a memory-allocated ThemePathMap.
*/
func NewThemePathMap() ThemePathMap {
	return make(ThemePathMap)
}

/*
A Theme in Sepia is an assembler of markup, styling and logic that will be
delivered to the end user through a web browser. It is, essentially, the
serverside parts of the web application component of a Sepia project, in that
it determines everything that the end user will interact with directly.

The Theme struct directly reflects the theme concept, more information
on which can be found in the Sepia documentation.
*/
type Theme struct {
	BackendTemplatePaths  []string
	BackendMasterTemplate string
	FrontendTemplatePaths []string
	CSS                   ThemePathMap
	JS                    ThemePathMap
	Dir                   ThemePathMap
}

/*
NewTheme creates a pointer to a new Theme object
*/
func NewTheme() *Theme {
	return &Theme{}
}

func (t *Theme) grpcTheme() *pb.SetupConfigResponse_Theme {

	return &pb.SetupConfigResponse_Theme{
		BackendTemplatePaths:  t.BackendTemplatePaths,
		BackendMasterTemplate: t.BackendMasterTemplate,
		FrontendTemplatePaths: t.FrontendTemplatePaths,
		Css: t.CSS.grpcThemePathMap(),
		Js:  t.JS.grpcThemePathMap(),
		Dir: t.Dir.grpcThemePathMap(),
	}
}

func (t ThemePathMap) grpcThemePathMap() map[string]*pb.SetupConfigResponse_Theme_Path {

	output := make(map[string]*pb.SetupConfigResponse_Theme_Path)

	for k, v := range t {
		output[k] = v.grpcThemePath()
	}

	return output
}

func (t ThemePath) grpcThemePath() *pb.SetupConfigResponse_Theme_Path {
	return &pb.SetupConfigResponse_Theme_Path{
		Paths:      t.Paths,
		TrimPrefix: t.TrimPrefix,
	}
}
