package cephlapod

import (
	"fmt"
	"net/http"
	"net/url"
	"testing"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcServerCanServeHttpRequests(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		handlers map[string]HTTPHandlerFunc
		req      *pb.Http_Request

		// Outputs
		output *pb.Http_Response
		err    error
	}{
		{
			description: "Unregistered URL",

			handlers: map[string]HTTPHandlerFunc{},
			req: &pb.Http_Request{
				Uri: "/doesnt-exist",
			},

			output: &pb.Http_Response{
				StatusCode: 200,
				Headers:    make(map[string]*pb.Http_Header),
			},

			err: fmt.Errorf("No URI '/doesnt-exist' registered"),
		},
		{
			description: "Invalid URL",

			handlers: map[string]HTTPHandlerFunc{
				`https://a/b/c/d%X;p?q`: func(Context, *http.Request, http.ResponseWriter) error { return nil },
			},
			req: &pb.Http_Request{
				Uri: `https://a/b/c/d%X;p?q`,
			},

			output: &pb.Http_Response{
				StatusCode: 200,
				Headers:    make(map[string]*pb.Http_Header),
			},

			err: fmt.Errorf("Can't parse HTTP request: parse https://a/b/c/d%%X;p?q: invalid URL escape \"%%X;\""),
		},
		{
			description: "Function returning error",

			handlers: map[string]HTTPHandlerFunc{
				`/some-url`: func(Context, *http.Request, http.ResponseWriter) error {
					return fmt.Errorf("Some error")
				},
			},
			req: &pb.Http_Request{
				Uri: `/some-url`,
			},

			output: &pb.Http_Response{
				StatusCode: 200,
				Headers:    make(map[string]*pb.Http_Header),
			},

			err: fmt.Errorf("Some error"),
		},
		{
			description: "Success with status code",

			handlers: map[string]HTTPHandlerFunc{
				`/some-url`: func(c Context, r *http.Request, w http.ResponseWriter) error {
					w.WriteHeader(201)
					return nil
				},
			},
			req: &pb.Http_Request{
				Uri: `/some-url`,
			},

			output: &pb.Http_Response{
				StatusCode: 201,
				Headers:    make(map[string]*pb.Http_Header),
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		g, _ := newMockGrpcServer(t)

		g.urls = test.handlers

		output, err := g.HttpRequest(context.Background(), test.req)

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}

func Test_HttpRequestsCanBeMadeToASever(t *testing.T) {

	cf := func(Context, Logger, *ControllerContext) error {
		return nil
	}

	for cycle, test := range []struct {
		description string

		// Inputs
		name string
		fn   ControllerFunc

		// Outputs
		controllers map[string]ControllerFunc
	}{
		{
			description: "Nil registration",

			name: "test",
			fn:   nil,

			controllers: map[string]ControllerFunc{
				"test": nil,
			},
		},

		{
			description: "Non-nil registration",

			name: "test",
			fn:   cf,

			controllers: map[string]ControllerFunc{
				"test": cf,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)

		p.Controller(test.name, test.fn)

		require.Equal(t, len(test.controllers), len(p.controllers))

		for k, v := range test.controllers {

			if v != nil {
				require.NotNil(t, p.controllers[k])
			}
		}
	}
}

func Test_AHttpRequestCanBeExtractedFromAGrpcRequest(t *testing.T) {

	uri, err := url.Parse("/some-url?test=1")
	require.Nil(t, err)

	for cycle, test := range []struct {
		description string

		// Inputs
		input *pb.Http_Request

		// Outputs
		output *http.Request
		err    error
	}{
		{
			description: "Normal request",

			input: &pb.Http_Request{
				Method: http.MethodGet,
				Uri:    "/some-url?test=1",
				Cookies: map[string]*pb.Http_Cookie{
					"c0": &pb.Http_Cookie{
						Value: "v0",
					},
				},
				Headers: map[string]*pb.Http_Header{
					"h0": &pb.Http_Header{Value: []string{"v1"}},
				},
			},
			output: &http.Request{
				Method: http.MethodGet,
				URL:    uri,
				Header: http.Header{
					"Cookie": []string{"c0=v0"},
					"H0":     []string{"v1"},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := httpRequest(test.input)

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}

func Test_AHttpRequestIsAnHttpResponseWriter(t *testing.T) {
	w := newHTTPResponseWriter()
	require.NotNil(t, w)
	require.NotNil(t, w.response)
	require.NotNil(t, w.header)

	var _ http.ResponseWriter = w
}

func Test_AnHttpResponseWriterCanProdudeAnHttpResponse(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input func(http.ResponseWriter)

		// Outputs
		output *pb.Http_Response
	}{
		{
			description: "Empty request",

			input: func(w http.ResponseWriter) {},

			output: &pb.Http_Response{
				StatusCode: 200,
				Headers:    make(map[string]*pb.Http_Header),
			},
		},
		{
			description: "Request with status and headers",

			input: func(w http.ResponseWriter) {
				w.WriteHeader(201)
				w.Header().Add("h0", "v0")
			},

			output: &pb.Http_Response{
				StatusCode: 201,
				Headers: map[string]*pb.Http_Header{
					"H0": &pb.Http_Header{
						Value: []string{"v0"},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		w := newHTTPResponseWriter()
		test.input(w)
		output := w.Response()

		require.Equal(t, output, test.output)
	}
}
