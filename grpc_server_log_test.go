package cephlapod

import "google.golang.org/grpc/grpclog"

type grpcLogger struct {
}

func (g *grpcLogger) Fatal(args ...interface{})                 {}
func (g *grpcLogger) Fatalf(format string, args ...interface{}) {}
func (g *grpcLogger) Fatalln(args ...interface{})               {}
func (g *grpcLogger) Print(args ...interface{})                 {}
func (g *grpcLogger) Printf(format string, args ...interface{}) {}
func (g *grpcLogger) Println(args ...interface{})               {}

func init() {
	grpclog.SetLogger(&grpcLogger{})
}
