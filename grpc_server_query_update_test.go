package cephlapod

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcCreateQueryCanBeUpdatedFromAValidServer(t *testing.T) {

	for cycle, test := range []struct {
		description string
		uid         string
		inputModel  Model
		outputModel Model
		results     []*pb.Query_Result
		err         error
	}{
		{
			description: "Success",
			uid:         "12345",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0, "B": 2.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model:   "test",
							Records: [][]byte{[]byte(`{"id": "12345", "A": 1, "B": 2}`)},
						},
					},
				},
			},
		},
		{
			description: "Failure: can't marshal JSON",
			inputModel:  NewModel("test", map[string]interface{}{"A": func() {}}),
			err:         fmt.Errorf("Failed to marshal JSON: json: unsupported type: func()"),
		},
		{
			description: "Failure: can't unmarshal JSON",
			uid:         "12345",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model:   "test",
							Records: [][]byte{[]byte(`{"id": "12345", "A": 1, "B": 2`)},
						},
					},
				},
			},
			err: fmt.Errorf("Failed to unmarshal response JSON: unexpected end of JSON input"),
		},
		{
			description: "Failure: wrong model type",
			uid:         "12345",
			inputModel:  NewModel("test0", map[string]interface{}{"A": 1.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model:   "test1",
							Records: [][]byte{[]byte(`{"id": "12345", "A": 1, "B": 2}`)},
						},
					},
				},
			},
			err: fmt.Errorf("Wrong model type: got test1, expected test0"),
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		qs, addr := iqs.NewGrpcServer(t, test.results...)
		defer qs.Stop()

		resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
		require.Nil(t, err)
		require.NotNil(t, resp)

		q := newGrpcUpdateQuery(s, test.uid, test.inputModel)
		require.NotNil(t, q)

		err = q.Execute()

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.outputModel.Map(), test.inputModel.Map())
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}
