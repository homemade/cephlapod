package cephlapod

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

/*
A ControllerContext represents the state of a the controller in a Module, which
ultimately influences the view and behaviour of that model. See the sepia
documentation for details of the effects of controller states.
*/
type ControllerContext struct {
	Template            string
	URLParams           map[string]string
	CSS                 []string
	JS                  []string
	Config              map[string]interface{}
	Flags               map[string]string
	CollectionsSnapshot json.RawMessage
	Redirect            struct {
		URL        string
		StatusCode int64
	}
	Headers []ResponseHeader
	Render  bool
}

type ResponseHeader struct {
	Name   string
	Values []string
}

/*
NewControllerContext creates a pointer to a ControllerContext with allocated maps.
*/
func NewControllerContext() *ControllerContext {
	return &ControllerContext{
		URLParams: make(map[string]string),
		Config:    make(map[string]interface{}),
		Flags:     make(map[string]string),
	}
}

func controllerContextFromGrpcControllerContext(input *pb.Controller_Context) (*ControllerContext, error) {

	output := NewControllerContext()

	output.Template = input.Template

	for k, v := range input.UrlParams {
		output.URLParams[k] = v
	}

	for k, v := range input.Flags {
		output.Flags[k] = v
	}

	output.CSS = append(output.CSS, input.Css...)
	output.JS = append(output.JS, input.Js...)

	if len(input.Config) > 0 {
		if err := json.Unmarshal(input.Config, &output.Config); err != nil {
			return nil, fmt.Errorf("JSON decode of %s failed: %s", string(input.Config), err)
		}
	}

	output.CollectionsSnapshot = input.CollectionsSnapshot

	if input.Redirect != nil {
		output.Redirect.URL = input.Redirect.Url
		output.Redirect.StatusCode = input.Redirect.StatusCode
	}
	var headers []ResponseHeader
	for _, next := range input.Headers {
		headers = append(headers, ResponseHeader{
			Name:   next.Name,
			Values: next.Values,
		})
	}
	output.Headers = headers
	output.Render = input.Render

	return output, nil
}

func (c *ControllerContext) grpcControllerContext() (*pb.Controller_Context, error) {

	output := &pb.Controller_Context{
		UrlParams: make(map[string]string),
		Flags:     make(map[string]string),
	}

	output.Template = c.Template

	for k, v := range c.URLParams {
		output.UrlParams[k] = v
	}

	for k, v := range c.Flags {
		output.Flags[k] = v
	}

	output.Css = append(output.Css, c.CSS...)
	output.Js = append(output.Js, c.JS...)

	if len(c.Config) > 0 {
		jsn, err := json.Marshal(c.Config)

		if err != nil {
			return nil, fmt.Errorf("JSON encode failed: %s", err)
		}

		output.Config = jsn
	}

	output.CollectionsSnapshot = c.CollectionsSnapshot

	if c.Redirect.URL != "" {
		output.Redirect = &pb.Controller_Redirect{
			Url:        c.Redirect.URL,
			StatusCode: c.Redirect.StatusCode,
		}
	}
	var headers []*pb.Controller_Header
	for _, next := range c.Headers {
		headers = append(headers, &pb.Controller_Header{
			Name:   next.Name,
			Values: next.Values,
		})
	}
	output.Headers = headers

	output.Render = c.Render

	return output, nil
}
