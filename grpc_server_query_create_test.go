package cephlapod

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcCreateQueryCanBeCreatedFromAValidServer(t *testing.T) {

	for cycle, test := range []struct {
		description string
		inputModel  Model
		outputModel Model
		results     []*pb.Query_Result
		err         error
	}{
		{
			description: "Success",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model:   "test",
							Records: [][]byte{[]byte(`{"id": "12345", "A": 1}`)},
						},
					},
				},
			},
		},
		{
			description: "Failure: can't marshal JSON",
			inputModel:  NewModel("test", map[string]interface{}{"A": func() {}}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			err:         fmt.Errorf("Failed to marshal JSON: json: unsupported type: func()"),
		},
		{
			description: "Failure: record is nil",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			results:     []*pb.Query_Result{&pb.Query_Result{}},
			err:         fmt.Errorf("Response record is nil"),
		},
		{
			description: "Failure: query failed",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			err:         fmt.Errorf("Query failed: Channel closed when expecting query results"),
		},
		{
			description: "Failure: non-matching results",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model:   "non-matching",
							Records: [][]byte{[]byte(`{"id": "12345", "A": 1}`)},
						},
					},
				},
			},
			err: fmt.Errorf("Create method gave model name non-matching, expected test"),
		},
		{
			description: "Failure: too many records",
			inputModel:  NewModel("test", map[string]interface{}{"A": 1.0}),
			outputModel: NewModel("test", map[string]interface{}{"id": "12345", "A": 1.0}),
			results: []*pb.Query_Result{
				&pb.Query_Result{
					Type: pb.Query_Result_RECORDSET,
					Payload: &pb.Query_Result_Record{
						Record: &pb.Query_Result_RecordSet{
							Model: "test",
							Records: [][]byte{
								[]byte(`{"id": "12345", "A": 1}`),
								[]byte(`{"id": "12345", "A": 1}`),
							},
						},
					},
				},
			},
			err: fmt.Errorf("Expected 2 records in create method, got 1"),
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		qs, addr := iqs.NewGrpcServer(t, test.results...)
		defer qs.Stop()

		resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
		require.Nil(t, err)
		require.NotNil(t, resp)

		q := newGrpcCreateQuery(s, test.inputModel)
		require.NotNil(t, q)

		err = q.Execute()

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.outputModel.Map(), test.inputModel.Map())
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}
