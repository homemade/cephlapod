package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
	"golang.org/x/net/context"
)

func (g *grpcServer) HandleHook(name string, fn HookFunc, scope HookScope, modelWhitelist ...string) {
	g.hooks[name] = hook{scope, fn, modelWhitelist}
}

func (g *grpcServer) Call(ctx context.Context, in *pb.ExternalServiceRequest) (*pb.ExternalServiceResponse, error) {

	hk, exists := g.hooks[in.Function]

	if !exists || hk.fn == nil {
		return nil, fmt.Errorf("No hook function called '%s' registered", in.Function)
	}

	if len(hk.modelWhitelist) > 0 {

		found := false

		for _, v := range hk.modelWhitelist {
			if v == in.Model.Name {
				found = true
				break
			}
		}

		if !found {
			return nil, fmt.Errorf("Hook function doesn't accept model '%s'", in.Model.Name)
		}
	}

	inputModel := NewModel(in.Model.Name)

	if err := inputModel.UnmarshalJSON(in.Model.Json); err != nil {
		return nil, fmt.Errorf("Can't unmarshall input JSON: %s", err)
	}

	req := NewHookRequest(in.Function, inputModel)
	resp := NewHookResponse(NewModel(in.Model.Name))

	hk.fn(g.context, g.logger, req, resp)

	return resp.externalServiceResponse()
}
