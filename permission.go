package cephlapod

import "bitbucket.org/homemade/cephlapod/pb"

/*
PermissionRoles describes the roles required for a given Sepia operation.
Permission roles in AllRoles must all be authorised and authenticated by the
access control plugin; if length of AnyRoles is more than zero, then one of the
roles specified must be authorised and authenticated by the access control
plugin.

PermissionRoles is used for telling Sepia about required access permissions for
assets, models and modules created by plugins. It's not used for creating an
access control plugin.
*/
type PermissionRoles struct {
	AllRoles []string
	AnyRoles []string
}

/*
NewPermissionRoles creates a PermissionRoles instance with allocated memory.
*/
func NewPermissionRoles() PermissionRoles {
	return PermissionRoles{}
}

/*
Transform a PermissionRoles struct into its gRPC equivalent.
*/
func (p PermissionRoles) grpc() *pb.Access_Permission_Realm {
	return &pb.Access_Permission_Realm{
		AllRoles: p.AllRoles,
		AnyRoles: p.AnyRoles,
	}
}

/*
Permission describes the access rules for a given Sepia operation. Access may
be completely public (as in, anyone can access regardless of credential),
completely private (no external access to anyone at all) or have
PermissionRoles specified againt realms. In the normal Sepia setup, a realm is
essentially an access control plugin, referenced by its friendly name.

Permission is used for telling Sepia about required access permissions for
assets, models and modules created by plugins. It's not used for creating an
access control plugin.

It should be noted that access control is only used for external access to the
system.  Plugins using the Internal Query System are not subject to access
control permissions.
*/
type Permission struct {
	Public  bool
	Private bool
	Realms  map[string]PermissionRoles
}

/*
NewPermission creates an instance of a Permission with allocated memory.
*/
func NewPermission() Permission {
	return Permission{
		Realms: make(map[string]PermissionRoles),
	}
}

/*
Transform a Permission struct into its gRPC equivalent.
*/
func (p Permission) grpc() *pb.Access_Permission {

	perm := &pb.Access_Permission{
		Private: p.Private,
		Public:  p.Public,
		Realms:  make(map[string]*pb.Access_Permission_Realm),
	}

	for k, v := range p.Realms {
		perm.Realms[k] = v.grpc()
	}

	return perm
}

/*
ReadWritePermission is a superset of Permission structs that specifies read
and write access only. It's generally used for model fields.
*/
type ReadWritePermission struct {
	Read  Permission
	Write Permission
}

/*
NewReadWritePermission creates a ReadWritePermission with allocated memory.
*/
func NewReadWritePermission() ReadWritePermission {
	return ReadWritePermission{}
}

/*
CRULDPermission is a superset of Permission structs that specifies create,
read, update, list and delete access. It's generally used for models.
*/
type CRULDPermission struct {
	Create Permission
	Read   Permission
	Update Permission
	List   Permission
	Delete Permission
}

/*
NewCRULDPermission creates a CRULDPermission with allocated memory.
*/
func NewCRULDPermission() CRULDPermission {
	return CRULDPermission{}
}
