package cephlapod

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_AUrlCanBeRegisteredAgainstAServer(t *testing.T) {

	fn := func(Context, *http.Request, http.ResponseWriter) error { return nil }

	for cycle, test := range []struct {
		description string

		// Inputs
		path string
		fn   HTTPHandlerFunc

		// Outputs
		handlers map[string]HTTPHandlerFunc
	}{
		{
			description: "Nil registration",

			path: "0",

			handlers: map[string]HTTPHandlerFunc{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			path: "1",
			fn:   fn,

			handlers: map[string]HTTPHandlerFunc{
				"1": fn,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.urls = make(map[string]HTTPHandlerFunc)

		p.URL(test.path, test.fn)

		require.Equal(t, len(test.handlers), len(p.urls))

		for k, v := range test.handlers {

			if v != nil {
				require.NotNil(t, p.urls[k])
			}
		}
	}
}
