package cephlapod

import (
	"context"
	"testing"

	"bitbucket.org/homemade/cephlapod/cephtest/iqs"
	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AGrpcDeleteQueryCanBeCreatedFromAValidServer(t *testing.T) {

	for cycle, test := range []struct {
		description string
		uid         string
		results     []*pb.Query_Result
		err         error
	}{
		{
			description: "Success",
			uid:         "1234",
			results:     []*pb.Query_Result{},
		},
	} {

		t.Logf("Cycle %d: %s", cycle, test.description)

		s, _ := newMockGrpcServer(t)
		defer s.Stop()

		qs, addr := iqs.NewGrpcServer(t, test.results...)
		defer qs.Stop()

		resp, err := s.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{addr})
		require.Nil(t, err)
		require.NotNil(t, resp)

		q := newGrpcDeleteQuery(s, "test0", test.uid)
		require.NotNil(t, q)

		require.Nil(t, q.Execute())
	}
}
