package cephlapod

import (
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/stretchr/testify/require"
)

func Test_AnAccessRequestCanBeExtractedFromAGrpcStruct(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		grpc *pb.Access_Request_Permission

		// Outputs
		ceph AccessRequest
	}{
		{
			description: "Valid",

			grpc: &pb.Access_Request_Permission{
				Operation:  pb.Access_Request_Permission_DELETE,
				Accessible: pb.Access_Request_Permission_MODEL,
				Identifier: "model0",
				Uid:        "123",
				Role:       "role0",
			},

			ceph: AccessRequest{
				Operation:  OperationDelete,
				EntityType: EntityTypeModel,
				Identifier: "model0",
				UID:        "123",
				Role:       "role0",
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)
		require.Equal(t, test.ceph, accessRequestFromGrpc(test.grpc))
	}
}

func Test_AccessControlErrorsShouldBeCreatableByType(t *testing.T) {

	noCred := NewAccessControlErrNoCredentials("%d", 0)
	require.Equal(t, "0", noCred.Error())

	invalidCred := NewAccessControlErrInvalidCredentials("%d", 1)
	require.Equal(t, "1", invalidCred.Error())

	notAuth := NewAccessControlErrInvalidCredentials("%d", 2)
	require.Equal(t, "2", notAuth.Error())
}
