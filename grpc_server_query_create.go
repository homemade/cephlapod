package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

type grpcCreateQuery struct {
	server *grpcServer
	model  Model
}

func newGrpcCreateQuery(server *grpcServer, model Model) WriteQuery {

	return &grpcCreateQuery{
		server: server,
		model:  model,
	}
}

func (q *grpcCreateQuery) Execute() error {

	jsn, err := q.model.MarshalJSON()

	if err != nil {
		return fmt.Errorf("Failed to marshal JSON: %s", err)
	}

	response, err := q.server.executeIQSQuery(&pb.Query_Input{
		Type: pb.Query_Input_CREATE,
		Payload: &pb.Query_Input_Create_{
			Create: &pb.Query_Input_Create{
				Model:   q.model.Name(),
				Content: jsn,
			},
		},
	}, 1)

	if err != nil {
		return fmt.Errorf("Query failed: %s", err)
	}

	recordSet := response[0].GetRecord()

	if recordSet == nil {
		return fmt.Errorf("Response record is nil")
	}

	if got, expected := recordSet.Model, q.model.Name(); got != expected {
		return fmt.Errorf("Create method gave model name %s, expected %s", got, expected)
	}

	if got, expected := len(recordSet.Records), 1; got != expected {
		return fmt.Errorf("Expected %d records in create method, got %d", got, expected)
	}

	return q.model.UnmarshalJSON(recordSet.Records[0])
}
