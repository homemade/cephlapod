package cephlapod

import (
	"net/http"
	"os"
)

/*
FileHandler is convenience wrapper for serving nominated files
under given URLs. It's generally used with Server.URL(). For example:

	 server, err := cephlapod.NewGrpcServer()

	 if err != nil {
		 // handle fatal error
	 }

	 server.URL("/my-path/my-image.jpg", cephlapod.FileHandler("path/to/image.jpg"))

	 server.Serve()

If the file doesn't exist, then a 404 error will be returned. If another error
occurs, it will trigger a general plugin error (which in turn will usually
return a 500 error) and issue an error in the log
*/
func FileHandler(path string) HTTPHandlerFunc {
	return func(c Context, r *http.Request, w http.ResponseWriter) error {

		info, err := os.Stat(path)

		if err != nil {
			if os.IsNotExist(err) {
				w.WriteHeader(http.StatusNotFound)
				return nil
			}

			return err
		}

		file, err := os.Open(path)

		if err != nil {
			return err
		}

		http.ServeContent(w, r, file.Name(), info.ModTime(), file)

		return nil
	}
}
