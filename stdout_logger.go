package cephlapod

import (
	"fmt"
	"strings"
)

type stdoutPrintFn func(...interface{}) (int, error)
type stdoutLogger struct {
	fn stdoutPrintFn
}

/*
NewStdoutLogger returns a Logger that writes to the standard output stream. Sep
reads from the plugin process's standard out in order to capure logs, so this
is the default logging mechanism.
*/
func NewStdoutLogger() Logger {
	return &stdoutLogger{fmt.Println}
}

func (l *stdoutLogger) Log(message string, args ...interface{}) {
	l.fn(strings.TrimRight(fmt.Sprintf(message, args...), "\n") + "\n")
}
