package cephlapod

/*
A HookScope represents the accessibility of a hook service. The accepted values
are defined as constants:

    ScopePublic    = 0
    ScopeProtected = 1
    ScopePrivate   = 2

Within any service there are three categories of function: public, protected
and private. The distinction between the categories is their access from
different parts of Sepia: public functions may be accessed by any part of the
system, including the Angular.js standard library’s hook system; protected
functions may be accessed by the server or the frontend application if it has
permission; private functions may only be accessed by the server.

Generally speaking, public functions are for non-sensitive operations like
checking if a username already exists or validating and address, and private
functions are for sensitive operations like making a record on a third party
system after a model has been created, or sending an email on behalf of a
client. Protected will fall somewhere in between, as you might imagine, and
isn’t yet implemented in Sepia.
*/
type HookScope int32

/*
A HookFunc is a hook callback function that will be invoked whenever a request
is made to a registered hook endpoint in Sepia. The function doesn't return any
errors; if some error occurs, it must be registered with the HookResponse
pointer that is passed to the function. Panics will be caught by the underlying
plugin engine and returned to Sepia automatically.

When a hook function is registered, a whitelist of model names may optionally
be passed. If one or more names is passed, then only models with a name on the
list will be served by the handler; others will rejected with an error message.
*/
type HookFunc func(Context, Logger, HookRequest, *HookResponse)

type hook struct {
	scope          HookScope
	fn             HookFunc
	modelWhitelist []string
}
