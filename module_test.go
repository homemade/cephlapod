package cephlapod

import (
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AModuleCanBeCreated(t *testing.T) {

	m := NewModule()

	require.NotNil(t, m)
	require.NotNil(t, m.View.Config)
}

func Test_AModuleCanBeExtractedFromAGrpcModule(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input pb.SetupConfigResponse_Module

		// Outputs
		output *Module
		err    error
	}{
		{
			description: "Valid, empty config",

			input: pb.SetupConfigResponse_Module{
				Config: []byte(`{}`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`{}`),
				},
			},

			output: &Module{
				Config: map[string]interface{}{},
				View: ModuleView{
					Config: map[string]interface{}{},
				},
				Model: map[string]ModuleModelQuery{},
			},
		},

		{
			description: "Invalid, no config",

			input: pb.SetupConfigResponse_Module{
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`{`),
				},
			},

			output: &Module{
				Config: map[string]interface{}{},
				View: ModuleView{
					Config: map[string]interface{}{},
				},
				Model: map[string]ModuleModelQuery{},
			},

			err: fmt.Errorf("View JSON decode failed: unexpected end of JSON input"),
		},

		{
			description: "Invalid, no view config",

			input: pb.SetupConfigResponse_Module{
				View: &pb.SetupConfigResponse_Module_View{},
			},

			output: &Module{
				Config: map[string]interface{}{},
				View: ModuleView{
					Config: map[string]interface{}{},
				},
				Model: map[string]ModuleModelQuery{},
			},

			err: fmt.Errorf("View JSON decode failed: unexpected end of JSON input"),
		},

		{
			description: "Invalid context",

			input: pb.SetupConfigResponse_Module{
				Config: []byte(`{`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`{}`),
				},
			},

			err: fmt.Errorf("JSON decode failed: unexpected end of JSON input"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := controllerFromGrpcModule(&test.input)

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, *test.output, *output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}

func Test_AGrpcModuleCanBeExtractedFromAModule(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input *Module

		// Outputs
		output *pb.SetupConfigResponse_Module
		err    error
	}{

		{
			description: "Valid",

			input: &Module{
				Controller: "controller",
				Config: map[string]interface{}{
					"one": "two",
				},
				View: ModuleView{
					Template: "template",
					Theme:    "theme",
					JS:       []string{"one", "two"},
					CSS:      []string{"three", "four"},
					Config: map[string]interface{}{
						"one": "two",
					},
				},
				Model: map[string]ModuleModelQuery{
					"q0": {
						Model: "test",
						Filters: []ModuleModelQueryFilters{
							{
								Field:    "f0",
								Operator: OperatorEQ,
								Value:    0,
							},
						},
						Sortations: []ModuleModelQuerySortation{
							{
								Field: "f1",
								Order: SortAscending,
							},
						},
						Limit: ModuleModelQueryLimit{
							Page:     1,
							PageSize: 20,
						},
						Fields:   []string{"f0", "f1", "f2"},
						Required: true,
					},
				},
			},

			output: &pb.SetupConfigResponse_Module{
				Controller: "controller",
				Config:     []byte(`{"one":"two"}`),
				View: &pb.SetupConfigResponse_Module_View{
					Template: "template",
					Theme:    "theme",
					Config:   []byte(`{"one":"two"}`),
					Js:       []string{"one", "two"},
					Css:      []string{"three", "four"},
				},
				Model: []*pb.SetupConfigResponse_Module_ModelQuery{
					{
						Name:  "q0",
						Model: "test",
						Filters: map[string]*pb.Query_Input_Read_Filter{
							"f0": &pb.Query_Input_Read_Filter{
								Operator: pb.Query_Input_Read_Filter_EQ,
								Value:    []byte(`0`),
							},
						},
						Sortations: []*pb.Query_Input_Read_Sortation{
							{
								Field: "f1",
								Order: pb.Query_Input_Read_Sortation_ASCENDING,
							},
						},
						Limit: &pb.Query_Input_Read_Limit{
							Page:     1,
							PageSize: 20,
						},
						Required: true,
					},
				},
			},
		},

		{
			description: "Valid, nil views",

			input: &Module{
				Controller: "controller",
			},

			output: &pb.SetupConfigResponse_Module{
				Controller: "controller",
				Config:     []byte(`null`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`null`),
				},
			},
		},

		{
			description: "Valid, no view",

			input: &Module{
				Controller: "controller",
				Config: map[string]interface{}{
					"one": "two",
				},
			},

			output: &pb.SetupConfigResponse_Module{
				Controller: "controller",
				Config:     []byte(`{"one":"two"}`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`null`),
				},
			},
		},

		{
			description: "Invalid config",

			input: &Module{
				Controller: "controller",
				Config: map[string]interface{}{
					"one": func() {},
				},
				View: ModuleView{
					Config: map[string]interface{}{
						"one": "two",
					},
				},
			},

			output: &pb.SetupConfigResponse_Module{
				Controller: "controller",
				Config:     []byte(`{"one":"two"}`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`{"one":"two"}`),
				},
			},

			err: fmt.Errorf("JSON encode failed: json: unsupported type: func()"),
		},

		{
			description: "Invalid view config",

			input: &Module{
				Controller: "controller",
				Config: map[string]interface{}{
					"one": "two",
				},
				View: ModuleView{
					Config: map[string]interface{}{
						"one": func() {},
					},
				},
			},

			output: &pb.SetupConfigResponse_Module{
				Controller: "controller",
				Config:     []byte(`{"one":"two"}`),
				View: &pb.SetupConfigResponse_Module_View{
					Config: []byte(`{"one":"two"}`),
				},
			},

			err: fmt.Errorf("View JSON encode failed: json: unsupported type: func()"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := test.input.grpcModule()

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}
