package cephlapod

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

/*
A ModelFieldDefinition represents a field in a model, including its public
label, validation rules and processing hooks. For full documentation on how
models in Sepia work, see the Sepia documentation.
*/
type ModelFieldDefinition struct {
	Label       string                                    `json:"label"`
	Type        string                                    `json:"type"`
	Validation  map[string]ModelFieldValidationDefinition `json:"validation_rules"`
	Hooks       map[string]ModelHookDefinition            `json:"hooks"`
	Permissions ReadWritePermission                       `json:"permissions"`
}

/*
NewModelFieldDefinition creates a new ModelFieldDefinition, with allocated memory.
*/
func NewModelFieldDefinition() *ModelFieldDefinition {
	return &ModelFieldDefinition{
		Validation: make(map[string]ModelFieldValidationDefinition),
		Hooks:      make(map[string]ModelHookDefinition),
	}
}

func grpcModelFieldValidationDefinition(input map[string]ModelFieldValidationDefinition) (map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig, error) {

	output := make(map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig)

	for rule, config := range input {

		jsn, err := json.Marshal(config)

		if err != nil {
			return nil, fmt.Errorf("Failed to marshall config for validation rule %s: %s", rule, err)
		}

		output[rule] = &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{Config: jsn}
	}

	return output, nil
}

func (f *ModelFieldDefinition) grpcModelField() (*pb.SetupConfigResponse_Model_Field, error) {

	field := &pb.SetupConfigResponse_Model_Field{
		Label:      f.Label,
		Type:       f.Type,
		Validation: make(map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig),
		Hooks:      make(map[string]*pb.SetupConfigResponse_Model_Hook),
		Permissions: &pb.SetupConfigResponse_Model_Field_Permissions{
			Read:  f.Permissions.Read.grpc(),
			Write: f.Permissions.Write.grpc(),
		},
	}

	var err error
	if field.Validation, err = grpcModelFieldValidationDefinition(f.Validation); err != nil {
		return nil, err
	}

	for trigger, hookInput := range f.Hooks {

		hookOutput := &pb.SetupConfigResponse_Model_Hook{
			Hooks: make(map[string]*pb.SetupConfigResponse_Model_Hook_HookAction),
		}

		for service, action := range hookInput {
			hookOutput.Hooks[service] = &pb.SetupConfigResponse_Model_Hook_HookAction{
				Async:     action.Async,
				Functions: action.Functions,
				Priority:  action.Priority,
			}
		}

		field.Hooks[trigger] = hookOutput
	}

	return field, nil
}

/*
A ModelFieldValidationDefinition is a set of configuration for a validation
rule. Since validation rules can accept any type of configuration value
(they're expressed as JavaScript, and it's down to the rule itself to figure
out valid configuration) the ModelFieldValidationDefinition type is a map of
strings to interfaces.

An example model definition, including a validation config, is as follows:

	 model := &ModelDefinition{
		Fields: map[string]*ModelFieldDefinition{
			"field0": &ModelFieldDefinition{
				Label: "label0",
				Type: "any",
				Validation: map[string]ModelFieldValidationDefinition{
					"rule0": ModelFieldValidationDefinition{
						"one": "two",
					},
				},
				Hooks: map[string]ModelHookDefinition{
					"ui:on-blur": ModelHookDefinition{
						"service0": &ModelHookActionDefinition{
							Functions: []string{"fn0"},
							Async:     true,
							Priority:  999,
						},
					},
				},
			},
		}
	}
*/
type ModelFieldValidationDefinition map[string]interface{}

/*
NewModelFieldValidationDefinition creates a new ModelFieldValidationDefinition,
with allocated memory.
*/
func NewModelFieldValidationDefinition() ModelFieldValidationDefinition {
	return make(ModelFieldValidationDefinition)
}

/*
A ModelHookDefinition represents the hook actions to be invoked on a particular
trigger, usually in a model field definition. The map key is the name of the
trigger, and the value is a *ModelHookActionDefinition describing the action
to be taken.

An example model definition, including a frontend hook, is as follows:

	 model := &ModelDefinition{
		Fields: map[string]*ModelFieldDefinition{
			"field0": &ModelFieldDefinition{
				Label: "label0",
				Type: "any",
				Validation: map[string]ModelFieldValidationDefinition{
					"rule0": ModelFieldValidationDefinition{
						"one": "two",
					},
				},
				Hooks: map[string]ModelHookDefinition{
					"ui:on-blur": ModelHookDefinition{
						"service0": &ModelHookActionDefinition{
							Functions: []string{"fn0"},
							Async:     true,
							Priority:  999,
						},
					},
				},
			},
		}
	}
*/
type ModelHookDefinition map[string]*ModelHookActionDefinition

/*
NewModelHookDefinition returns a ModelHookDefinition with allocated memory.
*/
func NewModelHookDefinition() ModelHookDefinition {
	return make(map[string]*ModelHookActionDefinition)
}

/*
A ModelHookActionDefinition describes the action to be taken when a hook is
triggered. Any number of 'functions' may be called (what a funtion is depends
on the type of hook transport being used, the three most common of which are
web hooks, AWS lambda functions and other plugins) and the action may be
triggered synchronously or asychronously. Actions are called in descending
order of their priority.
*/
type ModelHookActionDefinition struct {
	Functions []string `json:"functions"`
	Async     bool     `json:"async"`
	Priority  int32    `json:"priority"`
}

/*
NewModelHookActionDefinition returns a pointer to a new ModelHookActionDefinition.
*/
func NewModelHookActionDefinition() *ModelHookActionDefinition {
	return &ModelHookActionDefinition{}
}

/*
A ModelDefinition reflects the Sepia concept of models, and is used mainly with
Server.Model() to register new models that will be usable in the Sepia
application to which the plugin is attached.

Sepia uses a fairly simple MVC pattern to help implement user interfaces, and
it uses the concept of models from that pattern to store data and define
system-level interactions like the REST API endpoints and actions to take when
certain events, relating to models, occur in the system (in Sepia, this is
called the ‘hook’ system, and the events are called ‘triggers’ — see Sepia
documentation).  For the purposes of storing data, a model is essentially a
collection of fields, which are discreet entities that store a particular,
atomic kind of information, and can validate themselves in isolation and even
trigger external events to validate or update them in a more complex way.
*/
type ModelDefinition struct {
	Fields      map[string]*ModelFieldDefinition `json:"fields"`
	Hooks       map[string]ModelHookDefinition   `json:"hooks"`
	Apis        map[string]*API                  `json:"apis"`
	Permissions CRULDPermission                  `json:"permissions"`
}

/*
NewModelDefinition returns a pointer to a ModelDefinition object with allocated
memory.
*/
func NewModelDefinition() *ModelDefinition {
	return &ModelDefinition{
		Fields: make(map[string]*ModelFieldDefinition),
		Hooks:  make(map[string]ModelHookDefinition),
		Apis:   make(map[string]*API),
	}
}

/*
Grpc returns a pointer ito a gRPC model definition, which can be used to transfer
data over the wire. This is deprecated, and will soon be removed.
*/
func (m *ModelDefinition) Grpc() (*pb.SetupConfigResponse_Model, error) {

	output := &pb.SetupConfigResponse_Model{
		Fields: make(map[string]*pb.SetupConfigResponse_Model_Field),
		Hooks:  make(map[string]*pb.SetupConfigResponse_Model_Hook),
		Apis:   make(map[string]*pb.SetupConfigResponse_Api),
		Permissions: &pb.SetupConfigResponse_Model_Permissions{
			Create: m.Permissions.Create.grpc(),
			Read:   m.Permissions.Read.grpc(),
			Update: m.Permissions.Update.grpc(),
			List:   m.Permissions.List.grpc(),
			Delete: m.Permissions.Delete.grpc(),
		},
	}

	for k, v := range m.Fields {

		field, err := v.grpcModelField()

		if err != nil {
			return nil, fmt.Errorf("Field %s: %s", k, err)
		}

		output.Fields[k] = field
	}

	for trigger, hookInput := range m.Hooks {

		hookOutput := &pb.SetupConfigResponse_Model_Hook{
			Hooks: make(map[string]*pb.SetupConfigResponse_Model_Hook_HookAction),
		}

		for service, action := range hookInput {
			hookOutput.Hooks[service] = &pb.SetupConfigResponse_Model_Hook_HookAction{
				Async:     action.Async,
				Functions: action.Functions,
				Priority:  action.Priority,
			}
		}

		output.Hooks[trigger] = hookOutput
	}

	for name, api := range m.Apis {
		output.Apis[name] = api.grpcAPI()
	}

	return output, nil
}

func modelFieldDefinitionFromGrpcModelField(input *pb.SetupConfigResponse_Model_Field) (*ModelFieldDefinition, error) {
	output := NewModelFieldDefinition()

	// Store the label and type
	output.Label = input.Label
	output.Type = input.Type

	// Validation rules
	for rule, config := range input.Validation {

		validation := NewModelFieldValidationDefinition()

		if err := json.Unmarshal(config.Config, &validation); err != nil {
			return nil, fmt.Errorf("Failed to unmarshal config for validation rule %s: %s", rule, err)
		}

		output.Validation[rule] = validation
	}

	// Hooks
	for trigger, hookInput := range input.Hooks {
		hookOutput := NewModelHookDefinition()

		for service, action := range hookInput.Hooks {
			hookOutput[service] = &ModelHookActionDefinition{
				Async:     action.Async,
				Functions: action.Functions,
				Priority:  action.Priority,
			}
		}

		output.Hooks[trigger] = hookOutput
	}

	return output, nil
}

func grpcModelToModelDefinition(input *pb.SetupConfigResponse_Model) (*ModelDefinition, error) {

	output := NewModelDefinition()

	// Fields
	for name, field := range input.Fields {

		def, err := modelFieldDefinitionFromGrpcModelField(field)

		if err != nil {
			return nil, err
		}

		output.Fields[name] = def
	}

	// Hooks
	for trigger, hookInput := range input.Hooks {
		hookOutput := NewModelHookDefinition()

		for service, action := range hookInput.Hooks {
			hookOutput[service] = &ModelHookActionDefinition{
				Async:     action.Async,
				Functions: action.Functions,
				Priority:  action.Priority,
			}
		}

		output.Hooks[trigger] = hookOutput
	}

	// API
	for name, api := range input.Apis {
		output.Apis[name] = apiFromGrpcAPI(api)
	}

	return output, nil
}
