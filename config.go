package cephlapod

/*
A ConfigFunc is a callback function used by Server.ValidateConfig. It allows
the plugin developer to check that the Sepia application developer has passed
all the required configuration, and to modify the configuration as needed.
*/
type ConfigFunc func(Context, Logger, ConfigMap) error

/*
A ConfigMap is a simple map type that associated configuration field with values.
In practise, this will contain the same keys and values as the config section
of the sepia.scl where the plugin is defined.

Note that though a ConfigMap is passed to every callback in a Context, it
shouldn't be modified outside of the a Server.ValidateConfig callback! Doing
that at high load will cause concurrent read/write errors.
*/
type ConfigMap map[string]string

/*
NewConfigMap is a convenience function to make a new map.
*/
func NewConfigMap() ConfigMap {
	return make(ConfigMap)
}
