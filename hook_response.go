package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

/*
A HookResponse object is passed to hook callback functions. All the fields
in the object are passed back to Sepia over the wire, and are used to evaluate
the outcome of the hook request. There are convience functions for storing
general errors and field errors, but they are not required.
*/
type HookResponse struct {
	Model       Model
	FieldErrors map[string][]string
	Errors      []string
}

/*
NewHookResponse creates a HookResponse with allocated memory.
*/
func NewHookResponse(model Model) *HookResponse {
	return &HookResponse{
		Model:       model,
		FieldErrors: make(map[string][]string),
		Errors:      []string{},
	}
}

/*
FieldError registers errors on a field. Field errors are for frontend UI hooks,
where the end user wants immediate response to the value of a field. A typical
implementation of this would be data validation. When the function returns, the
field error is usually sent back to Sepia so that it can be displayed alongside
the field.
*/
func (h *HookResponse) FieldError(name, err string, args ...interface{}) {

	if _, exists := h.FieldErrors[name]; !exists {
		h.FieldErrors[name] = []string{}
	}

	h.FieldErrors[name] = append(h.FieldErrors[name], fmt.Sprintf(err, args...))
}

/*
General errors should be registered against the HookResponse object. When a hook request is triggered by a 'before' internal server hook in Sepia (that is, before a model creation or save), error will prevent the save operation; when triggered by a frontend UI hook, the error will be added to the list of serious form errors. In the latter case, it's best to use FieldError() where possible, because it's more likely the end user will see the error message (see below).
*/
func (h *HookResponse) Error(err string, args ...interface{}) {

	h.Errors = append(h.Errors, fmt.Sprintf(err, args...))
}

func (h *HookResponse) externalServiceResponse() (*pb.ExternalServiceResponse, error) {

	resp := &pb.ExternalServiceResponse{
		Errors:      h.Errors,
		FieldErrors: make(map[string]*pb.ExternalServiceResponse_FieldErrors),
	}

	var err error
	resp.Entity, err = h.Model.MarshalJSON()

	if err != nil {
		return nil, err
	}

	for k, v := range h.FieldErrors {
		resp.FieldErrors[k] = &pb.ExternalServiceResponse_FieldErrors{Error: v}
	}

	return resp, nil
}
