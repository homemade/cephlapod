package cephlapod_test

import (
	"fmt"
	"net/http"

	"bitbucket.org/homemade/cephlapod"
)

/*
A server must call its (blocking) Serve() function in order to receive requests.
*/
func ExampleNewGrpcServer_basic() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.Serve()
}

/*
A server can be stopped with its Stop() function at any time.
*/
func ExampleNewGrpcServer_stopping() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	go server.Serve()

	// Do something else

	server.Stop()
}

/*
A server must call its (blocking) Serve() function in order to receive requests.
*/
func ExampleServer_serve() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.Serve()
}

/*
A server can be stopped with its Stop() function at any time.
*/
func ExampleServer_stop() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	go server.Serve()

	// Do something else

	server.Stop()
}

/*
General errors should be registered against the HookResponse object. When a
hook request is triggered by a 'before' internal server hook in Sepia (that is,
before a model creation or save), error will prevent the save operation; when
triggered by a frontend UI hook, the error will be added to the list of serious
form errors. In the latter case, it's best to use FieldError() where possible,
because it's more likely the end user will see the error message (see below).
*/
func ExampleHookFunc_error() {
	server, _ := cephlapod.NewGrpcServer()

	hookFunc := func(ctx cephlapod.Context, logger cephlapod.Logger, req cephlapod.HookRequest, resp *cephlapod.HookResponse) {
		resp.Error("This is a general error, which will be sent to Sepia")
	}

	server.HandleHook("Hook", hookFunc, cephlapod.ScopePublic)

	server.Serve()
}

/*
Field errors are for frontend UI hooks, where the end user wants immediate
response to the value of a field. A typical implementation of this would be
data validation. When the function returns, the field error is usually sent
back to Sepia so that it can be displayed alongside the field.
*/
func ExampleHookFunc_FieldError() {
	server, _ := cephlapod.NewGrpcServer()

	hookFunc := func(ctx cephlapod.Context, logger cephlapod.Logger, req cephlapod.HookRequest, resp *cephlapod.HookResponse) {
		resp.FieldError("someFieldName", "This field is invalid")
	}

	server.HandleHook("Hook", hookFunc, cephlapod.ScopePublic)

	server.Serve()
}

func ExampleHookResponse_Error() {
	server, _ := cephlapod.NewGrpcServer()

	hookFunc := func(ctx cephlapod.Context, logger cephlapod.Logger, req cephlapod.HookRequest, resp *cephlapod.HookResponse) {
		resp.Error("This is a general error, which will be sent to Sepia")
	}

	server.HandleHook("Hook", hookFunc, cephlapod.ScopePublic)

	server.Serve()
}

/*
Files can be served over the wire using the FileHandler utility
function in conjunction with Server.Url(). The root path specified
in the Sepia config will automatically be applied.
*/
func ExampleServer_url() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.URL("/my-path/my-image.jpg", cephlapod.FileHandler("path/to/image.jpg"))

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
Sepia provides an Internal Query Service (IQS) which allows plugins to extract
and manpulate models stored in Sepia's datastore. The service uses a CRUD
architecture and is able to obtain the complete data model.

The IQS is similar to the REST API that Sepia provides by default, except that
it's not subject to auth (plugins are assumed to be trusted entities). Normal
validation rules and hooks apply to model changes.

Note that the Internal Query Servce is not available immediately; there is
usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at
that time.
*/
func ExampleServer_queries() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		modelName := "my_model"

		////////////////////////////////////////////////////////////////////////////////
		// The data model can be read; check that out model is in it
		////////////////////////////////////////////////////////////////////////////////

		dataModel, err := server.NewDataModelQuery().Execute()

		if err != nil {
			logger.Log("Failed to get dataModel: %s", err)
			return
		}

		if _, ok := dataModel[modelName]; !ok {
			logger.Log("Couldn't find an entry for %s in the data model", modelName)
			return
		}

		////////////////////////////////////////////////////////////////////////////////
		// Create a new record with valid fields. If the field values fail validation,
		// the Execute() function will return an error.
		////////////////////////////////////////////////////////////////////////////////

		record := cephlapod.NewModel(modelName)

		record.Set("field1", "A")
		record.Set("field2", 1)

		if err := server.NewCreateQuery(record).Execute(); err != nil {
			logger.Log("Failed to create: %s", err)
			return
		}

		////////////////////////////////////////////////////////////////////////////////
		// Read back the record that we just created
		////////////////////////////////////////////////////////////////////////////////

		id, err := record.Get("id")

		if err != nil {
			logger.Log("Failed to get id: %s", err)
			return
		}

		results, err := server.NewReadQuery(modelName).Where("id", cephlapod.OperatorEQ, id).Execute()

		if err != nil {
			logger.Log("Failed to get read results: %s", err)
			return
		}

		////////////////////////////////////////////////////////////////////////////////
		// Update a field. It's possible that hooks on the model save actions may update
		// the field values.
		////////////////////////////////////////////////////////////////////////////////

		results[0].Set("field2", 2)

		if err := server.NewUpdateQuery(id.(string), results[0]).Execute(); err != nil {
			logger.Log("Updating the ield failed: %s", err)
			return
		}

		////////////////////////////////////////////////////////////////////////////////
		// Write the record out to the logger.
		////////////////////////////////////////////////////////////////////////////////

		logger.Log("Final record: %+v", results[0].Map())

		////////////////////////////////////////////////////////////////////////////////
		// Delete the record
		////////////////////////////////////////////////////////////////////////////////

		if err := server.NewDeleteQuery(modelName, id.(string)).Execute(); err != nil {
			logger.Log("Delete failed: %s", err)
			return
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
QueryEngineReadyFunc can be used in conjunction with QueryEngineReady.
*/
func ExampleQueryEngineReadyFunc() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {
		logger.Log("The query engine is ready!")
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
There is a baked-in read query type for all Servers. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at that time.
*/
func ExampleReadQuery() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		query := server.NewReadQuery("my_model").
			Select("myfield1", "myfield2").
			Where("myfield1", cephlapod.OperatorEQ, 1.0).
			Limit(1, 10).
			Sort("myfield2", cephlapod.SortAscending)

		results, err := query.Execute()

		if err != nil {
			logger.Log("Some error occurred: %s", err)
		}

		for i, result := range results {
			logger.Log("[result %d] %+v", i, result)
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
There is a baked-in create query type for all Servers. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at that time.
*/
func ExampleWriteQuery_create() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		record := cephlapod.NewModel("my_model")

		record.Set("field1", "A")
		record.Set("field2", 1)

		if err := server.NewCreateQuery(record).Execute(); err != nil {
			logger.Log("Failed to create: %s", err)
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
There is a baked-in update type for all Servers. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at that time.
*/
func ExampleWriteQuery_update() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		model := cephlapod.NewModel("my_model")
		model.Set("field0", 1)

		if err := server.NewUpdateQuery("some-id", model).Execute(); err != nil {
			logger.Log("Updating the ield failed: %s", err)
			return
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
There is a baked-in delete type for all Servers. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at that time.
*/
func ExampleWriteQuery_delete() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		if err := server.NewDeleteQuery("my_model", "123").Execute(); err != nil {
			logger.Log("Delete failed: %s", err)
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
There is a baked-in datamodel type for all Servers. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at that time.
*/
func ExampleDataModelQuery() {
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		dataModel, err := server.NewDataModelQuery().Execute()

		if err != nil {
			logger.Log("Failed to get dataModel: %s", err)
			return
		}

		myModelSchema, ok := dataModel["my_model"]

		if !ok {
			logger.Log("Couldn't find an entry for my_model in the data model")
			return
		}

		logger.Log("%+v", myModelSchema)
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

/*
Plugins have access to the same file systems as the main Sepia binary,
including any files embedded in the sep binary itself, and any remote systems,
using the IQS. Note that the Internal Query Servce is not available
immediately; there is usually a short delay, in the order of milliseconds,
before queries can be executed. The QueryEngineReady() function allows for a
callback to be fired at that time.

Calls to Watch() can be made at any time, but they won't do anything
until the IQS is ready. Calls to Unwatch require the same arguments in the
same order as the original Watch() call.
*/
func ExampleServer_files() {

	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	server.QueryEngineReady(func(ctx cephlapod.Context, logger cephlapod.Logger) {

		// Get a list of files
		files, err := server.FileGlob("some/path/*.txt")

		if err != nil {
			// handle fatal error
		}

		// Read the first file's contents
		contents, err := server.FileRead(files[0])

		if err != nil {
			// handle fatal error
		}

		logger.Log("The contents of %s is %s!", files[0], contents)

		// Watch a bunch of files, and log a message when they change
		if err := server.FileWatch(
			func(logger cephlapod.Logger, files []string, change cephlapod.FileChange, path string) {
				logger.Log("%s has been %s! Full file list is now %+v", path, change, files)
			},
			"some/other/path/*.txt",
		); err != nil {
			// handle fatal error
		}
	})

	// Nothing will happen unless the server is running!
	server.Serve()
}

type thirdPartyAuthSystem struct{}

func (t thirdPartyAuthSystem) FindCredentialsFromRequest(*http.Request) (interface{}, error) {
	return nil, nil
}

func (t thirdPartyAuthSystem) Authenticate(interface{}) (interface{}, error) {
	return nil, nil
}

func (t thirdPartyAuthSystem) Authorise(interface{}, interface{}) error {
	return nil
}

func connectToThirdpartyAuthSystem() (thirdPartyAuthSystem, error) {
	return thirdPartyAuthSystem{}, nil
}

/*
Plugins can provide access control functionality over HTTP using
Server.HTTPAccessControl() to register an access control callback function.
When a function is registered, properly configured Sepia applications will send
access requests to the plugin, which will be translated by cephlapod into
useful Go structs, and routed to the callback function. See the documentation
for HTTPAccessControlFunc for full details of how to write a suitable function.
*/
func ExampleServer_accessControlOverHTTP() {

	// Set up a server
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	// Register a callback function to handle access requests.
	server.HTTPAccessControl(

		/*
			This is the callback function.

			the appropriate credentials have been provided in the HTTP request) and
			authorise any access requests. For efficiency, Sepia batches access request
			operations, so it's possible that the callback function may receive several
			separate access requests, all relating to a single HTTP request. Every access
			request includes as much meta information as possible about the system entity.

			The *http.Request and http.ResponseWriter may be used as normal. Headers and
			content sent to the http.ResponseWriter will be proxied out to the end user's
			client.

			A return value of nil indicates that the http request has been authenticated,
			and all provided access requests have been authorised. There are specific error
			types for other kinds of failure:

			AccessControlErrNoCredentials indicates an authentication failure due to lack
			of credentials.

			AccessControlErrInvalidCredentials indicated an authentication failure due to
			invalid or expired credentials.

			AccessControlErrNotAuthorised indicates an authorisation failure. One or more
			of the access requests is not permitted.

			Any other kind of error is treated as a system error, which generally results
			in a 500 Internal Server Error response.

			This example is slightly contrived in order to show the full range of error
			handling possibilities. Your own implementation will probably be much more
			intuitive and streamlined.
		*/
		func(ctx cephlapod.Context, logger cephlapod.Logger, requests []cephlapod.AccessRequest, r *http.Request, w http.ResponseWriter) error {

			// In this example, we'll use a generic third party auth system which will
			// be connected to when required. You can imagine this as being a standin for
			// AWS Congnito, Auth0, Digits, or anything else.
			auth, err := connectToThirdpartyAuthSystem()

			// If any system-level failure occurs, return a generic error to Sepia.
			if err != nil {
				return fmt.Errorf("Failed to connect to auth provider: %s", err)
			}

			// First, check to see if any credentials were supplied in the HTTP request.
			// These could take the form of headers, cookies, request params, or anything else
			// that's possible in the scope of an HTTP request.
			credentials, err := auth.FindCredentialsFromRequest(r)

			// If there are no credentials, tell Sepia using a typed error. You could also
			// force an HTTP redirect to your login page here using http.Redirect().
			if err != nil {
				logger.Log("No credentials: %s", err)
				return cephlapod.NewAccessControlErrNoCredentials("No credentials supplied in the HTTP request")
			}

			// Get a user record out of the credentials.
			user, err := auth.Authenticate(credentials)

			// Again, report errors using the appropriate error type
			if err != nil {
				logger.Log("Invalid credentials: %s", err)
				return cephlapod.NewAccessControlErrInvalidCredentials("Supplied credentials are invalid")
			}

			// Check that each requested access permission can be authorised, and return at the first failure
			for _, request := range requests {
				if err := auth.Authorise(user, request); err != nil {
					logger.Log("Not authorised: %s", err)
					return cephlapod.NewAccessControlErrNotAuthorised("Not authorised")
				}
			}

			// The authorisation is successful, so return nil!
			return nil
		},
	)

	// Nothing will happen unless the server is running!
	server.Serve()
}

func ExampleHTTPAccessControlFunc() {

	// Set up a server
	server, err := cephlapod.NewGrpcServer()

	if err != nil {
		// handle fatal error
	}

	// Register a callback function to handle access requests.
	server.HTTPAccessControl(

		/*
			This is the callback function.  This example is slightly contrived in order to show the full range of error
			handling possibilities. Your own implementation will probably be much more
			intuitive and streamlined.
		*/
		func(ctx cephlapod.Context, logger cephlapod.Logger, requests []cephlapod.AccessRequest, r *http.Request, w http.ResponseWriter) error {

			// In this example, we'll use a generic third party auth system which will
			// be connected to when required. You can imagine this as being a standin for
			// AWS Congnito, Auth0, Digits, or anything else.
			auth, err := connectToThirdpartyAuthSystem()

			// If any system-level failure occurs, return a generic error to Sepia.
			if err != nil {
				return fmt.Errorf("Failed to connect to auth provider: %s", err)
			}

			// First, check to see if any credentials were supplied in the HTTP request.
			// These could take the form of headers, cookies, request params, or anything else
			// that's possible in the scope of an HTTP request.
			credentials, err := auth.FindCredentialsFromRequest(r)

			// If there are no credentials, tell Sepia using a typed error. You could also
			// force an HTTP redirect to your login page here using http.Redirect().
			if err != nil {
				logger.Log("No credentials: %s", err)
				return cephlapod.NewAccessControlErrNoCredentials("No credentials supplied in the HTTP request")
			}

			// Get a user record out of the credentials.
			user, err := auth.Authenticate(credentials)

			// Again, report errors using the appropriate error type
			if err != nil {
				logger.Log("Invalid credentials: %s", err)
				return cephlapod.NewAccessControlErrInvalidCredentials("Supplied credentials are invalid")
			}

			// Check that each requested access permission can be authorised, and return at the first failure
			for _, request := range requests {
				if err := auth.Authorise(user, request); err != nil {
					logger.Log("Not authorised: %s", err)
					return cephlapod.NewAccessControlErrNotAuthorised("Not authorised")
				}
			}

			// The authorisation is successful, so return nil!
			return nil
		},
	)

	// Nothing will happen unless the server is running!
	server.Serve()
}
