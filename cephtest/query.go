package cephtest

import "bitbucket.org/homemade/cephlapod"

// WriteQueryFunc is a callback for mock write queries. The passed model may be nil.
type WriteQueryFunc func(cephlapod.Model) error

// ReadQueryFunc is a callback for mock read queries
type ReadQueryFunc func(modelName string) ([]cephlapod.Model, error)

// DataModelQueryFunc is a callback for mock data model queries
type DataModelQueryFunc func() (map[string]cephlapod.ModelDefinition, error)

/////////////////////////////////////////////////////////////////
// Write
/////////////////////////////////////////////////////////////////

type mockWriteQuery struct {
	executor WriteQueryFunc
	model    cephlapod.Model
}

/*
NewMockWriteQuery creates a cephlapod.WriteQuery that calls a function when
Execute() is invoked, and can be used to similate create, update and delete
queries. The passed cephlapod.Model argument may be nil; if it is, the callback
receives a nil argument.
*/
func NewMockWriteQuery(model cephlapod.Model, fn WriteQueryFunc) cephlapod.WriteQuery {
	return &mockWriteQuery{
		executor: fn,
		model:    model,
	}
}

func (m *mockWriteQuery) Execute() error {
	return m.executor(m.model)
}

/////////////////////////////////////////////////////////////////
// Read
/////////////////////////////////////////////////////////////////

type mockReadQuery struct {
	executor  ReadQueryFunc
	modelName string
}

/*
NewMockReadQuery creates a cephlapod.ReadQuery that calls a function when
Execute() is invoked, and can be used to simulate read queries.
*/
func NewMockReadQuery(modelName string, fn ReadQueryFunc) cephlapod.ReadQuery {
	return &mockReadQuery{
		executor:  fn,
		modelName: modelName,
	}
}

func (m *mockReadQuery) Select(...string) cephlapod.ReadQuery                              { return m }
func (m *mockReadQuery) Where(string, cephlapod.Operator, interface{}) cephlapod.ReadQuery { return m }
func (m *mockReadQuery) Sort(string, cephlapod.Order) cephlapod.ReadQuery                  { return m }
func (m *mockReadQuery) Limit(page, pageSize int) cephlapod.ReadQuery                      { return m }

func (m *mockReadQuery) Execute() ([]cephlapod.Model, error) {
	return m.executor(m.modelName)
}

/////////////////////////////////////////////////////////////////
// Datamodel
/////////////////////////////////////////////////////////////////

type mockDataModelQuery struct {
	executor DataModelQueryFunc
}

/*
NewMockDataModelQuery creates a cephlapod.DataModelQuery that calls a function
when Execute() is invoked, and can be used to simulate data model queries.
*/
func NewMockDataModelQuery(fn DataModelQueryFunc) cephlapod.DataModelQuery {
	return &mockDataModelQuery{
		executor: fn,
	}
}

func (m *mockDataModelQuery) Execute() (map[string]cephlapod.ModelDefinition, error) {
	return m.executor()
}
