package cephtest

import (
	"log"
	"testing"

	"github.com/stretchr/testify/require"
)

func ExampleNewTestPlugin() {

	plugin, err := NewTestServer()

	if err != nil {
		log.Fatalf("Test plugin creation failed: %s", err)
	}

	go plugin.Serve()

	// ...do something...

	plugin.Stop()
}

func Test_ANewPluginCanBeCreated(t *testing.T) {

	p, err := NewTestServer()

	require.Nil(t, err)
	require.NotNil(t, p)

	p.Stop()
}
