package cephtest

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

/*
Logger writes to the testing log instead of the stdout, and also keeps a record
of all the logged messages. It implements the cephlapod.Logger interface, so it
can be used to test the log behaviour of plugin functionality.
*/
type Logger struct {
	t     *testing.T
	lines []string
}

/*
Log sends a message to the unit test output (visible with go test -v) and also
stores the message for later assertion
*/
func (l *Logger) Log(message string, args ...interface{}) {

	msg := fmt.Sprintf(message, args...)
	l.t.Log(msg)
	l.lines = append(l.lines, msg)
}

/*
Assert ensures that supplied messages were logged in order, and fails the unit
test if they don't match.

Example:

 func Test_SomeUnitTest(t *testing.T) {

     l := NewLogger(t)

     l.Log("Hello, %s", "world")

     l.Assert("Hello world") // Matches; no effect
     l.Assert("not logged")  // Causes the test to fail
 }
*/
func (l *Logger) Assert(lines ...string) {

	require.Equal(l.t, len(lines), len(l.lines))

	for i, line := range lines {
		require.Equal(l.t, line, l.lines[i])
	}
}

/*
NewLogger creates a Logger than will write to the testing log instead of the
stdout. This is very useful when writing tests for plugin functionality.
*/
func NewLogger(t *testing.T) *Logger {
	return &Logger{t, []string{}}
}
