package cephtest

import (
	"fmt"
	"net"
	"os"

	"github.com/Pallinder/go-randomdata"

	"bitbucket.org/homemade/cephlapod"
)

func openPort() (string, error) {

	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")

	if err != nil {
		return "", fmt.Errorf("Can't resolve TCP address: %s", err)
	}

	l, err := net.ListenTCP("tcp", addr)

	if err != nil {
		return "", fmt.Errorf("Can't listen on TCP address: %s", err)
	}

	defer l.Close()

	port := fmt.Sprintf("%d", l.Addr().(*net.TCPAddr).Port)

	return port, nil
}

/*
NewTestServer creates a new test plugin, which is a gRPC server running on a
random port. The plugin server behaves like any natural plugin, and will need
to be started and stopped as normal.
*/
func NewTestServer() (cephlapod.Server, error) {

	port, handle := os.Getenv(cephlapod.EnvPluginPort), os.Getenv(cephlapod.EnvPluginHandle)

	defer func() {
		os.Setenv(cephlapod.EnvPluginPort, port)
		os.Setenv(cephlapod.EnvPluginHandle, handle)
	}()

	tmpPort, err := openPort()

	if err != nil {
		return nil, err
	}

	os.Setenv(cephlapod.EnvPluginPort, tmpPort)
	os.Setenv(cephlapod.EnvPluginHandle, randomdata.SillyName())

	return cephlapod.NewGrpcServer()
}
