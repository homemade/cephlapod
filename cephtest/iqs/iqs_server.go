package iqs

import (
	"fmt"
	"net"
	"sort"
	"testing"
	"time"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/stretchr/testify/require"
	"google.golang.org/grpc"
)

func openPort() (string, error) {

	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")

	if err != nil {
		return "", fmt.Errorf("Can't resolve TCP address: %s", err)
	}

	l, err := net.ListenTCP("tcp", addr)

	if err != nil {
		return "", fmt.Errorf("Can't listen on TCP address: %s", err)
	}

	defer l.Close()

	port := fmt.Sprintf("%d", l.Addr().(*net.TCPAddr).Port)

	return port, nil
}

/*
GrpcServer represents an internal query server, much like the one that Sepia
presents to allows plugins to make queries against the database. When queried,
the GrpcIqsServer will response with any query results that have been added to
it programmatically.

Files can be written and deleted to a virtual file system. File lists and
contents will be returned in file read and file glob queries as is (ie. all
files in alphabetical order; no actual globbing will occur).
*/
type GrpcServer interface {
	pb.ApplicationServer
	Attach(server pb.SetupServer) error
	AddQueryResult(response *pb.Query_Result)
	FileSystem(files map[string][]byte)
	WriteFile(name string, content []byte)
	DeleteFile(name string)
	Serve() (address string)
	Stop() error
}

type grpcServer struct {
	server     *grpc.Server
	responses  []*pb.Query_Result
	t          *testing.T
	files      map[string][]byte
	fileEvents chan *pb.FileGlob_Output
	addr       string
}

/*
NewGrpcServer starts a mock gRPC internal query server on a random port. Any
pre-programmed responses can be added as optional arguments at creation time, or
later with the AddResponse() function. When the program terminates, the server will
stop, and it can be explicitly stopped in the lifetime of the program by using the
Stop() function.
*/
func NewGrpcServer(t *testing.T, responses ...*pb.Query_Result) (server GrpcServer, address string) {

	s := &grpcServer{
		server:     grpc.NewServer(),
		responses:  responses,
		t:          t,
		files:      make(map[string][]byte),
		fileEvents: make(chan *pb.FileGlob_Output, 100),
	}

	return s, s.Serve()
}

func (s *grpcServer) Attach(server pb.SetupServer) error {
	_, err := server.InternalQueryServerConfig(context.Background(), &pb.IQSConfigRequest{s.addr})
	return err
}

func (s *grpcServer) AddQueryResult(response *pb.Query_Result) {
	s.responses = append(s.responses, response)
}

func (s *grpcServer) FileSystem(files map[string][]byte) {
	s.files = files
}

func (s *grpcServer) WriteFile(name string, content []byte) {

	event := &pb.FileGlob_Output{
		Type: pb.FileGlob_Output_CHANGE,
		Payload: &pb.FileGlob_Output_Change_{
			Change: &pb.FileGlob_Output_Change{
				Path: name,
				Type: pb.FileGlob_Output_Change_CHANGE,
			},
		},
	}

	if _, exists := s.files[name]; exists {
		event.GetChange().Type = pb.FileGlob_Output_Change_CHANGE
	} else {
		event.GetChange().Type = pb.FileGlob_Output_Change_ADDITION
	}

	s.files[name] = content

	s.fileEvents <- event
}

func (s *grpcServer) DeleteFile(name string) {
	delete(s.files, name)
	s.fileEvents <- &pb.FileGlob_Output{
		Type: pb.FileGlob_Output_CHANGE,
		Payload: &pb.FileGlob_Output_Change_{
			Change: &pb.FileGlob_Output_Change{
				Path: name,
				Type: pb.FileGlob_Output_Change_DELETION,
			},
		},
	}
}

func (s *grpcServer) Serve() string {

	port, err := openPort()
	require.Nil(s.t, err)

	addr := ":" + port

	lis, err := net.Listen("tcp", addr)
	require.Nil(s.t, err)

	pb.RegisterApplicationServer(s.server, s)

	go s.server.Serve(lis)

	<-time.After(10 * time.Millisecond)
	s.addr = addr

	return addr
}

func (s *grpcServer) Stop() error {

	s.server.Stop()

	return nil
}

func (s *grpcServer) Query(q *pb.Query_Input, qsvr pb.Application_QueryServer) error {

	for _, r := range s.responses {
		if err := qsvr.Send(r); err != nil {
			return err
		}

	}

	return nil
}

func (g *grpcServer) FileGlob(q *pb.FileGlob_Input, s pb.Application_FileGlobServer) error {

	var fileset []string

	for k := range g.files {
		fileset = append(fileset, k)
	}

	sort.Strings(fileset)

	if err := s.Send(&pb.FileGlob_Output{
		Type: pb.FileGlob_Output_LIST,
		Payload: &pb.FileGlob_Output_List_{
			List: &pb.FileGlob_Output_List{
				Paths: fileset,
			},
		},
	}); err != nil {
		return err
	}

	for {
		select {
		case <-s.Context().Done():
			return nil

		case e, ok := <-g.fileEvents:

			if !ok {
				return nil
			}

			if err := s.Send(e); err != nil {
				return err
			}
		}
	}

	return nil
}

func (s *grpcServer) FileRead(ctx context.Context, q *pb.FileRead_Input) (*pb.FileRead_Output, error) {

	content, ok := s.files[q.Path]

	if !ok {
		return nil, fmt.Errorf("File not found")
	}

	return &pb.FileRead_Output{
		Content: content,
	}, nil
}

func (g *grpcServer) LiveReload(ctx context.Context, q *pb.LiveReload_Input) (*pb.LiveReload_Output, error) {
	return &pb.LiveReload_Output{}, nil
}
