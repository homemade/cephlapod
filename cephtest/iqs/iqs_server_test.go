package iqs

import (
	"testing"
	"time"

	"golang.org/x/net/context"

	"google.golang.org/grpc/metadata"

	"github.com/stretchr/testify/require"

	"bitbucket.org/homemade/cephlapod/pb"
)

type noopServer struct {
	context context.Context
}

func (m noopServer) SetHeader(metadata.MD) error  { return nil }
func (m noopServer) SendHeader(metadata.MD) error { return nil }
func (m noopServer) SetTrailer(metadata.MD)       {}
func (m noopServer) Context() context.Context     { return m.context }
func (m noopServer) SendMsg(interface{}) error    { return nil }
func (m noopServer) RecvMsg(interface{}) error    { return nil }

type mockPbApplicationQueryServer struct {
	noopServer
	response chan *pb.Query_Result
}

func newMockPbApplicationQueryServer(t *testing.T) *mockPbApplicationQueryServer {
	return &mockPbApplicationQueryServer{
		response: make(chan *pb.Query_Result),
	}
}

func (m *mockPbApplicationQueryServer) Send(q *pb.Query_Result) error {
	m.response <- q
	return nil
}

func (m *mockPbApplicationQueryServer) Assert(t *testing.T, qs ...*pb.Query_Result) {

	ticker := time.NewTicker(50 * time.Millisecond)
	timeout := ticker.C
	defer ticker.Stop()

	for _, q := range qs {
		select {

		case <-timeout:
			t.Fatalf("Timed out when waiting for query response")

		case r, ok := <-m.response:

			if !ok {
				t.Fatalf("Channel closed unexpectedly")
			}
			require.Equal(t, q, r)
		}
	}
}

func Test_AGrpcIqsServerCanBeCreatedAndShutDown(t *testing.T) {

	r0 := &pb.Query_Result{
		Type: pb.Query_Result_RECORDSET,
		Payload: &pb.Query_Result_Record{
			Record: &pb.Query_Result_RecordSet{
				Model:   "test0",
				Records: [][]byte{[]byte(`{}`)},
			},
		},
	}

	r1 := &pb.Query_Result{
		Type: pb.Query_Result_RECORDSET,
		Payload: &pb.Query_Result_Record{
			Record: &pb.Query_Result_RecordSet{
				Model:   "test1",
				Records: [][]byte{[]byte(`{}`)},
			},
		},
	}

	server, address := NewGrpcServer(t, r0)
	require.NotNil(t, server)
	require.NotEmpty(t, address)
	defer server.Stop()

	qsvr := newMockPbApplicationQueryServer(t)
	go server.Query(&pb.Query_Input{}, qsvr)
	qsvr.Assert(t, r0)

	server.AddQueryResult(r1)
	go server.Query(&pb.Query_Input{}, qsvr)
	qsvr.Assert(t, r0, r1)
}

func Test_AGrpcIqsServerCanSimulateFileWritesAndDeletes(t *testing.T) {

	ifc, _ := NewGrpcServer(t)
	defer ifc.Stop()

	s := ifc.(*grpcServer)

	s.WriteFile("A", []byte("hello"))
	require.Len(t, s.files, 1)
	require.Equal(t, map[string][]byte{"A": []byte("hello")}, s.files)

	s.DeleteFile("A")
	require.Len(t, s.files, 0)
}

type mockPbApplicationFileGlobServer struct {
	noopServer
	response chan *pb.FileGlob_Output
}

func newMockPbApplicationFileGlobServer(t *testing.T) *mockPbApplicationFileGlobServer {

	gs := &mockPbApplicationFileGlobServer{
		response: make(chan *pb.FileGlob_Output),
	}

	gs.context = context.Background()

	return gs
}

func (m *mockPbApplicationFileGlobServer) Send(q *pb.FileGlob_Output) error {
	m.response <- q
	return nil
}

func (m *mockPbApplicationFileGlobServer) Assert(t *testing.T, qs ...*pb.FileGlob_Output) {

	ticker := time.NewTicker(50 * time.Millisecond)
	timeout := ticker.C
	defer ticker.Stop()

	for _, q := range qs {
		select {

		case <-timeout:
			t.Fatalf("Timed out when waiting for glob response")

		case r, ok := <-m.response:

			if !ok {
				t.Fatalf("Channel closed unexpectedly")
			}
			require.Equal(t, q, r)
		}
	}
}

func Test_AGrpcServerCanListFilesOnDemand(t *testing.T) {

	s, _ := NewGrpcServer(t)
	defer s.Stop()

	qsvr := newMockPbApplicationFileGlobServer(t)

	s.FileSystem(map[string][]byte{
		"B": []byte("content B"),
		"C": []byte("content C"),
		"A": []byte("content A"),
	})

	// Start the server
	go s.FileGlob(&pb.FileGlob_Input{}, qsvr)

	// Test 0: basic glob
	qsvr.Assert(t, &pb.FileGlob_Output{
		Type: pb.FileGlob_Output_LIST,
		Payload: &pb.FileGlob_Output_List_{
			List: &pb.FileGlob_Output_List{
				Paths: []string{"A", "B", "C"},
			},
		},
	})

	// Test 1: changes
	s.WriteFile("B", []byte("content B modified"))
	s.DeleteFile("B")

	qsvr.Assert(t,
		&pb.FileGlob_Output{
			Type: pb.FileGlob_Output_CHANGE,
			Payload: &pb.FileGlob_Output_Change_{
				Change: &pb.FileGlob_Output_Change{
					Path: "B",
					Type: pb.FileGlob_Output_Change_CHANGE,
				},
			},
		},
		&pb.FileGlob_Output{
			Type: pb.FileGlob_Output_CHANGE,
			Payload: &pb.FileGlob_Output_Change_{
				Change: &pb.FileGlob_Output_Change{
					Path: "B",
					Type: pb.FileGlob_Output_Change_DELETION,
				},
			},
		},
	)
}

func Test_AGrpcServerWillProvideFileContentsOnDemand(t *testing.T) {

	s, _ := NewGrpcServer(t)
	defer s.Stop()

	s.FileSystem(map[string][]byte{
		"B": []byte("content B"),
		"C": []byte("content C"),
		"A": []byte("content A"),
	})

	o0, e0 := s.FileRead(context.Background(), &pb.FileRead_Input{Path: "A"})
	require.Nil(t, e0)
	require.NotNil(t, o0)
	require.Equal(t, []byte("content A"), o0.Content)

	o1, e1 := s.FileRead(context.Background(), &pb.FileRead_Input{Path: "notfound"})
	require.NotNil(t, e1)
	require.Equal(t, "File not found", e1.Error())

	require.Nil(t, o1)
}
