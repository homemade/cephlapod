package cephtest

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"bitbucket.org/homemade/cephlapod"
)

func Test_AMockWriteQueryFuncHasACallback(t *testing.T) {

	c := make(chan cephlapod.Model, 1)
	m := cephlapod.NewModel("t0")
	e := cephlapod.NewModel("t0", map[string]interface{}{"A": 1.0})

	require.Nil(t, NewMockWriteQuery(m, func(i cephlapod.Model) error {
		m.Set("A", 1.0)
		c <- m
		return nil
	}).Execute())

	ticker := time.NewTicker(10 * time.Millisecond)
	defer ticker.Stop()

	var (
		m0 cephlapod.Model
		ok bool
	)

	select {
	case <-ticker.C:
		t.Fatalf("Timed out when waiting for callback func")

	case m0, ok = <-c:
		if !ok {
			t.Fatalf("Unexpected channel close")
		}

		require.Equal(t, m, m0)
	}

	require.Equal(t, e, m0)
}

func Test_AMockReadQueryFuncHasACallback(t *testing.T) {

	c := make(chan []cephlapod.Model, 1)
	e := []cephlapod.Model{
		cephlapod.NewModel("t0", map[string]interface{}{"A": 1.0}),
		cephlapod.NewModel("t0", map[string]interface{}{"B": 2.0}),
	}

	results, err := NewMockReadQuery("t0", func(modelName string) ([]cephlapod.Model, error) {
		results := []cephlapod.Model{
			cephlapod.NewModel(modelName, map[string]interface{}{"A": 1.0}),
			cephlapod.NewModel(modelName, map[string]interface{}{"B": 2.0}),
		}

		c <- results

		return results, nil

	}).Execute()

	require.Nil(t, err)
	require.Equal(t, e, results)

	ticker := time.NewTicker(10 * time.Millisecond)
	defer ticker.Stop()

	var (
		m0 []cephlapod.Model
		ok bool
	)

	select {
	case <-ticker.C:
		t.Fatalf("Timed out when waiting for callback func")

	case m0, ok = <-c:
		if !ok {
			t.Fatalf("Unexpected channel close")
		}

		require.Equal(t, e, m0)
	}

	require.Equal(t, e, m0)
}

func Test_AMockDataModelQueryFuncHasACallback(t *testing.T) {

	c := make(chan map[string]cephlapod.ModelDefinition, 1)

	expected := map[string]cephlapod.ModelDefinition{
		"A": cephlapod.ModelDefinition{
			Fields: map[string]*cephlapod.ModelFieldDefinition{
				"field0": &cephlapod.ModelFieldDefinition{
					Label: "label0",
					Validation: map[string]cephlapod.ModelFieldValidationDefinition{
						"rule0": cephlapod.ModelFieldValidationDefinition{
							"one": "two",
						},
					},
					Hooks: map[string]cephlapod.ModelHookDefinition{
						"ui:on-blur": cephlapod.ModelHookDefinition{
							"service0": &cephlapod.ModelHookActionDefinition{
								Functions: []string{"fn0"},
								Async:     true,
								Priority:  999,
							},
						},
					},
				},
			},
		},
		"B": cephlapod.ModelDefinition{
			Fields: map[string]*cephlapod.ModelFieldDefinition{
				"field0": &cephlapod.ModelFieldDefinition{
					Label: "label0",
					Validation: map[string]cephlapod.ModelFieldValidationDefinition{
						"rule0": cephlapod.ModelFieldValidationDefinition{
							"one": "two",
						},
					},
					Hooks: map[string]cephlapod.ModelHookDefinition{
						"ui:on-blur": cephlapod.ModelHookDefinition{
							"service0": &cephlapod.ModelHookActionDefinition{
								Functions: []string{"fn0"},
								Async:     true,
								Priority:  999,
							},
						},
					},
				},
			},
		},
	}

	results, err := NewMockDataModelQuery(func() (map[string]cephlapod.ModelDefinition, error) {
		c <- expected
		return expected, nil

	}).Execute()

	require.Nil(t, err)
	require.Equal(t, expected, results)

	ticker := time.NewTicker(10 * time.Millisecond)
	defer ticker.Stop()

	var (
		ret map[string]cephlapod.ModelDefinition
		ok  bool
	)

	select {
	case <-ticker.C:
		t.Fatalf("Timed out when waiting for callback func")

	case ret, ok = <-c:
		if !ok {
			t.Fatalf("Unexpected channel close")
		}
	}

	require.Equal(t, expected, ret)
}
