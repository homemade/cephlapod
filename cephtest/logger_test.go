package cephtest

import (
	"testing"
)

func Test_ALoggerCanLogAndStoreMessages(t *testing.T) {

	l := NewLogger(t)

	l.Log("Hello 1")
	l.Log("Hello %d", 2)

	l.Assert("Hello 1", "Hello 2")
}
