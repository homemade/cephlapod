package cephlapod

import (
	"fmt"
	"time"

	"google.golang.org/grpc"

	"bitbucket.org/homemade/cephlapod/pb"
	"golang.org/x/net/context"
)

func (g *grpcServer) ValidateConfig(fn ConfigFunc) {
	g.configFn = fn
}

func (g *grpcServer) Config(ctx context.Context, req *pb.SetupConfigRequest) (*pb.SetupConfigResponse, error) {

	resp := &pb.SetupConfigResponse{
		Models:          make(map[string]*pb.SetupConfigResponse_Model),
		ModelFieldTypes: make(map[string]*pb.SetupConfigResponse_ModelFieldType),
		Modules:         make(map[string]*pb.SetupConfigResponse_Module),
		Apis:            make(map[string]*pb.SetupConfigResponse_Api),
		Themes:          make(map[string]*pb.SetupConfigResponse_Theme),
		UrlPermissions:  make(map[string]*pb.Access_Permission),
	}

	cfg := NewConfigMap()

	for _, v := range req.Config {
		cfg[v.Name] = v.Value
	}

	g.context.ApplicationRootURL = req.RootPath

	if g.configFn != nil {
		if err := g.configFn(g.context, g.logger, cfg); err != nil {
			return nil, fmt.Errorf("Config failed: %s", err)
		}
	}

	g.context.Config = cfg

	for name, hk := range g.hooks {
		resp.HookFunctions = append(
			resp.HookFunctions,
			&pb.SetupConfigResponse_HookFunction{
				Name:  name,
				Scope: pb.SetupConfigResponse_HookFunction_Scope(hk.scope),
			},
		)
	}

	for name := range g.deployers {
		resp.DeployerNames = append(resp.DeployerNames, name)
	}

	for name := range g.controllers {
		resp.ControllerNames = append(resp.ControllerNames, name)
	}

	for url, module := range g.modules {

		mod, err := module.grpcModule()

		if err != nil {
			return nil, fmt.Errorf("Can't serialise module '%s': %s", url, err)
		}

		resp.Modules[url] = mod
	}

	for version, api := range g.apis {
		resp.Apis[version] = api.grpcAPI()
	}

	for name, def := range g.models {
		model, err := def.Grpc()

		if err != nil {
			return nil, fmt.Errorf("Can't serialise model '%s': %s", name, err)
		}

		resp.Models[name] = model
	}

	for name, def := range g.modelFieldTypes {

		ft, err := def.grpc()

		if err != nil {
			return nil, fmt.Errorf("Can't serialise model '%s': %s", name, err)
		}

		resp.ModelFieldTypes[name] = ft
	}

	for name, theme := range g.themes {
		resp.Themes[name] = theme.grpcTheme()
	}

	for path := range g.urls {

		resp.Urls = append(resp.Urls, path)

		if p, exists := g.urlPermissions[path]; exists {
			resp.UrlPermissions[path] = p.grpc()
		}
	}

	return resp, nil
}

func (g *grpcServer) InternalQueryServerConfig(ctx context.Context, req *pb.IQSConfigRequest) (*pb.IQSConfigResponse, error) {

	resp := &pb.IQSConfigResponse{}

	// Connect to the service
	// TODO! Support TLS
	dialopts := []grpc.DialOption{
		grpc.WithInsecure(),
		grpc.WithBlock(),
		grpc.WithTimeout(50 * time.Millisecond),
	}
	conn, err := grpc.Dial(req.Url, dialopts...)

	if err != nil {
		return nil, fmt.Errorf("Can't connect to internal query server: %s", err)
	}

	// Store the connection
	g.iqsConn = pb.NewApplicationClient(conn)

	// Restart any file watchers
	for _, a := range g.fileWatchActuators {
		a.stop()
		if err := a.start(g.iqsConn); err != nil {
			return nil, err
		}
	}

	// Invoke callback, if required
	if g.queryEngineReadyFn != nil {
		g.queryEngineReadyFn(g.context, g.logger)
	}

	return resp, nil
}
