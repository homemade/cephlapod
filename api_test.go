package cephlapod

import (
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func AnAPICanBeCreated(t *testing.T) {
	require.NotNil(t, NewAPI())
}

func Test_AnApiCanBeExtractedFromAGrpcApiMethodAndViceVersa(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		grpc *pb.SetupConfigResponse_Api

		// Outputs
		ceph *API
	}{
		{
			description: "Valid, no methods",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{},
				Read:     &pb.SetupConfigResponse_Api_Method{},
				List:     &pb.SetupConfigResponse_Api_Method{},
				Update:   &pb.SetupConfigResponse_Api_Method{},
				Delete:   &pb.SetupConfigResponse_Api_Method{},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{
					Disabled: true,
				},
			},
		},
		{
			description: "Valid, create method",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{Disabled: true},
				Read:     &pb.SetupConfigResponse_Api_Method{},
				List:     &pb.SetupConfigResponse_Api_Method{},
				Update:   &pb.SetupConfigResponse_Api_Method{},
				Delete:   &pb.SetupConfigResponse_Api_Method{},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{Disabled: true},
				Create:      APIMethod{APIEndpoint{Disabled: true}},
			},
		},
		{
			description: "Valid, read method",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{},
				Read:     &pb.SetupConfigResponse_Api_Method{Disabled: true},
				List:     &pb.SetupConfigResponse_Api_Method{},
				Update:   &pb.SetupConfigResponse_Api_Method{},
				Delete:   &pb.SetupConfigResponse_Api_Method{},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{Disabled: true},
				Read:        APIMethod{APIEndpoint{Disabled: true}},
			},
		},
		{
			description: "Valid, list method",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{},
				Read:     &pb.SetupConfigResponse_Api_Method{},
				List:     &pb.SetupConfigResponse_Api_Method{Disabled: true},
				Update:   &pb.SetupConfigResponse_Api_Method{},
				Delete:   &pb.SetupConfigResponse_Api_Method{},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{Disabled: true},
				List:        APIMethod{APIEndpoint{Disabled: true}},
			},
		},
		{
			description: "Valid, update method",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{},
				Read:     &pb.SetupConfigResponse_Api_Method{},
				List:     &pb.SetupConfigResponse_Api_Method{},
				Update:   &pb.SetupConfigResponse_Api_Method{Disabled: true},
				Delete:   &pb.SetupConfigResponse_Api_Method{},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{Disabled: true},
				Update:      APIMethod{APIEndpoint{Disabled: true}},
			},
		},
		{
			description: "Valid, delete method",

			grpc: &pb.SetupConfigResponse_Api{
				Disabled: true,
				Create:   &pb.SetupConfigResponse_Api_Method{},
				Read:     &pb.SetupConfigResponse_Api_Method{},
				List:     &pb.SetupConfigResponse_Api_Method{},
				Update:   &pb.SetupConfigResponse_Api_Method{},
				Delete:   &pb.SetupConfigResponse_Api_Method{Disabled: true},
			},

			ceph: &API{
				APIEndpoint: APIEndpoint{Disabled: true},
				Delete:      APIMethod{APIEndpoint{Disabled: true}},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		ceph := apiFromGrpcAPI(test.grpc)
		require.Equal(t, *test.ceph, *ceph)

		grpc := test.ceph.grpcAPI()
		require.Equal(t, test.grpc.Disabled, grpc.Disabled)
		require.Equal(t, test.grpc.Create, grpc.Create)
	}
}
