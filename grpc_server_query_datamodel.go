package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

type grpcDatamodelQuery struct {
	server *grpcServer
}

func newGrpcDatamodelQuery(server *grpcServer) DataModelQuery {

	return &grpcDatamodelQuery{
		server: server,
	}
}

func (q *grpcDatamodelQuery) Execute() (map[string]ModelDefinition, error) {

	response, err := q.server.executeIQSQuery(&pb.Query_Input{
		Type: pb.Query_Input_DATAMODEL,
		Payload: &pb.Query_Input_DataModel_{
			DataModel: &pb.Query_Input_DataModel{},
		},
	}, 1)

	if err != nil {
		return nil, fmt.Errorf("Query failed: %s", err)
	}

	datamodel := response[0].GetDataModel()

	if datamodel == nil {
		return nil, fmt.Errorf("Response record is nil")
	}

	defs := make(map[string]ModelDefinition)

	for name, model := range datamodel.Models {

		m, err := grpcModelToModelDefinition(model)

		if err != nil {
			return nil, fmt.Errorf("Failed to decode model '%s': %s", name, err)
		}

		defs[name] = *m
	}

	return defs, nil
}
