package cephlapod

import "bitbucket.org/homemade/cephlapod/pb"

/*
An APIEndpoint describes the configuration of a final destination of an API
request. The only currenly configurable option is `Disabled` (which defaults to
false, such that all API endpoints are enabled by default).
*/
type APIEndpoint struct {
	Disabled bool
}

/*
An APIMethod describes the configuration of a CRULD API request. The only currenly
configurable option is `Disabled` (which defaults to false, such that all API
endpoints are enabled by default).
*/
type APIMethod struct {
	APIEndpoint
}

/*
An API describes one set of CRULD endpoints, usually for a Sepia model. The API
struct embeds APIEndpoint, so the entire API itself can be disabled. The API struct
is used when declaring global API configuration, and also when declaring specific
model API configuration. (The two configurations are interrelated; see sep doc
for more details).
*/
type API struct {
	APIEndpoint
	Create APIMethod
	Read   APIMethod
	List   APIMethod
	Update APIMethod
	Delete APIMethod
}

/*
NewAPI creates a pointer to an API object. By default, all endpoints are enabled.
*/
func NewAPI() *API {
	return &API{}
}

func apiMethodFromGrpcAPIMethod(input *pb.SetupConfigResponse_Api_Method) (output APIMethod) {

	if input == nil {
		return
	}

	output.Disabled = input.Disabled

	return
}

func apiFromGrpcAPI(input *pb.SetupConfigResponse_Api) *API {

	output := NewAPI()

	output.Disabled = input.Disabled
	output.Create = apiMethodFromGrpcAPIMethod(input.Create)
	output.Read = apiMethodFromGrpcAPIMethod(input.Read)
	output.List = apiMethodFromGrpcAPIMethod(input.List)
	output.Update = apiMethodFromGrpcAPIMethod(input.Update)
	output.Delete = apiMethodFromGrpcAPIMethod(input.Delete)

	return output
}

func (a APIEndpoint) grpcAPIMethod() *pb.SetupConfigResponse_Api_Method {
	return &pb.SetupConfigResponse_Api_Method{
		Disabled: a.Disabled,
	}
}

func (a *API) grpcAPI() *pb.SetupConfigResponse_Api {

	output := &pb.SetupConfigResponse_Api{}

	output.Disabled = a.Disabled
	output.Create = a.Create.grpcAPIMethod()
	output.Read = a.Read.grpcAPIMethod()
	output.List = a.List.grpcAPIMethod()
	output.Update = a.Update.grpcAPIMethod()
	output.Delete = a.Delete.grpcAPIMethod()

	return output
}
