package cephlapod

import (
	"fmt"
	"testing"
	"time"

	"golang.org/x/net/context"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AHookCanBeRegisteredAgainstAServer(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		name   string
		fn     HookFunc
		scope  HookScope
		models []string

		// Outputs
		hooks map[string]hook
	}{
		{
			description: "Basic registration",

			name:  "0",
			fn:    nil,
			scope: ScopePrivate,

			hooks: map[string]hook{
				"0": hook{
					scope: ScopePrivate,
					fn:    nil,
				},
			},
		},

		{
			description: "Model whitelisting",

			name:   "1",
			fn:     nil,
			scope:  ScopePrivate,
			models: []string{"registration", "payment"},

			hooks: map[string]hook{
				"1": hook{
					scope:          ScopePrivate,
					fn:             nil,
					modelWhitelist: []string{"registration", "payment"},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.hooks = make(map[string]hook)

		p.HandleHook(test.name, test.fn, test.scope, test.models...)

		require.Equal(t, test.hooks, p.hooks)
	}
}

func Test_AHookCallCanBeMadeToAServer(t *testing.T) {

	c := make(chan *HookResponse, 1)

	for cycle, test := range []struct {
		description string

		// Inputs
		req   *pb.ExternalServiceRequest
		hooks map[string]hook

		// Outputs
		err          error
		hookResponse *HookResponse
	}{
		{
			description: "Basic function call",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"}`),
				},
			},
			hooks: map[string]hook{
				"test": hook{
					scope: ScopePrivate,
					fn: func(ctx Context, logger Logger, req HookRequest, resp *HookResponse) {
						c <- resp
					},
				},
			},

			hookResponse: NewHookResponse(NewModel("testModel")),
		},

		{
			description: "Modifying function call",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"}`),
				},
			},
			hooks: map[string]hook{
				"test": hook{
					scope: ScopePrivate,
					fn: func(ctx Context, logger Logger, req HookRequest, resp *HookResponse) {

						resp.Model.Set("A", "2")
						resp.FieldError("A", "Some error")
						resp.Error("Some general error")

						c <- resp
					},
				},
			},

			hookResponse: &HookResponse{
				Model: NewModel("testModel", map[string]interface{}{"A": "2"}),
				FieldErrors: map[string][]string{
					"A": []string{"Some error"},
				},
				Errors: []string{"Some general error"},
			},
		},

		{
			description: "Whitelisted function",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"}`),
				},
			},
			hooks: map[string]hook{
				"test": hook{
					scope: ScopePrivate,
					fn: func(ctx Context, logger Logger, req HookRequest, resp *HookResponse) {
						c <- resp
					},
					modelWhitelist: []string{"testModel"},
				},
			},

			hookResponse: NewHookResponse(NewModel("testModel")),
		},

		{
			description: "Non-whitelisted function",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"}`),
				},
			},
			hooks: map[string]hook{
				"test": hook{
					scope: ScopePrivate,
					fn: func(ctx Context, logger Logger, req HookRequest, resp *HookResponse) {
						c <- resp
					},
					modelWhitelist: []string{"onlyOneWhitelisted"},
				},
			},

			err:          fmt.Errorf("Hook function doesn't accept model 'testModel'"),
			hookResponse: NewHookResponse(NewModel("testModel")),
		},

		{
			description: "Invalid JSON",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"`),
				},
			},
			hooks: map[string]hook{
				"test": hook{
					scope: ScopePrivate,
					fn: func(Context, Logger, HookRequest, *HookResponse) {
						c <- &HookResponse{}
					},
				},
			},
			err: fmt.Errorf("Can't unmarshall input JSON: unexpected end of JSON input"),
		},

		{
			description: "Function not declared",

			req: &pb.ExternalServiceRequest{
				Function: "test",
				Model: &pb.ExternalServiceRequest_Model{
					Name: "testModel",
					Json: []byte(`{"A":"1"}`),
				},
			},

			err: fmt.Errorf("No hook function called 'test' registered"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.hooks = test.hooks

		resp, err := p.Call(context.Background(), test.req)

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, resp)

			timeout := time.NewTicker(1 * time.Second).C

			select {
			case <-timeout:
				t.Fatalf("Timed out waiting for function call")

			case resp := <-c:
				require.Equal(t, resp, test.hookResponse)
				break
			}

		} else {
			require.Equal(t, test.err.Error(), err.Error())
		}
	}
}
