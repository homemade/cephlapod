package cephlapod

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func Test_AModuleCanBeRegisteredAgainstAServer(t *testing.T) {

	mod := NewModule()

	for cycle, test := range []struct {
		description string

		// Inputs
		name   string
		module *Module

		// Outputs
		modules map[string]*Module
	}{
		{
			description: "Nil registration",

			name:   "0",
			module: nil,

			modules: map[string]*Module{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			name:   "1",
			module: mod,

			modules: map[string]*Module{
				"1": mod,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.modules = make(map[string]*Module)

		p.Module(test.name, test.module)

		require.Equal(t, len(test.modules), len(p.modules))

		for k, v := range test.modules {

			if v != nil {
				require.NotNil(t, p.modules[k])
			}
		}
	}
}

func Test_AnApiCanBeRegisteredAgainstAServer(t *testing.T) {

	api := NewAPI()

	for cycle, test := range []struct {
		description string

		// Inputs
		name string
		api  *API

		// Outputs
		apis map[string]*API
	}{
		{
			description: "Nil registration",

			name: "0",
			api:  nil,

			apis: map[string]*API{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			name: "1",
			api:  api,

			apis: map[string]*API{
				"1": api,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.apis = make(map[string]*API)

		p.API(test.name, test.api)

		require.Equal(t, len(test.apis), len(p.apis))

		for _, v := range test.apis {

			if v != nil {
				require.Equal(t, test.api, v)
			}
		}
	}
}

func Test_AModelDefinitionCanBeRegisteredAgainstAServer(t *testing.T) {

	mod := NewModelDefinition()

	for cycle, test := range []struct {
		description string

		// Inputs
		name  string
		model *ModelDefinition

		// Outputs
		models map[string]*ModelDefinition
	}{
		{
			description: "Nil registration",

			name: "0",

			models: map[string]*ModelDefinition{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			name:  "1",
			model: mod,

			models: map[string]*ModelDefinition{
				"1": mod,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.models = make(map[string]*ModelDefinition)

		p.Model(test.name, test.model)

		require.Equal(t, len(test.models), len(p.models))

		for k, v := range test.models {

			if v != nil {
				require.NotNil(t, p.models[k])
			}
		}
	}
}

func Test_AThemeDefinitionCanBeRegisteredAgainstAServer(t *testing.T) {

	theme := NewTheme()

	for cycle, test := range []struct {
		description string

		// Inputs
		name  string
		theme *Theme

		// Outputs
		themes map[string]*Theme
	}{
		{
			description: "Nil registration",

			name: "0",

			themes: map[string]*Theme{
				"0": nil,
			},
		},

		{
			description: "Non-nil registration",

			name:  "1",
			theme: theme,

			themes: map[string]*Theme{
				"1": theme,
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		p, _ := newMockGrpcServer(t)
		p.themes = make(map[string]*Theme)

		p.Theme(test.name, test.theme)

		require.Equal(t, len(test.themes), len(p.themes))

		for k, v := range test.themes {

			if v != nil {
				require.NotNil(t, p.themes[k])
			}
		}
	}
}
