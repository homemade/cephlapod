package cephlapod

import (
	"fmt"
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/Pallinder/go-randomdata"
	"github.com/stretchr/testify/require"
)

type variadicFormatString struct {
	message string
	args    []interface{}
}

func Test_AHookResponseCanPopulateErrors(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		model            Model
		inputErrors      []variadicFormatString
		inputFieldErrors map[string][]variadicFormatString

		// Outputs
		fieldErrors map[string][]string
		errors      []string
	}{
		{
			description: "No errors",

			model: NewModel(randomdata.SillyName()),

			fieldErrors: map[string][]string{},
			errors:      []string{},
		},

		{
			description: "General errors",

			model: NewModel(randomdata.SillyName()),

			inputErrors: []variadicFormatString{
				{
					message: "Hello %s",
					args:    []interface{}{"World"},
				},
			},

			fieldErrors: map[string][]string{},
			errors:      []string{"Hello World"},
		},

		{
			description: "General errors",

			model: NewModel(randomdata.SillyName()),

			inputFieldErrors: map[string][]variadicFormatString{
				"A": []variadicFormatString{
					{
						message: "Hello %s",
						args:    []interface{}{"World"},
					},
				},
			},

			fieldErrors: map[string][]string{
				"A": []string{"Hello World"},
			},
			errors: []string{},
		},

		{
			description: "Both kinds of errors",

			model: NewModel(randomdata.SillyName()),

			inputErrors: []variadicFormatString{
				{
					message: "Hello %s",
					args:    []interface{}{"World"},
				},
			},
			inputFieldErrors: map[string][]variadicFormatString{
				"A": []variadicFormatString{
					{
						message: "Hello %s",
						args:    []interface{}{"World"},
					},
				},
			},

			fieldErrors: map[string][]string{
				"A": []string{"Hello World"},
			},
			errors: []string{"Hello World"},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		hr := NewHookResponse(test.model)

		for _, v := range test.inputErrors {
			hr.Error(v.message, v.args...)
		}

		for k, sv := range test.inputFieldErrors {
			for _, v := range sv {
				hr.FieldError(k, v.message, v.args...)
			}
		}

		require.Equal(t, test.errors, hr.Errors)
		require.Equal(t, test.fieldErrors, hr.FieldErrors)
	}
}

func Test_AHookResponseCanReturnAProtoBufferResponse(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		hookResponse *HookResponse

		// Outputs
		pbResponse *pb.ExternalServiceResponse
		err        error
	}{
		{
			description: "Empty fields",

			hookResponse: &HookResponse{
				Model:       NewModel(randomdata.SillyName()),
				FieldErrors: make(map[string][]string),
				Errors:      []string{},
			},

			pbResponse: &pb.ExternalServiceResponse{
				Entity:      []byte(`{}`),
				Errors:      []string{},
				FieldErrors: make(map[string]*pb.ExternalServiceResponse_FieldErrors),
			},
		},

		{
			description: "Model fields",

			hookResponse: &HookResponse{
				Model:       NewModel(randomdata.SillyName(), map[string]interface{}{"A": "1"}),
				FieldErrors: make(map[string][]string),
				Errors:      []string{},
			},

			pbResponse: &pb.ExternalServiceResponse{
				Entity:      []byte(`{"A":"1"}`),
				Errors:      []string{},
				FieldErrors: make(map[string]*pb.ExternalServiceResponse_FieldErrors),
			},
		},

		{
			description: "General errors",

			hookResponse: &HookResponse{
				Model:       NewModel(randomdata.SillyName()),
				FieldErrors: make(map[string][]string),
				Errors:      []string{"A"},
			},

			pbResponse: &pb.ExternalServiceResponse{
				Entity:      []byte(`{}`),
				Errors:      []string{"A"},
				FieldErrors: make(map[string]*pb.ExternalServiceResponse_FieldErrors),
			},
		},

		{
			description: "Field errors",

			hookResponse: &HookResponse{
				Model: NewModel(randomdata.SillyName()),
				FieldErrors: map[string][]string{
					"A": []string{"1"},
				},
				Errors: []string{},
			},

			pbResponse: &pb.ExternalServiceResponse{
				Entity: []byte(`{}`),
				Errors: []string{},
				FieldErrors: map[string]*pb.ExternalServiceResponse_FieldErrors{
					"A": &pb.ExternalServiceResponse_FieldErrors{[]string{"1"}},
				},
			},
		},

		{
			description: "Model JSON failure",

			hookResponse: &HookResponse{
				Model: &model{
					name:   randomdata.SillyName(),
					values: nil,
				},
			},

			err: fmt.Errorf("Can't marshal nil values"),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		resp, err := test.hookResponse.externalServiceResponse()

		if test.err == nil {
			require.Nil(t, err)
			require.Equal(t, test.pbResponse, resp)
		} else {
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, resp)
		}
	}
}
