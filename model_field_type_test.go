package cephlapod

import (
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"
	"github.com/stretchr/testify/require"
)

func Test_AGrpcModelFieldTypeCanBeExtractedFromAModelFieldType(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input *ModelFieldType

		// Outputs
		output *pb.SetupConfigResponse_ModelFieldType
		err    error
	}{

		{
			description: "Empty",

			input: NewModelFieldType(),

			output: &pb.SetupConfigResponse_ModelFieldType{
				Validation: make(map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig),
			},
		},
		{
			description: "Non-empty",

			input: &ModelFieldType{
				Validation: map[string]ModelFieldValidationDefinition{
					"rule0": ModelFieldValidationDefinition{
						"one": "two",
					},
				},
			},

			output: &pb.SetupConfigResponse_ModelFieldType{
				Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
					"rule0": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
						Config: []byte(`{"one":"two"}`),
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := test.input.grpc()

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}
