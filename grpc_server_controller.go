package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"

	"golang.org/x/net/context"
)

func (g *grpcServer) Controller(name string, fn ControllerFunc) {
	g.controllers[name] = fn
}

func (g *grpcServer) InvokeController(ctx context.Context, in *pb.Controller_InvokeRequest) (*pb.Controller_InvokeResponse, error) {

	resp := &pb.Controller_InvokeResponse{}

	fn, ok := g.controllers[in.Name]

	if !ok {
		return resp, fmt.Errorf("Controller '%s' not registered", in.Name)
	}

	controllerCtx, err := controllerContextFromGrpcControllerContext(in.Context)

	if err != nil {
		return resp, fmt.Errorf("Failed to extract context from request: %s", err)
	}

	if err = fn(g.context, g.logger, controllerCtx); err != nil {
		resp.Success = false
		resp.ErrorMessage = err.Error()
		return resp, nil
	}

	respCtx, err := controllerCtx.grpcControllerContext()

	if err != nil {
		return resp, fmt.Errorf("Faled to serialise response: %s", err)
	}

	resp.Success = true
	resp.Context = respCtx

	return resp, nil
}
