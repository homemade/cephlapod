package cephlapod

import (
	"testing"

	"bitbucket.org/homemade/cephlapod/pb"

	"github.com/stretchr/testify/require"
)

func Test_AModelCanBeCreatedWithAllocatedMemory(t *testing.T) {
	m := NewModelDefinition()
	require.NotNil(t, m)
	require.NotNil(t, m.Fields)
	require.NotNil(t, m.Hooks)
	require.NotNil(t, m.Apis)
}

func Test_AGrpcModelCanBeExtractedFromAModel(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input *ModelDefinition

		// Outputs
		output *pb.SetupConfigResponse_Model
		err    error
	}{

		{
			description: "Empty",

			input: NewModelDefinition(),

			output: &pb.SetupConfigResponse_Model{
				Fields: make(map[string]*pb.SetupConfigResponse_Model_Field),
				Hooks:  make(map[string]*pb.SetupConfigResponse_Model_Hook),
				Apis:   make(map[string]*pb.SetupConfigResponse_Api),
				Permissions: &pb.SetupConfigResponse_Model_Permissions{
					Create: &pb.Access_Permission{
						Realms: make(map[string]*pb.Access_Permission_Realm),
					},
					Read: &pb.Access_Permission{
						Realms: make(map[string]*pb.Access_Permission_Realm),
					},
					Update: &pb.Access_Permission{
						Realms: make(map[string]*pb.Access_Permission_Realm),
					},
					List: &pb.Access_Permission{
						Realms: make(map[string]*pb.Access_Permission_Realm),
					},
					Delete: &pb.Access_Permission{
						Realms: make(map[string]*pb.Access_Permission_Realm),
					},
				},
			},
		},
		{
			description: "Non-empty",

			input: &ModelDefinition{
				Fields: map[string]*ModelFieldDefinition{

					"field0": &ModelFieldDefinition{
						Type:  "any",
						Label: "label0",
						Validation: map[string]ModelFieldValidationDefinition{
							"rule0": ModelFieldValidationDefinition{
								"one": "two",
							},
						},
						Hooks: map[string]ModelHookDefinition{
							"on:something": ModelHookDefinition{
								"service0": &ModelHookActionDefinition{
									Functions: []string{"fn0"},
									Async:     true,
									Priority:  999,
								},
							},
						},
					},
					"field1": &ModelFieldDefinition{
						Label: "label1",
						Type:  "any",
						Validation: map[string]ModelFieldValidationDefinition{
							"rule1": ModelFieldValidationDefinition{
								"one": "two",
							},
						},
						Hooks: map[string]ModelHookDefinition{
							"on:something": ModelHookDefinition{
								"service1": &ModelHookActionDefinition{
									Functions: []string{"fn1"},
									Async:     true,
									Priority:  999,
								},
							},
						},
						Permissions: ReadWritePermission{
							Read: Permission{
								Public:  true,
								Private: true,
								Realms: map[string]PermissionRoles{
									"realm0": PermissionRoles{
										AllRoles: []string{"r0", "r1"},
										AnyRoles: []string{"r2", "r3"},
									},
								},
							},
							Write: Permission{
								Public:  true,
								Private: true,
								Realms: map[string]PermissionRoles{
									"realm0": PermissionRoles{
										AllRoles: []string{"r0", "r1"},
										AnyRoles: []string{"r2", "r3"},
									},
								},
							},
						},
					},
				},
				Hooks: map[string]ModelHookDefinition{
					"on:something": ModelHookDefinition{
						"my-service": &ModelHookActionDefinition{
							Functions: []string{"fn0"},
							Async:     true,
							Priority:  999,
						},
					},
				},
				Apis: map[string]*API{
					"v1": &API{
						Create: APIMethod{
							APIEndpoint: APIEndpoint{
								Disabled: true,
							},
						},
					},
				},
				Permissions: CRULDPermission{
					Create: Permission{
						Public:  true,
						Private: true,
						Realms: map[string]PermissionRoles{
							"realm0": PermissionRoles{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Read: Permission{
						Public:  true,
						Private: true,
						Realms: map[string]PermissionRoles{
							"realm0": PermissionRoles{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Update: Permission{
						Public:  true,
						Private: true,
						Realms: map[string]PermissionRoles{
							"realm0": PermissionRoles{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					List: Permission{
						Public:  true,
						Private: true,
						Realms: map[string]PermissionRoles{
							"realm0": PermissionRoles{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Delete: Permission{
						Public:  true,
						Private: true,
						Realms: map[string]PermissionRoles{
							"realm0": PermissionRoles{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
				},
			},

			output: &pb.SetupConfigResponse_Model{
				Fields: map[string]*pb.SetupConfigResponse_Model_Field{
					"field0": &pb.SetupConfigResponse_Model_Field{
						Label: "label0",
						Type:  "any",
						Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
							"rule0": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
								Config: []byte(`{"one":"two"}`),
							},
						},
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
							"on:something": &pb.SetupConfigResponse_Model_Hook{
								Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
									"service0": &pb.SetupConfigResponse_Model_Hook_HookAction{
										Async:     true,
										Functions: []string{"fn0"},
										Priority:  999,
									},
								},
							},
						},
						Permissions: &pb.SetupConfigResponse_Model_Field_Permissions{
							Read: &pb.Access_Permission{
								Realms: make(map[string]*pb.Access_Permission_Realm),
							},
							Write: &pb.Access_Permission{
								Realms: make(map[string]*pb.Access_Permission_Realm),
							},
						},
					},
					"field1": &pb.SetupConfigResponse_Model_Field{
						Label: "label1",
						Type:  "any",
						Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
							"rule1": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
								Config: []byte(`{"one":"two"}`),
							},
						},
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
							"on:something": &pb.SetupConfigResponse_Model_Hook{
								Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
									"service1": &pb.SetupConfigResponse_Model_Hook_HookAction{
										Async:     true,
										Functions: []string{"fn1"},
										Priority:  999,
									},
								},
							},
						},
						Permissions: &pb.SetupConfigResponse_Model_Field_Permissions{
							Read: &pb.Access_Permission{
								Public:  true,
								Private: true,
								Realms: map[string]*pb.Access_Permission_Realm{
									"realm0": &pb.Access_Permission_Realm{
										AllRoles: []string{"r0", "r1"},
										AnyRoles: []string{"r2", "r3"},
									},
								},
							},
							Write: &pb.Access_Permission{
								Public:  true,
								Private: true,
								Realms: map[string]*pb.Access_Permission_Realm{
									"realm0": &pb.Access_Permission_Realm{
										AllRoles: []string{"r0", "r1"},
										AnyRoles: []string{"r2", "r3"},
									},
								},
							},
						},
					},
				},
				Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
					"on:something": &pb.SetupConfigResponse_Model_Hook{
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
							"my-service": &pb.SetupConfigResponse_Model_Hook_HookAction{
								Functions: []string{"fn0"},
								Async:     true,
								Priority:  999,
							},
						},
					},
				},
				Apis: map[string]*pb.SetupConfigResponse_Api{
					"v1": &pb.SetupConfigResponse_Api{
						Disabled: false,
						Create: &pb.SetupConfigResponse_Api_Method{
							Disabled: true,
						},
						Read: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						List: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						Update: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						Delete: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
					},
				},
				Permissions: &pb.SetupConfigResponse_Model_Permissions{
					Create: &pb.Access_Permission{
						Public:  true,
						Private: true,
						Realms: map[string]*pb.Access_Permission_Realm{
							"realm0": &pb.Access_Permission_Realm{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Read: &pb.Access_Permission{
						Public:  true,
						Private: true,
						Realms: map[string]*pb.Access_Permission_Realm{
							"realm0": &pb.Access_Permission_Realm{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Update: &pb.Access_Permission{
						Public:  true,
						Private: true,
						Realms: map[string]*pb.Access_Permission_Realm{
							"realm0": &pb.Access_Permission_Realm{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					List: &pb.Access_Permission{
						Public:  true,
						Private: true,
						Realms: map[string]*pb.Access_Permission_Realm{
							"realm0": &pb.Access_Permission_Realm{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
					Delete: &pb.Access_Permission{
						Public:  true,
						Private: true,
						Realms: map[string]*pb.Access_Permission_Realm{
							"realm0": &pb.Access_Permission_Realm{
								AllRoles: []string{"r0", "r1"},
								AnyRoles: []string{"r2", "r3"},
							},
						},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := test.input.Grpc()

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}

func Test_AModelDefinitionCanBeExtractedFromAGrpcModel(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		input *pb.SetupConfigResponse_Model

		// Outputs
		output *ModelDefinition
		err    error
	}{
		{
			description: "Empty",

			input: &pb.SetupConfigResponse_Model{
				Fields: make(map[string]*pb.SetupConfigResponse_Model_Field),
				Hooks:  make(map[string]*pb.SetupConfigResponse_Model_Hook),
				Apis:   make(map[string]*pb.SetupConfigResponse_Api),
			},

			output: NewModelDefinition(),
		},
		{
			description: "Non-empty",

			input: &pb.SetupConfigResponse_Model{
				Fields: map[string]*pb.SetupConfigResponse_Model_Field{
					"field0": &pb.SetupConfigResponse_Model_Field{
						Label: "label0",
						Type:  "any",
						Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
							"rule0": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
								Config: []byte(`{"one":"two"}`),
							},
						},
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
							"on:something": &pb.SetupConfigResponse_Model_Hook{
								Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
									"service0": &pb.SetupConfigResponse_Model_Hook_HookAction{
										Async:     true,
										Functions: []string{"fn0"},
										Priority:  999,
									},
								},
							},
						},
					},
					"field1": &pb.SetupConfigResponse_Model_Field{
						Label: "label1",
						Type:  "any",
						Validation: map[string]*pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
							"rule1": &pb.SetupConfigResponse_Model_Field_ValidationRuleConfig{
								Config: []byte(`{"one":"two"}`),
							},
						},
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
							"on:something": &pb.SetupConfigResponse_Model_Hook{
								Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
									"service1": &pb.SetupConfigResponse_Model_Hook_HookAction{
										Async:     true,
										Functions: []string{"fn1"},
										Priority:  999,
									},
								},
							},
						},
					},
				},
				Hooks: map[string]*pb.SetupConfigResponse_Model_Hook{
					"on:something": &pb.SetupConfigResponse_Model_Hook{
						Hooks: map[string]*pb.SetupConfigResponse_Model_Hook_HookAction{
							"my-service": &pb.SetupConfigResponse_Model_Hook_HookAction{
								Functions: []string{"fn0"},
								Async:     true,
								Priority:  999,
							},
						},
					},
				},
				Apis: map[string]*pb.SetupConfigResponse_Api{
					"v1": &pb.SetupConfigResponse_Api{
						Disabled: false,
						Create: &pb.SetupConfigResponse_Api_Method{
							Disabled: true,
						},
						Read: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						List: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						Update: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
						Delete: &pb.SetupConfigResponse_Api_Method{
							Disabled: false,
						},
					},
				},
			},

			output: &ModelDefinition{
				Fields: map[string]*ModelFieldDefinition{

					"field0": &ModelFieldDefinition{
						Label: "label0",
						Type:  "any",
						Validation: map[string]ModelFieldValidationDefinition{
							"rule0": ModelFieldValidationDefinition{
								"one": "two",
							},
						},
						Hooks: map[string]ModelHookDefinition{
							"on:something": ModelHookDefinition{
								"service0": &ModelHookActionDefinition{
									Functions: []string{"fn0"},
									Async:     true,
									Priority:  999,
								},
							},
						},
					},
					"field1": &ModelFieldDefinition{
						Label: "label1",
						Type:  "any",
						Validation: map[string]ModelFieldValidationDefinition{
							"rule1": ModelFieldValidationDefinition{
								"one": "two",
							},
						},
						Hooks: map[string]ModelHookDefinition{
							"on:something": ModelHookDefinition{
								"service1": &ModelHookActionDefinition{
									Functions: []string{"fn1"},
									Async:     true,
									Priority:  999,
								},
							},
						},
					},
				},
				Hooks: map[string]ModelHookDefinition{
					"on:something": ModelHookDefinition{
						"my-service": &ModelHookActionDefinition{
							Functions: []string{"fn0"},
							Async:     true,
							Priority:  999,
						},
					},
				},
				Apis: map[string]*API{
					"v1": &API{
						Create: APIMethod{
							APIEndpoint: APIEndpoint{
								Disabled: true,
							},
						},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		output, err := grpcModelToModelDefinition(test.input)

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, output)
			require.Equal(t, test.output, output)
		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, output)
		}
	}
}
