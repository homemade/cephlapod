package cephlapod

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

type grpcUpdateQuery struct {
	server *grpcServer
	model  Model
	uid    string
}

func newGrpcUpdateQuery(server *grpcServer, uid string, model Model) WriteQuery {

	return &grpcUpdateQuery{
		server: server,
		model:  model,
		uid:    uid,
	}
}

func (q *grpcUpdateQuery) Execute() error {

	jsn, err := json.Marshal(q.model.dirtyFields())

	if err != nil {
		return fmt.Errorf("Failed to marshal JSON: %s", err)
	}

	response, err := q.server.executeIQSQuery(&pb.Query_Input{
		Type: pb.Query_Input_UPDATE,
		Payload: &pb.Query_Input_Update_{
			Update: &pb.Query_Input_Update{
				Model:   q.model.Name(),
				Content: jsn,
				Uid:     q.uid,
			},
		},
	}, 1)

	if err != nil {
		return err
	}

	record := response[0].GetRecord()

	if record == nil {
		return fmt.Errorf("Can't parse record set from response")
	}

	if e, g := q.model.Name(), record.Model; e != g {
		return fmt.Errorf("Wrong model type: got %s, expected %s", g, e)
	}

	if err := q.model.UnmarshalJSON(record.Records[0]); err != nil {
		return fmt.Errorf("Failed to unmarshal response JSON: %s", err)
	}

	q.model.resetDirtyFields()

	return nil
}
