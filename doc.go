/*
Package cephlapod is a Sepia plugin framework for Go.

All Sepia plugins are gRPC servers which respond to requests from Sepia itself,
which acts as a client.  This package wraps the complex and sometimes messy
gRPC functionality, and allows plugins to be developed and tested in an
abstract, test-driven way.

All Sepia plugins are independent, stand-alone applications that run as a
discreet process at the operating system level. They must be independently
executable (as in, their package must be named 'main' and contain a main()
function) and are required to clear up after themselved when their process is
killed by an interrupt signal. Aside from using some standard code to interact
with Sepia, they are free to operate in any way they would like.

Cephlapod-driven plugins can provide deployment strategies, models, modules,
themes, APIs (for both specific models and general purposes), MVC controllers,
and URL handlers, including files from the disk or memory. A full description
of how to design and integrate a plugin is contained in the Sepia documentation.

At the core of Cephlapod is the Server interface (see formal definition and
examples below), which acts as the gateway to all the supported plugin
functionality. Once you have created a valid server all the power of the plugin
system becomes available. The Server instance should be stored as a persistent
entity in the plugin application.

A cephlapod-powered plugin could be as simple as:

 package main

 import "bitbucket.org/homemade/cephlapod"

 func main() {

	 server, err := cephlapod.NewGrpcServer()

	 if err != nil {
		 // handle fatal error
	 }

	 server.Serve()
 }

Note that to actually be used in Sepia is will require a Pluginfile (see the
'Plugins' section in sep doc). Also that plugin won't actually do anything - it
would just idle in the background.

A more realistic example of a very simple plugin is:

 package main

 import "bitbucket.org/homemade/cephlapod"

 func main() {

	 server, err := cephlapod.NewGrpcServer()

	 if err != nil {
		 // handle fatal error
	 }

	 server.HandleHook("hookname", myHookHandler, cephlapod.ScopePublic)

	 server.Serve()
 }

 func myHookHandler(ctx cephlapod.Context, logger cephlapod.Logger, req cephlapod.HookRequest, resp *cephlapod.HookResponse) {

	logger.Log("Received a hook request for '%s'", req.Function)

	resp.Model.Set("someField", "This was set by my hook!")
 }

That plugin (again, it requires a Pluginfile) could be attacked to a model hook
field, and would set the value of `someField`, if it could. It would also log
to Sepia's main log output.

Sepia also provides an Internal Query Service (IQS) which allows plugins to
extract and manpulate models stored in Sepia's datastore. The service uses a
CRUD architecture, and is able to obtain the complete data model, using a set
of interfaces that are returned from functions on the server object. Additionally,
the IQS offers file read access using the same data pipelines as sep itself –
anything that sep can read, including file data stored inside the binary or on
remote systems, is accessible to Sepia plugins.

The IQS is similar to the REST API that Sepia provides by default, except that
it's not subject to auth (plugins are assumed to be trusted entities). Normal
validation rules and hooks apply to model changes.

(Note that the Internal Query Servce is not available immediately; there is
usually a short delay, in the order of milliseconds, before queries can be
executed. The QueryEngineReady() function allows for a callback to be fired at
that time. See examples below.)

Sepia plugins may also provide access control functionality, allowing for complex
third-party authentication and authorisation in Sepia applications. Sepia has a
full suite of access control systems which carry requests to nominated plugins
for auth (see the Sepia documentation for details) and cephlapod makes responding
to those requests simple and intuitive.

The best way to get started with a plugin is to use one of the templates from
the sep binary itself. For a hook template, run

 sep new hook-plugin

in a directory under `plugins` in your sepia project. A fully-formed,
documented and unit tested template will be generated with multuple hooks and
complex configuration.
*/
package cephlapod
