package cephlapod

import (
	"fmt"
	"net"
	"os"
	"testing"
	"time"

	"google.golang.org/grpc"

	randomdata "github.com/Pallinder/go-randomdata"
	"github.com/stretchr/testify/require"
)

func openPort(t *testing.T) int {

	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	require.Nil(t, err)

	l, err := net.ListenTCP("tcp", addr)
	require.Nil(t, err)

	defer l.Close()

	port := l.Addr().(*net.TCPAddr).Port

	return port
}

func newMockGrpcServer(t *testing.T) (*grpcServer, *mockStdoutPrintFn) {

	logger, m := newMockStdoutLogger(t)

	return &grpcServer{
		server: grpc.NewServer(),
		opts:   []grpc.ServerOption{},
		context: Context{
			Port:   fmt.Sprintf(":%d", openPort(t)),
			Handle: "test",
		},
		logger:      logger,
		hooks:       make(map[string]hook),
		deployers:   map[string]DeploymentFunc{"test": mockDeploymentFn},
		controllers: map[string]ControllerFunc{"test": mockControllerFn},
		modules:     map[string]*Module{"test": NewModule()},
		apis:        map[string]*API{"v1": NewAPI()},
		themes:      map[string]*Theme{"test": NewTheme()},
		urls:        map[string]HTTPHandlerFunc{"0": nil},
	}, m
}

func Test_ANewGrpcServerCanBeCreatedAndExecuted(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		port   string
		handle string

		// Outputs
		err      error
		serveErr error
	}{
		{
			description: "Happy path",

			port:   fmt.Sprintf("%d", openPort(t)),
			handle: randomdata.SillyName(),
		},

		{
			description: "Sad path: no port",

			handle: randomdata.SillyName(),

			err: fmt.Errorf("%s environment variable required", EnvPluginPort),
		},

		{
			description: "Sad path: invalid port",

			handle: randomdata.SillyName(),
			port:   "::123",

			serveErr: fmt.Errorf("Failed to listen on port ::123: listen tcp: address :::123: too many colons in address"),
		},

		{
			description: "Sad path: no handle",

			port: ":1234",

			err: fmt.Errorf("%s environment variable required", EnvPluginHandle),
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)

		os.Setenv(EnvPluginPort, test.port)
		os.Setenv(EnvPluginHandle, test.handle)

		p, err := NewGrpcServer()

		if test.err == nil {
			require.Nil(t, err)
			require.NotNil(t, p)

			if test.serveErr == nil {
				go p.Serve()
				defer p.Stop()

			} else {

				c := make(chan error, 1)
				timeout := time.NewTicker(1 * time.Second).C

				go func() {
					c <- p.Serve()
				}()

				select {
				case <-timeout:
					t.Fatalf("Timed out waiting for server error")

				case err := <-c:
					require.Equal(t, test.serveErr.Error(), err.Error())
					break
				}
			}

		} else {
			require.NotNil(t, err)
			require.Equal(t, test.err.Error(), err.Error())
			require.Nil(t, p)
		}
	}

	os.Setenv(EnvPluginPort, "")
	os.Setenv(EnvPluginHandle, "")
}
