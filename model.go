package cephlapod

import (
	"encoding/json"
	"fmt"
)

/*
A Model in cephlapod represents the pertinent parts of a Sepia model for the
purposes of plugin development: chiefly its name and field values. When a
request is made that pertains to a Sepia model, a cephlapod Model will be generated
to allow decoupled interation with it.

Field values are stored as blank interface{}s. They are completely mutable and may
contain almost any value. There are two restrictions on field values:

1. The model will be translated to JSON in order to be sent back to Sepia,
so the field must be JSON-ifyable.

2. Sepia does not accept nested field values, so if you send back an object it will
be stored as JSON, not unmarshalled back into a complex type.
*/
type Model interface {
	Name() string
	Get(string) (interface{}, error)
	Set(string, interface{}) error
	MarshalJSON() ([]byte, error)
	UnmarshalJSON([]byte) error
	Map() map[string]interface{}
	dirtyFields() map[string]interface{}
	resetDirtyFields()
}

type model struct {
	name   string
	values map[string]interface{}
	dirty  map[string]interface{}
}

/*
NewModel is a convenience function for creating a new model, with option field values.
*/
func NewModel(name string, fields ...map[string]interface{}) Model {

	m := &model{
		name:   name,
		values: make(map[string]interface{}),
	}

	m.resetDirtyFields()

	for _, input := range fields {
		for k, v := range input {
			m.Set(k, v)
		}
	}

	return m
}

func (m *model) Name() string {
	return m.name
}

func (m *model) Get(s string) (interface{}, error) {

	if v, ok := m.values[s]; ok {
		return v, nil
	}

	return nil, fmt.Errorf("No field %s", s)
}

func (m *model) Set(s string, i interface{}) error {
	m.values[s] = i

	restricted := map[string]struct{}{"id": {}, "created": {}, "last_updated": {}}

	if _, ok := restricted[s]; !ok {
		m.dirty[s] = i
	}

	return nil
}

func (m *model) MarshalJSON() ([]byte, error) {

	if m.values == nil {
		return []byte(`{}`), fmt.Errorf("Can't marshal nil values")
	}

	return json.Marshal(m.values)
}

func (m *model) UnmarshalJSON(jsn []byte) (e error) {

	m.resetDirtyFields()

	if e := json.Unmarshal(jsn, &m.values); e != nil {
		return e
	}
	return
}

func (m *model) Map() map[string]interface{} {
	return m.values
}

func (m *model) dirtyFields() map[string]interface{} {
	return m.dirty
}

func (m *model) resetDirtyFields() {
	m.dirty = make(map[string]interface{})
}
