package cephlapod

import (
	"testing"

	"github.com/stretchr/testify/require"
)

////////////////////////////////////////////////////////////////////////////////
// Testing utility functions
////////////////////////////////////////////////////////////////////////////////

func newMockStdoutLogger(t *testing.T) (Logger, *mockStdoutPrintFn) {

	l := NewStdoutLogger().(*stdoutLogger)
	m := newMockmockStdoutPrintFn(t)

	l.fn = m.print

	return l, m
}

////////////////////////////////////////////////////////////////////////////////
// Mock objects
////////////////////////////////////////////////////////////////////////////////

type mockStdoutPrintFn struct {
	lines []string
}

func newMockmockStdoutPrintFn(t *testing.T) *mockStdoutPrintFn {
	return &mockStdoutPrintFn{}
}

func (m *mockStdoutPrintFn) print(inputs ...interface{}) (int, error) {

	line := ""

	for _, input := range inputs {
		if s, ok := input.(string); ok {
			line += s
		}
	}

	m.lines = append(m.lines, line)

	return len(line), nil
}

func (m *mockStdoutPrintFn) assert(t *testing.T, lines ...string) {

	require.Equal(t, len(lines), len(m.lines))

	for i, line := range lines {
		require.Equal(t, line+"\n", m.lines[i])
	}
}

////////////////////////////////////////////////////////////////////////////////
// Unit tests
////////////////////////////////////////////////////////////////////////////////

func Test_AStdoutLoggerCanBeCreated(t *testing.T) {
	require.NotNil(t, NewStdoutLogger())
}

func Test_AStdoutLoggerWillLogMessages(t *testing.T) {

	l, m := newMockStdoutLogger(t)

	l.Log("Hello 1")
	l.Log("Hello %d", 2)

	m.assert(t, "Hello 1", "Hello 2")
}
