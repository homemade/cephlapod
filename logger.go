package cephlapod

/*
A Logger can be used to write logs to the parent process during plugin
execution (or to the testing log during unit tests). Because sep uses
newline characters to mark the end of log messages, trailing newlines
will be ommitted.
*/
type Logger interface {
	Log(string, ...interface{})
}
