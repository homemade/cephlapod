package cephlapod

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"
)

type grpcReadQuery struct {
	server *grpcServer
	query  *pb.Query_Input
	model  string
}

func newGrpcReadQuery(server *grpcServer, model string) ReadQuery {

	return &grpcReadQuery{
		server: server,
		model:  model,
		query: &pb.Query_Input{
			Type: pb.Query_Input_READ,
			Payload: &pb.Query_Input_Read_{
				Read: &pb.Query_Input_Read{
					Model: model,
				},
			},
		},
	}
}

func (q *grpcReadQuery) queryInput() *pb.Query_Input {
	return q.query
}

func (q *grpcReadQuery) appendString(slice []string, i string) []string {
	for _, ele := range slice {
		if ele == i {
			return slice
		}
	}
	return append(slice, i)
}

func (q *grpcReadQuery) Select(fields ...string) ReadQuery {

	r := q.query.GetRead()

	if r == nil {
		return q
	}

	for _, v := range fields {
		r.Fields = q.appendString(r.Fields, v)
	}

	return q
}

func (q *grpcReadQuery) Where(field string, op Operator, val interface{}) ReadQuery {

	r := q.query.GetRead()

	if r == nil {
		return q
	}

	if r.Filters == nil {
		r.Filters = make(map[string]*pb.Query_Input_Read_Filter)
	}

	jsn, err := json.Marshal(val)

	if err != nil {
		return q
	}

	var operator pb.Query_Input_Read_Filter_Operator

	switch op {
	case OperatorEQ:
		operator = pb.Query_Input_Read_Filter_EQ

	case OperatorGT:
		operator = pb.Query_Input_Read_Filter_GT

	case OperatorGTE:
		operator = pb.Query_Input_Read_Filter_GTE

	case OperatorLT:
		operator = pb.Query_Input_Read_Filter_LT

	case OperatorLTE:
		operator = pb.Query_Input_Read_Filter_LTE

	case OperatorNE:
		operator = pb.Query_Input_Read_Filter_NE

	case OperatorIN:
		operator = pb.Query_Input_Read_Filter_IN

	case OperatorNIN:
		operator = pb.Query_Input_Read_Filter_NIN

	default:
		operator = pb.Query_Input_Read_Filter_EQ
	}

	r.Filters[field] = &pb.Query_Input_Read_Filter{
		Operator: operator,
		Value:    jsn,
	}

	return q
}

func (q *grpcReadQuery) Sort(field string, order Order) ReadQuery {

	var pbOrder pb.Query_Input_Read_Sortation_Order

	switch order {
	case SortAscending:
		pbOrder = pb.Query_Input_Read_Sortation_ASCENDING
	default:
		pbOrder = pb.Query_Input_Read_Sortation_DESCENDING
	}

	r := q.query.GetRead()

	if r == nil {
		return q
	}

	r.Sortations = append(r.Sortations,
		&pb.Query_Input_Read_Sortation{
			Field: field,
			Order: pbOrder,
		},
	)

	return q
}

func (q *grpcReadQuery) Limit(page, pageSize int) ReadQuery {

	r := q.query.GetRead()

	if r == nil {
		return q
	}

	r.Limit = &pb.Query_Input_Read_Limit{
		Page:     int32(page),
		PageSize: int32(pageSize),
	}

	return q
}

func (q *grpcReadQuery) Execute() (results []Model, err error) {

	response, err := q.server.executeIQSQuery(q.queryInput(), 1)

	if err != nil {
		return results, fmt.Errorf("Query failed: %s", err)
	}

	record := response[0].GetRecord()

	if record == nil {
		return results, fmt.Errorf("Can't parse record set from response")
	}

	if e, g := q.model, record.Model; e != g {
		return results, fmt.Errorf("Wrong model type: got %s, expected %s", g, e)
	}

	for _, v := range record.Records {

		var raw []json.RawMessage

		if err := json.Unmarshal(v, &raw); err != nil {
			return results, fmt.Errorf("Failed to parse record results: %s", err)
		}

		for k, r := range raw {

			m := NewModel(q.model)

			if err := m.UnmarshalJSON(r); err != nil {
				return results, fmt.Errorf("Failed to parse result %d: %s", k, err)
			}

			results = append(results, m)
		}
	}

	return
}
