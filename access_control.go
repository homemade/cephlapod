package cephlapod

import (
	"fmt"
	"net/http"

	"bitbucket.org/homemade/cephlapod/pb"
)

/*
HTTPAccessControlFunc is a callback for plugins providing access control
functionality over HTTP. An access control function must accept a Content, a
Logger, a list of AccessRequests, a standard http.Request pointer, and a
standard http.ResponseWriter. It must return an error if a failure, either
related or unrelated to the access control operation, occurs, and nil if all
the AccessRquests are successfully checked and permitted.

The purpose of the function is to perform authentication (that is, check that
the appropriate credentials have been provided in the HTTP request) and
authorise any access requests. For efficiency, Sepia batches access request
operations, so it's possible that the callback function may receive several
separate access requests, all relating to a single HTTP request. Every access
request includes as much meta information as possible about the system entity.
Models and model field access requests include a UID, so any authorisation
decisions based on content can be make in conjunction with the Internal Query
Server.

The *http.Request and http.ResponseWriter may be used as normal. Headers and
content sent to the http.ResponseWriter will be proxied out to the end user's
client. It's also possible to set the HTTP status code, but it's not advisable
because Sepia issues standard response codes depending on the circumstance.

A return value of nil indicates that the http request has been authenticated,
and all provided access requests have been authorised. There are specific error
types for other kinds of failure:

AccessControlErrNoCredentials indicates an authentication failure due to lack
of credentials.

AccessControlErrInvalidCredentials indicated an authentication failure due to
invalid or expired credentials.

AccessControlErrNotAuthorised indicates an authorisation failure. One or more
of the access requests is not permitted.

Any other kind of error is treated as a system error, which generally results
in a 500 Internal Server Error response.

All error messages may, depending on the type and circumstance, be sent to the
end user, so it's worth keeping sensitive information out of them. Anything that
needs to be recorded can be logged using the Logger.

Note that access control requests are made to plugins during HTTP requests in
Sepia, so they should be as fast and efficient as possible. Callback functions
only return a single error (as opposed to a more complex and informative
structures, like a slice or map or errors) and Sepia expects access control
plugins to only report the first error they encounter, even if that gives an
incomplete picture of the failure.
*/
type HTTPAccessControlFunc func(Context, Logger, []AccessRequest, *http.Request, http.ResponseWriter) error

//go:generate stringer -type=AccessRequestOperation

/*
AccessRequestOperation represents the mode of access for an access request.
*/
type AccessRequestOperation int

// There are six operations: Create, Read, Update (or Write), List, delete and
// a general concept of Access.
const (
	OperationGeneral AccessRequestOperation = iota
	OperationCreate
	OperationRead
	OperationUpdate
	OperationList
	OperationDelete
)

//go:generate stringer -type=AccessRequestEntityType

/*
AccessRequestEntityType represents the kind of thing being accessed in an
access request.
*/
type AccessRequestEntityType int

/*
There are seven categories of entity: General, models, model fields, APIs,
modules, assets and URLs created by plugins.
*/
const (
	EntityTypeGeneral AccessRequestEntityType = iota
	EntityTypeModel
	EntityTypeModelField
	EntityTypeAPI
	EntityTypeModule
	EntityTypeAsset
	EntityTypePluginAsset
)

/*
An AccessRequest describes a particular operation on a particular entity in a
given role. AccessRequests are the primary mode of access control in Sepia. The
structure represents a provisional end-user request to protected elements of a
Sepia application, and it's used in callback functions like
HTTPAccessControlFunc to authorise requests. Each AccessRequest refers to a
single entity in a single role; multiple requests for access are represented as
an []AccessRequest.

Note that AccessRequest is used only in the creation of access control plugins.
It's not required for the registration of protected entities.
*/
type AccessRequest struct {

	// Operation represents the mode of access for an access request, like Read,
	// Update, Delete, etc.
	Operation AccessRequestOperation

	// EntityType represents the kind of thing being accessed – a model, a module,
	// etc.
	EntityType AccessRequestEntityType

	// Identifier is the system name of the entity. For models this is the name; for
	// model fields it's a dotpath (for example, 'mymodel.myfield'); for APIs it's the
	// name of the API; for modules, assets and plugin assets it's the relative URL,
	// as defined in the SCL file or plugin that defined them.
	Identifier string

	// UID is the system identifier for models (also passed for model fields).
	UID string

	// Role is the user-defined role identifier, as written in the SCL file or plugin
	// that specified the entity permissions.
	Role string
}

/*
Read an access request from its gRPC equivalent.
*/
func accessRequestFromGrpc(input *pb.Access_Request_Permission) (output AccessRequest) {

	output.Operation = AccessRequestOperation(input.Operation)
	output.EntityType = AccessRequestEntityType(input.Accessible)
	output.Identifier = input.Identifier
	output.UID = input.Uid
	output.Role = input.Role

	return
}

/*
accessControlErrorBase essentially stores and outputs an error string, thus
implementing the error interface.
*/
type accessControlErrorBase struct {
	s string
}

func (e accessControlErrorBase) Error() string { return e.s }

/*
AccessControlErrNoCredentials represents an authentication error resultant from
a lack of credentials bring provided in a request.  It's usually returned by
access request callback functions like HTTPAccessControlFunc.
*/
type AccessControlErrNoCredentials struct{ accessControlErrorBase }

/*
AccessControlErrInvalidCredentials represents an authentication error resultant
from credentials supplied in a request being invalid or expired.  It's usually
returned by access request callback functions like HTTPAccessControlFunc.
*/
type AccessControlErrInvalidCredentials struct{ accessControlErrorBase }

/*
AccessControlErrNotAuthorised represents an authorisation error resultant from
provided credentials not allowing an access request operation.  It's usually
returned by access request callback functions like HTTPAccessControlFunc.
*/
type AccessControlErrNotAuthorised struct{ accessControlErrorBase }

/*
NewAccessControlErrNoCredentials creates a new error with variadic interpolation for convenience.
*/
func NewAccessControlErrNoCredentials(s string, args ...interface{}) AccessControlErrNoCredentials {
	e := AccessControlErrNoCredentials{}
	e.s = fmt.Sprintf(s, args...)
	return e
}

/*
NewAccessControlErrInvalidCredentials creates a new error with variadic interpolation for convenience.
*/
func NewAccessControlErrInvalidCredentials(s string, args ...interface{}) AccessControlErrInvalidCredentials {
	e := AccessControlErrInvalidCredentials{}
	e.s = fmt.Sprintf(s, args...)
	return e
}

/*
NewAccessControlErrNotAuthorised creates a new error with variadic interpolation for convenience.
*/
func NewAccessControlErrNotAuthorised(s string, args ...interface{}) AccessControlErrNotAuthorised {
	e := AccessControlErrNotAuthorised{}
	e.s = fmt.Sprintf(s, args...)
	return e
}
