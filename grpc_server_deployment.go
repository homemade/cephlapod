package cephlapod

import (
	"fmt"

	"bitbucket.org/homemade/cephlapod/pb"

	"golang.org/x/net/context"
)

/*
DeploymentFunc is the callback type used to create deployment strategies via
Server.Deployer(). When called a DeploymentFunc is tasked with deploying the
current state of the application to a given stage.
*/
type DeploymentFunc func(Context, Logger, string) error

func (g *grpcServer) Deployer(strategy string, fn DeploymentFunc) {
	g.deployers[strategy] = fn
}

func (g *grpcServer) Deploy(ctx context.Context, dr *pb.DeploymentRequest) (*pb.DeploymentResponse, error) {

	fn, exists := g.deployers[dr.Strategy]

	if !exists || fn == nil {
		return &pb.DeploymentResponse{Success: false}, fmt.Errorf("No deployment strategy '%s' registered", dr.Strategy)
	}

	if err := fn(g.context, g.logger, dr.Stage); err != nil {
		g.logger.Log("Deployment to %s failed: %s", dr.Stage, err)
		return &pb.DeploymentResponse{Success: false}, nil
	}

	return &pb.DeploymentResponse{Success: true}, nil
}
