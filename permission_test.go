package cephlapod

import (
	"testing"

	"github.com/stretchr/testify/require"

	"bitbucket.org/homemade/cephlapod/pb"
)

func Test_APermissionRoleSetCanBeSerialisedToGRPC(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		ceph PermissionRoles

		// Outputs
		grpc *pb.Access_Permission_Realm
	}{
		{
			description: "Empty",
			ceph:        PermissionRoles{},
			grpc:        &pb.Access_Permission_Realm{},
		},
		{
			description: "Not empty",
			ceph: PermissionRoles{
				AllRoles: []string{"r0", "r1"},
				AnyRoles: []string{"r2", "r3"},
			},
			grpc: &pb.Access_Permission_Realm{
				AllRoles: []string{"r0", "r1"},
				AnyRoles: []string{"r2", "r3"},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)
		require.Equal(t, test.grpc, test.ceph.grpc())
	}
}

func Test_APermissionCanBeSerialisedToGRPC(t *testing.T) {

	for cycle, test := range []struct {
		description string

		// Inputs
		ceph Permission

		// Outputs
		grpc *pb.Access_Permission
	}{
		{
			description: "Empty",
			ceph:        Permission{},
			grpc: &pb.Access_Permission{
				Realms: make(map[string]*pb.Access_Permission_Realm),
			},
		},
		{
			description: "Not empty",
			ceph: Permission{
				Public:  true,
				Private: true,
				Realms: map[string]PermissionRoles{
					"realm0": PermissionRoles{
						AllRoles: []string{"r0", "r1"},
						AnyRoles: []string{"r2", "r3"},
					},
				},
			},
			grpc: &pb.Access_Permission{
				Public:  true,
				Private: true,
				Realms: map[string]*pb.Access_Permission_Realm{
					"realm0": &pb.Access_Permission_Realm{
						AllRoles: []string{"r0", "r1"},
						AnyRoles: []string{"r2", "r3"},
					},
				},
			},
		},
	} {
		t.Logf("Cycle %d: %s", cycle, test.description)
		require.Equal(t, test.grpc, test.ceph.grpc())
	}
}
