package cephlapod

func (g *grpcServer) Module(url string, module *Module) {
	g.modules[url] = module
}

func (g *grpcServer) API(version string, api *API) {
	g.apis[version] = api
}

func (g *grpcServer) Model(name string, model *ModelDefinition) {
	g.models[name] = model
}

func (g *grpcServer) Theme(name string, theme *Theme) {
	g.themes[name] = theme
}
