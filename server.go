package cephlapod

// These are passed by Sepia to the plugin process at startup
const (
	EnvPluginPort   = "PLUGIN_PORT"
	EnvPluginHandle = "PLUGIN_HANDLE"
)

// These are visiblity hooks, and  must match the .proto file!
const (
	ScopePublic    = 0
	ScopeProtected = 1
	ScopePrivate   = 2
)

/*
A Server represents the heard of a cephlapod plugin. All interactions with
Sepia must be declared using the interface functions. The Sever() function
is blocking, and the plugin code is responsible for starting and stopping the
server.
*/
type Server interface {
	Serve() error
	Stop() error
	ValidateConfig(ConfigFunc)
	QueryEngineReady(QueryEngineReadyFunc)
	HandleHook(name string, fn HookFunc, scope HookScope, modelWhiteList ...string)
	Deployer(strategy string, fn DeploymentFunc)
	Controller(name string, fn ControllerFunc)
	Model(name string, model *ModelDefinition)
	ModelFieldType(name string, fieldType *ModelFieldType)
	Module(url string, module *Module)
	API(version string, api *API)
	Theme(name string, theme *Theme)
	URL(path string, fn HTTPHandlerFunc, perm ...Permission)
	NewCreateQuery(model Model) WriteQuery
	NewReadQuery(modelName string) ReadQuery
	NewUpdateQuery(uid string, model Model) WriteQuery
	NewDeleteQuery(modelName, uid string) WriteQuery
	NewDataModelQuery() DataModelQuery
	FileRead(path string) ([]byte, error)
	FileGlob(globs ...string) ([]string, error)
	FileWatch(fn FileWatchFunc, globs ...string) error
	FileUnwatch(globs ...string) error
	LiveReload(paths ...string) error
	HTTPAccessControl(HTTPAccessControlFunc)
}
